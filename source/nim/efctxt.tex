\section{Extreme Forward Calorimeter, EFC}

In order to improve the experimental sensitivity to some physics
processes such as $B\rightarrow \tau\nu$, the extreme forward
calorimeter, EFC, is needed to further extend the polar angle coverage
by ECL, 17$^o$ $< \theta <$ 150$^o$ \cite{TDR95}. EFC covers the
angular range from 6.4$^o$ to 11.5$^o$ in the forward direction and
163.3$^o$ to 171.2$^o$ in the backward direction. The EFC detector is
attached to the front faces of the cryostats of the compensation
solenoid magnets of the KEKB accelerator, surrounding the beam
pipe~\cite{Progress97,efc-Ueno}. EFC is also required to function as a
beam mask to reduce backgrounds for CDC.  In addition, EFC is used for
a beam monitor for the KEKB control and a luminosity monitor for the
Belle experiment.  It can also be used as a tagging device for
two-photon physics.

\subsection{Design and Construction}

Since EFC is placed in the very high radiation-level area around the
beam pipe near the interaction point, it is required to be
radiation-hard.  Therefore, two options for EFC were considered: 1) a
silicon and tungsten sandwich calorimeter and 2) a radiation-hard BGO
(Bismuth Germanate, Bi$_4$Ge$_3$O$_{12}$) crystal calorimeter. Both
candidates are equally radiation-hard and effective as a beam mask.
After studying the two options carefully, we chose the BGO option
because it costs less in terms of money and manpower and gives better
energy resolution~\cite{efc-WangBN164}.\\

\subsubsection{BGO and radiation hardness}

BGO has very desirable characteristics for electromagnetic calorimeters:
\begin{itemize}
\item radiation hardness at megarad level,
\item excellent $e/\gamma$ energy resolution of (0.3 - 1)\%/$\sqrt{E(GeV)}$,
\item high density of 7.1 gm/cm$^3$,
\item short radiation length of 1.12 cm,
\item large refractive index of 2.15,
\item suitable scintillating properties with the fast decay time of
about 300 ns and peak scintillation at about 480 nm, and
\item non-hygroscopic nature.
\end{itemize}
%

Pure BGO crystals with silicon photodiodes were proven to be capable
of detecting minimum-ionizing particles (MIPs) with a large S/N
ratio~\cite{efc-UenoA396}.  In the same experiment the nuclear counter
effect (NCE) was also clearly observed.  NCE is the extra amount of
charge produced in the photodiode by a charged particle directly
hitting it, on the top of the charge produced by the scintillation
light. The signal from MIPs is well separated from electronics noise
and NCE signal.\\

The radiation hardness of undoped BGO crystals from two manufacturers,
the Institute of Single Crystals, Kharkov, Ukraine and the Institute
of Inorganic Chemistry, Novosibirsk, Russia, was
measured~\cite{efc-SahuA388}. BGO crystals from Novosibirsk are found
to be quite radiation hard at least up to 10.5 Mrad equivalent dose
with a maximum of 15 \% damaging effect, while Kharkov crystals
degrade by more than 40 \% in terms of light output and transparency.
It was concluded that BGO crystals from Novosivirsk can be used for
the Belle detector.\\

Radiation damage tests were carried out using a $^{60}$Co source
($\sim$1000 C) at National Tsing University,
Taiwan~\cite{efc-PengA427}.  A total of 10 pieces of undoped trapezoid
BGO crystals with an approximate dimension of 12 $\times$ 2 $\times$ 1
cm$^3$ from Novosibirsk were used in the tests.
%%%%%%%%%%%%%%(Fig. 1 efc-PengA427)
Fig. \ref{eps-efc-setup} shows the experimental setup.  Trapezoid BGO
crystals were irradiated longitudinally (LBGO) or transversely
(TBGO). Light outputs were measured using a 90 $\mu$C $^{137}$Cs
source after irradiation and the ratio of signals from an irradiated
crystal and the non-irradiated reference crystal was obtained.
%%%%%%%%%%%%%%(Fig. 2 efc-PengA427)
Fig. \ref{eps-efc-raddata} shows some of the results.  \\

%%%%%%%%%%%%% Figure 1 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=efc/nthu.eps,width=8cm,angle=0}}
\vspace{5mm}
\caption{Top view of the experimental setup in the radiation source
room.  BGO crysals were 3 cm away from the center of a $^{60}$Co
source of $\sim$ 1000 C.}
\label{eps-efc-setup}
\end{center}
\vspace{5mm}
\end{figure}


Stable characteristics of the BGO crystals were observed under very
high dose conditions.  The light output decreases by $\sim$30 \% after
receiving $\sim$10 Mrad dose.  The crystals may have suffered some
permanent damage but can recover to 90 \% of their original light
output in a period of a day~\cite{efc-lowdose}.  Furthermore, after a
low dose irradiation of 115 krad the light output recovers to 100 \%
of the original value within hours, as shown in
%%%%%%%%%%%%%%%(Fig. 4 efc-Ueno)
Fig. \ref{eps-efc-radtime}.  After receiving the BGO crystals used in
the final assembly, the scintillation light yields of all the crystals
were checked by a radioactive source. The percentage spread of the
distribution is only 6 \%~\cite{efc-survey}.  The radiation hardness
check was done by using sample crystals, two per ingot, and some
randomly selected crystals of the final assembly.  Their performance
characteristics are quite different from those of crystals produced in
a small quantity.  The light yield drops about 25 - 50 \% after
receiving 1 krad dose and remains stable afterwards, checked up to 10
Mrad. The irradiation rate to receive 1 krad dose does not make much
difference since the recovery is slow, in days or weeks.  This
characteristics is good for a stable operation in the Belle
environment.\\

%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=efc/bgorad.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Results of the first week irradiation. The horizontal scale
is in equivalent doses and the vertical scale is the light yield
normalized to the original value without irradiation: (a) and (b)
correspond to the crystals irradiated longitudinally, and (c) and (d)
to those irradiated transversely.}
\label{eps-efc-raddata}
\end{center}
\vspace{5mm}
\end{figure}

%%%%%%%%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=efc/bgob115k.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{BGO test crystal 1 received a dose of 115 krad in one hour at
its front surface.  The time dependence of the relative output is
shown before and after the irradiation.}
\label{eps-efc-radtime}
\end{center}
\vspace{5mm}
\end{figure}


Radiation damage tests on optical reflector~\cite{efc-PengA384} and
epoxy-based optical glue~\cite{efc-HuangA385} which are used in the
BGO detector assembly were performed.  The reflectance of optical
white fluorocarbon reflector, Goretex~\cite{Goretex}, was monitored to
see any effect of radiation damage.  No radiation damage was observed
for the maximum equivalent dose of 8.6 Mrad.  The epoxy-based optical
glue was found later to be inadequate to bond the photodiode surface
on a BGO crystal.  An RTV type of glue, KE45T, was proved to be good.
Its radiation hardness was checked acceptable up to 10 Mrad maximum
equivalent dose.\\

Photodiodes are good for a radiation dose of about 50 krad and the
least radiation-hard element.  They are, however, protected behind the
BGO crystals, the container, and the accelerator magnets. In case they
are radiation damaged, they can be replaced relatively easily.

\subsubsection{Mechanical structure}

The finer lateral segmentation can provide the better position
resolution. The segmentation, however, is limited by front-end
electronics.  The detector is segmented into 32 in $\phi$ and 5 in
$\theta$ for both the forward and backward cones.  A three dimensional
view of the crystal arrangement is shown in
%%%%%%%%%%%%%%%%(Fig. 1 efc-Ueno)	
 Fig. \ref{eps-efc-structure}. 
%%%%%%%%%%%%% Figure 4 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=efc/efcdet.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{An isometric view of the BGO crystals of the forward and
backward EFC detectors.}
\label{eps-efc-structure}
\end{center}
\vspace{8mm}
\end{figure}

The schematic side view of the forward EFC is shown in	
%%%%%%%%%%%%%%%%(Fig. 2.8 Progress97)
Fig. \ref{eps-efc-sideview}. The distance between the front surface of
the detector and the interaction point is 60 and 43.5 cm in the
forward and backward EFCs, respectively. The inner bore radius of the
detector is 6.5 cm.\\

%%%%%%%%%%%%% Figure 5 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=efc/efc-f1.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{Side view of the mounting of forward EFC.}
\label{eps-efc-sideview}
\end{center}
\vspace{5mm}
\end{figure}

The BGO crystals are housed in a bucket-shape container made of 1
mm-thick stainless steel, the inner bore of which is the beam pipe.
The container is attached to the front surface of the compensation
solenoids. Since the space allocated for EFC is limited, the radiation
lengths of the forward and backward crystals are 12 and 11,
respectively.  Front-end circuits for photodiodes and preamps as well
as miscellaneous instruments are installed in the space between the
magnet surface and the backend of the BGO crystals which is 2.7 and
2.0 cm for the forward and backward EFC detectors, respectively.

\subsubsection{Light collection}

EFC points approximately to the interaction point to make a tower
geometry.  The crystals are shaped in trapezoid and their dimensions
of the front and rear surfaces are significantly different. As a
result, scintillation lights generated along the crystal axes are not
collected uniformly. A Monte Carlo simulation by GEANT indicates that
a good uniformity of light collection is important to get a good
energy resolution.  Therefore, all crystals were matt-treated to
achieve a uniform light collection. The largest surface of the forward
crystals and the second largest surface of the backward crystals were
made dim. All other surfaces were polished. The nonuniformity of light
collection for various types of crystals was reduced to about 10 \%
level~\cite{efc-Ueno}. After the matt treatment, all the crystals were
wrapped by a 100 $\mu$m Teflon sheet and a 20 $\mu$m aluminized Mylar
sheet.

\subsection{Electronics}

The readout chain of EFC is shown in Fig.~\ref{eps-efc-elect}. The
major parts are:

\begin{itemize}
  \item front-end amplifier,

  \item rear-end receiver and digitizer, and

  \item trigger system (see Sec. 11.5).

\end{itemize}
\begin{figure}[hbt]
\vspace{10mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=efc/efcdaq_0912.eps,width=15cm,angle=0}}
\vspace{0mm}
\caption{Block diagram of EFC readout electronics.}
\label{eps-efc-elect}
\end{center}
\vspace{5mm}
\end{figure}


\subsubsection{Front-end amplifier} 

The circuit diagram for the preamplifier and shaper for a photodiode
is shown in Fig.~\ref{efc-ecl-circuit}. The photodiode used is
Hamamatsu S5106. Its active area is 5 $\times$ 5 mm$^2$. Scintillation
lights generated in each BGO crystal are collected by two photodiodes
except for the crystals in the two inner-most layers, where only one
photodiode collects lights in each crystal. \\

%%%%%%%%%%%%% Figure 7 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=efc/preampsch.eps,width=15cm,angle=0}}
\caption{Schematic diagram of the preamplifier-shaper circuit.}
\label{efc-ecl-circuit}
\end{center}
\vspace{5mm}
\end{figure}

The printed circuit boards (PCB) for the front-end electronics are
stacked together and mounted right behind the rear face of the BGO
crystals.  The boards include a board for housing photodiodes and
distributing their biases, a board to house LEDs and to drive them at
each crystal, and a mother board to mount 10 preamplifiers.  The rise
time of the signal of the preamplifier and shaper is about 300 ns. The
gain is 6 V/pC and 12 V/pC for the forward and the backward EFC,
respectively. \\

Signals are sent differentially through a cable of 16
individually-shielded twisted pairs of 16 m in length to the receivers
located outside of the Belle detector.  The cable also supplies powers
for preamplifiers and biases for photodiodes, and sends pulses for
pulsing a preamplifier and LED.  Each crystal can be pulsed by a light
pulse through fiber cables. \\


\subsubsection{Rear-end receiver and digitizer} 

The receiver receives analog signals through a transformer which
cancels a common-mode noise in the twisted-pair cable.  After the gain
adjustment, the signal is split into two.  One is sent to a charge
integrator and the other to a sum amplifier which makes an analog sum
of preamplifier signals from a trigger cell.  The cell is a group of
two or four BGO crystals. \\

The summed signal is sent to a constant-fraction discriminator (CFD)
for generating gates for the charge integrator and the trigger.  The
integrator integrates the analog signal within the gate and produces
an ECL signal whose width is proportional to the charge integrated.
It is implemented by LeCroy MQT300A chip.  The ECL signal is sent
through a 30-m long twisted-pair cable to the electronics hut. \\

The ECL signal goes through a fanout located in the hut, making
another copy.  One signal is sent to a time-to-digital converter (TDC)
for local EFC DAQ.  The other is sent to TDC for Belle global DAQ.
TDC used is a multi-hit Fastbus TDC of LeCroy 1887S and is operated in
a common-stop mode. \\

\subsection{Performance}

The energy sum spectra for Bhabha events show a correlation between
the forward and backward EFC detectors.  A clear peak at 8 GeV with an
rms resolution of 7.3 \% is seen for the forward EFC, while a clear
peak at 3.5 GeV with an rms resolution of 5.8 \% is seen in the
backward EFC.  These results are compatible with the beam test
results~\cite{efc-beamtest} and are slightly worse than those obtained
by a GEANT Monte Carlo simulation.  The discrepancies are due to dead
channels and crystal-to-crystal nonuniformity.  An expected counting
rate for Bhabha events is a few kHz at an ultimate luminosity of
10$^{34}$ cm$^{-2}$s$^{-1}$.\\

EFC has provided a fast online feedback about the luminosity and
background rates to the KEKB operations group.
%%%%%%%%%%%%%(Fig. 8 efc-Ueno)
Fig. \ref{eps-efc-operation} shows an operational history of a typical
beam fill at KEKB. The coincidence and accidental rates of the forward
and the backward EFC are shown as a function of time in seconds
together with $e^+$ (LER) and $e^-$ (HER) beam currents.  During the
injection period from 0 to 1800 s accidental Bhabha rates are quite
high, but after the injection the accidental rate is very low.
Singles rates of EFC can provide very useful information for
diagnosing of background sources.

%%%%%%%%%%%%% Figure 8 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=efc/time_plot_2.eps,width=15cm,angle=0}}
\caption{An operational history of a typical beam fill of KEKB.  The
top figures show e$^+$ and e$^-$ beam current, and the bottom figures
the coincidence rate of the forward and backward detectors (left) and
their accidental rates (right) of EFC.}
\label{eps-efc-operation}
\end{center}
\vspace{10mm}
\end{figure}
