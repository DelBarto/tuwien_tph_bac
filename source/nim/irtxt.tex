\section{Interaction Region}

\subsection{Beam Crossing Angle}

The layout of the interaction region is shown in	
%%%%%%%%%%%%%%(Fig. 2.1 TDR95)
Fig. \ref{eps-ir-layout}~\cite{TDR95}.  The beam crossing angle of
$\pm$11 mr allows us to fill all RF buckets with the beam and still
avoid parasitic collisions, thus permitting higher luminosity. Another
important merit of the large crossing-angle scheme is that it
eliminates the need for the separation-bend magnets, significantly
reducing beam-related backgrounds in the detector.  The risk
associated with this choice of a non-zero crossing angle is the
possibility of luminosity loss caused by the excitation of
synchro-beta resonances~\cite{KEKB}.\\

%%%%%%%%%%%%% Figure 1 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{-20mm}
\begin{center}
%\epsfysize=10cm
%\epsfbox{eps/irlayout.eps}
\centerline{\psfig{file=ir/ir_mori.eps,width=12cm,angle=-90}}
\caption{Layout of the interaction region for the beam crossing angle
of $\pm$ 11 mr.}
\label{eps-ir-layout}
\end{center}
\vspace{0mm}
\end{figure}

The low energy beam line ($e^+$) is aligned with the axis of the
detector solenoid since the lower-momentum beam particles would suffer
more bending in the solenoid field if they were off-axis.  This
results in a 22 mr angle between the high energy beam line ($e^-$) and
the solenoid axis.

\subsection{Beam-line Magnets near the Interaction Point}

The final-focus quadrupole magnets (QCS) are located inside the field
volume of the detector solenoid and are common to both beams.  In
order to facilitate the high gradient and tunability, these magnets
are superconducting at the expense of a larger size.  In order to
minimize backgrounds from QCS-generated synchrotron radiation, their
axes are aligned with the incoming $e^+$ and $e^-$ beams.  This
requires the radius of the backward-angle region cryostat to be larger
than that of the one in the forward-angle region.  The inner aperture
is determined by the requirements of injection and the need to avoid
direct synchrotron radiation incident on the beam pipe inside the
cryostats.  The $z$-positions are determined by the detector
acceptance (17$^o \leq \theta \leq$ 150$^o$).\\

To minimize solenoid-field-induced coupling between the $x$ and $y$
beam motions, superconducting compensation solenoid magnets are
located near the interaction point (IP), occupying the same cryostat
as the QCS magnets.  Since the $\int B_z dz$ between IP and QCS is
required to be nearly zero, these magnets run as high as 4.8 T.  The
cryostats for QCS and the compensation magnets are supported from a
movable stage that provides a common support base for all the
accelerator magnets located in the experimental hall.\\

The QC1 magnets are located outside the QCS cryostats and help provide
the vertical focus for the high energy beam only.  Although these are
normal conductor magnets with an iron return yoke, a special design is
necessary because of the small beam separation in this region. The one
in the forward region is a half-quadrupole with the iron septum.  In
order to reduce the synchrotron radiation background from the incoming
beam, the backward region QC1 is a special
full-quadrupole~\cite{KEKB}.  The locations are chosen so as to avoid
the leakage field of the detector solenoid.

\subsection{Beam Pipe}

The precise determination of decay vertices is an essential feature of
the Belle experiment.  Multiple coulomb scattering in the beam-pipe
wall and the first layer of the silicon detector are the limiting
factors on the $z$-vertex position resolution, making the minimization
of the beam-pipe thickness a necessity.  Moreover, since the vertex
resolution improves inversely with the distance to the first detection
layer, the vertex detector has to be placed as close to the
interaction point as possible and, thus, to the beam pipe wall.  This
is complicated by the fact that the beam pipe at the interaction
region is subjected to beam-induced heating at levels as high as a few
hundred watts.  This requires an active cooling system for the beam
pipe and a mechanism for shielding the vertex detector from this
heat.\\

%%%%%%%%%%%%%%(Fig. 2.2 TDR95)
Figure \ref{eps-Be-beampipe} shows the cross section of the beryllium
beam pipe at the interaction point.  The central part (-4.6 cm $\leq z
\leq$ 10.1 cm) of the beam pipe is a double-wall beryllium cylinder
with an inner diameter of 40 mm.  A 2.5 mm gap between the inner and
outer walls of the cylinder provides a helium gas channel for cooling.
The machine vacuum is supported by the 0.5 mm thick inner wall.  The
outer wall is also 0.5 mm thick. The beryllium central section is
brazed to aluminum pipes that extend outside of the collision region
as shown in
%%%%%%%%%%%%%%(Fig. 2.3 TRD95)
Fig. \ref{eps-ir-mask}.  The conical shape of the aluminum beam pipe
allows the synchrotron x-rays generated in the QCS and QC1 magnets to
pass through the detector region without hitting the beam pipe wall.
The helium-gas cooling is adopted instead of water in order to
minimize the material in the beam pipe.\\

%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
\epsfysize=10cm
%\epsfbox{beampipe_2.ps}
\centerline{\bf{\psfig{file=ir/beampipe_2.eps,width=10cm,angle=-90}}}
\caption{The cross section of the beryllium beam pipe at the
interaction point.}
\label{eps-Be-beampipe}
\end{center}
\vspace{3mm}
\end{figure}

%%%%%%%%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{-10mm}
\begin{center}
\epsfysize=10cm
\centerline{\bf{\psfig{file=ir/ip_mask_mori.eps,width=13cm,angle=-90}}}
\vspace{-15mm}
\caption{The arrangement of the beam pipe and horizontal masks.}
\label{eps-ir-mask}
\end{center}
\vspace{10mm}
\end{figure}

Assuming a uniformly distributed 100 W heat load on the inner wall,
the maximum temperature increase for the inner (outer) beryllium wall
is calculated to be 25 (5) $^o$C for a 2 g/s He flow velocity at a 1.5
atm pressure and a 0.0007 atm pressure drop. The calculation assumes
that the aluminum tubes in contact with the ends of the beryllium
section are maintained at a fixed temperature by an independent
cooling system.  The total material thickness of the central beryllium
section is 0.3 \% of a radiation length.  Outside the outer beryllium
cylinder, a 20 $\mu$m thick gold sheet is attached in order to reduce
the low energy x-ray background ($<$ 5 keV) from the high energy ring.
Its thickness corresponds to 0.6 \% of a radiation length.\\
%%%%%%%%%%%

\subsection{Beam Background}

With the elimination of the separation-bend magnets near IP, the
synchrotron radiation backgrounds are not so severe as they were for
TRISTAN, where the critical energy was higher.  The apertures of the
beam pipe and masks (SR-Mask) near IP were designed so that
synchrotron radiation from QCS and QC1 go through without hitting
them. In these synchrotron radiation calculations, we protect against
beam tails out to 10 $\sigma_x$ and 30 $\sigma_y$. The beam-beam
simulations indicate no long tails in the interaction region. Mask-A
shields the beryllium beam pipe from back-scattered photons.  Although
some photons from QC2 and QC3 can hit SR-Mask, they have a critical
energy less than 2 keV and are easily absorbed in the material of the
mask.  The masks are gold-plated for this purpose. Photons from
upstream magnets far from IP are intercepted by movable masks
installed just upstream of QC3.\\

Particle backgrounds were expected to be more critical at KEKB. A
DECAY TURTLE calculation indicates that the rate of spent particles
from both beams directly hitting the beam pipe between the two
cryostats is 130 kHz for 10$^{-9}$ torr vacuum.  Particle-Masks were
installed outside the beam pipe to reduce the particle background.
Movable masks installed in the arcs and the non-IR straight sections
cut off the beam tails far from IR. These masks could reduce radiation
levels at the detector during injection.\\
%%%%%%%%%%%%%%%%
