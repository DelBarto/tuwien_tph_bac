The paper is organised as follows:

total.tex - main LaTeX file which includes all other sections

fronttxt.tex - front text including the author list
inttxt.tex - introduction
irtxt.tex - interaction region
efctxt.tex - extreme forward calorimeter
svdtxt.tex - silicon vertex detector
cdctxt.tex - central drift chamber
acctxt.tex - aerogel Cerenkov counter system
toftxt.tex - time of flight counters
ecltxt.tex - electromagnetic calorimetry
klmtxt.tex - K_L and muon detection system
magtxt.tex - solenoid magnet
trgtxt.tex - trigger
daqtxt.tex - data aquisition
cmptxt.tex - offline computing system

reftxt_ack.tex - references and acknowledgements

Figures for each of the files above are in a special folder, so there are

int/
ir/
efc/
svd/
cdc/
acc/
tof/
ecl/
klm/
mag/
trg/
daq/
cmp/

