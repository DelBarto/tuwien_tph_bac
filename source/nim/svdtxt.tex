\section{Silicon Vertex Detector, SVD}

\subsection{General Characteristics}
A primary goal of the Belle experiment is to observe time-dependent CP
asymmetries in the decays of $B$ mesons.  Doing so requires the
measurement of the difference in $z$-vertex positions for $B$ meson
pairs with a precision of $\sim 100~\mu$m.  In addition, the vertex
detector is useful for identifying and measuring the decay vertices of
$D$ and $\tau$ particles.  It also contributes to the tracking.

Since most particles of interest in Belle have momenta of 1~GeV/$c$ or
less, the vertex resolution is dominated by the multiple-Coulomb
scattering.  This imposes strict constraints on the design of the
detector.  In particular, the innermost layer of the vertex detector
must be placed as close to the interaction point as possible; the
support structure must be low in mass but rigid; and the readout
electronics must be placed outside of the tracking volume.
%%DRM I don't think this sentence is needed.
%The last is not a problem for 
%$\phi$-coordinate measurement but for the z-coordinate measurement it 
%requires additional readout traces orthogonal to the z-readout strips.

The design must also withstand large beam backgrounds.  With the
anticipated high luminosity operation of KEKB, the radiation dose to
the detector due to beam background is expected to be 30 kRad/y at the
full design current.  Radiation doses of this level both degrade the
noise performance of the electronics (the readout fails outright at
$\sim 200$~kRad) and induce leakage currents in the silicon
detectors. In addition, the beam backgrounds induce large single-hit
count rates. The electronic shaping time---currently set to
1000~ns---is determined by a tradeoff between the desire to minimize
count-rate and leakage current effects, which argue for short shaping
times, and input-FET noise of front-end integrated circuits, which is
minimized with longer shaping times.

Figure \ref{eps-svd-config} shows side and end views of SVD.  It
consists of three layers in a barrel-only design and covers a solid
angle $23^o < \theta < 139^o$ where $\theta$ is the angle from the
beam axis. This corresponds to
%%%%%
86\% of the full solid angle.  The radii of the three layers are 30
mm, 45.5 mm asnd 60.5 mm.  Each layer is constructed from independent
ladders.  Each ladder comprises double-sided silicon strip detectors
(DSSDs) reinforced by boron-nitride support ribs.  The design uses
only a single type of DSSD, which reduces the cost of detector
production, minimizes the amount of detector development work, and
streamlines testing and bookkeeping during production.  The benefit
also extends to hybrid production and ladder assembly, where only a
single type of hybrid is necessary and the design of the ladder
assembly fixtures is greatly simplified.

The readout chain for DSSDs is based on the VA1 integrated
circuit~\cite{svd-va1}.  The VA1 has excellent noise performance (200
e$^-+8$ e$^-$/pF) and reasonably good radiation tolerance of
200~kRad. The back-end electronics is a system of flash
analog-to-digital converters (FADCs), digital signal processors
(DSPs), and field programmable gate arrays (FPGAs), mounted on
standard 6U VME boards.  DSPs perform on-line common-mode noise
subtraction, data sparsification and data formatting.
%%%%%%%%%%%%% Figure 1 %%%%%%%%%%%%%%
\begin{figure}[hbt]
%\vspace{8mm}
\begin{center}
\centerline{\psfig{file=svd/svd-config_1117.eps,width=14cm,angle=0}}
%\vspace{3mm}
\caption{Detector configuration of SVD.}
\label{eps-svd-config}
\end{center}
%\vspace{0mm}
\end{figure}

\subsection{Double-sided Silicon Detector, DSSD}
We use S6936 DSSDs fabricated by Hamamatsu Photonics (HPK).  These
detectors were originally developed for the DELPHI micro-vertex
detector~\cite{svd-DELPHI}.  Each DSSD consists of 1280 sense strips
and 640 readout pads on opposite sides.  The sense strips are biased
via 25~M$\Omega$ polysilicon bias resistors. The $z$-strip pitch is
42~$\mu$m and the $\phi$-strip pitch is 25~$\mu$m.  The size of the
active region is $53.5 \times 32.0$~mm$^2$ on the $z$-side and $54.5
\times 32.0$~mm$^2$ on the $\phi$-side.  The overall DSSD size is
$57.5 \times 33.5$~mm$^2$.  In total 102 DSSDs are used.

For the $z$-coordinate ($z$ is the approximate beam direction)
measurement, a double-metal structure running parallel to $z$ is
employed to route the signals from orthogonal $z$-sense strips to the
ends of the detector. Adjacent strips are connected to a single
readout trace on the second metal layer which gives an effective strip
pitch of 84 $\mu$m. The ohmic side is chosen to be on the $z$-side
and a $p$-stop structure is employed to isolate the sense strips.  A
relatively large thermal noise (600 e$^-$) is observed due to the
common-$p$-stop design.

On the $\phi$ side, only every other sense-strip is connected to a
readout channel.  Charge collected by the floating strips in between
is read from adjacent strips by means of capacitive charge division.

\subsection{Mechanical Structure}

SVD consists of 8, 10 and 14 ladders in the inner, middle and outer
layers, respectively.  The total number of full ladders is 32.  The
ladders are arranged to include some overlap regions in the $r$-$\phi$
plane to help the internal alignment among the sensors.  The fraction
of the overlap region is 9.7, 3.8, and 8.7 \% for the inner, middle,
and outer layers, respectively.  The ladders are supported by a
structure consisting of end rings, support cylinders, and an outer
cover, as shown in Fig. \ref{eps-svd-support}.


%%%%%%%%%%%%% Figure 18 %%%%%%%%%%%%%%
\begin{figure}[hbt]
%\vspace{8mm}
\begin{center}
\centerline{\psfig{file=svd/svd-support.eps,width=14cm}}
%\vspace{6mm}
\caption{Overview of the SVD supporting system.}
\label{eps-svd-support}
\end{center}
%\vspace{3mm}
\end{figure}


%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
%\begin{figure}[hbt]
%\vspace{3mm}
%\begin{center}
%\epsfysize=10cm
%\epsfbox{eps/svdladder.eps}
%\caption{Schematic view of silicon detector ladders layer 1 (inner), layer 2 (m%iddle) and layer 3 (outer).}
%\label{eps-svd-ladder}
%\end{center}
%\vspace{3mm}
%\end{figure}


\subsubsection{Ladder}
Each ladder is made of two half-ladders that are electrically
independent, but mechanically joined by support ribs.  The support
ribs are made of boron nitride (BN) sandwiched by carbon-fiber
reinforced plastic (CFRP).  This arrangement is necessary to avoid
directly gluing CFRP to DSSD since CFRP is much stiffer but
conductive, and induces noise to the detector.  The dimensions of the
BN and CFRP are 6.25$\sim$6.65~mm high $\times$ 0.5~mm wide and 3.7~mm
high $\times$ 0.15~mm wide, respectively, giving a stiffness of
$EI=3.5\sim 4.0$~Nm$^2$.  The average thickness of the rib is
approximately 0.15 \% of the radiation length.  To reduce the number
of fixture types needed for ladder assembly, the ladder design is
modular, with each module made as symmetrically as possible. As a
result, only two basic module types are needed: a single detector
module and a double detector module.  Each module consists of a
detector unit and a hybrid unit, as shown in
Fig. \ref{eps-svd-config}.
%A schematic view of the SVD ladders is shown in	 
%Fig. \ref{eps-svd-ladder}.	 

A detector unit consists of either a single detector or two detectors
with an overlap joint.  In order to minimize the noise of the
double-detector modules, different sides of the two detectors are
connected---meaning that $p$-strips on one detector are wire-bonded to
$n$-strips on the other detector. This is possible because the
detectors incorporate integrated coupling capacitors and the readout
system can process pulses of either polarity.

A hybrid unit is produced by gluing two hybrids back-to-back to read
both sides of DSSD.  Since the preamplifier chips are a heat source,
careful attention is paid to the thermal path way through the hybrid,
across glue joints, and into the heat-sink. Boron-nitride-loaded epoxy
is used to join the two hybrids due to good heat conductivity of $\sim
0.8$~W/m$\cdot$K.

The heat-sink is made of aluminum nitride, whose high thermal
conductivity (150--200 W/m$\cdot$K) and low coefficient of thermal
expansion (CTE) (2-3 ppm/K) make it an attractive option for both heat
conduction and mechanical support.  The thermal conductivity is
further enhanced by embedding two heat-pipes
%
\footnote{The HP-NB series of heat-pipes produced by Thermacore
Inc. 780 Eden Road, Lancaster, PA 17601 USA are used. The heat-pipes
transfer heat by evaporation and condensation of water. Internal
pressure of the heat-pipe is saturation pressure of water so that the
water will vaporize with any thermal input.}
%
in the heat-sink.  Measurements indicate that the total temperature
drop across the heat-sink will be 1.8$^\circ$C at normal preamplifier
power levels.
%This design maximizes thermal performance while minimizing cost.
The attachment of the hybrid to the heat-sink is also by means of 
the boron-nitride loaded epoxy.

\subsubsection{Support structure}
The ladders are mounted on the forward and backward end-rings, which
provide mechanical support and cooling.  The positions of the ladders
are fixed by cylindrical pins, which penetrate cylindrical holes in
the heat-sinks and the end-rings.  The end-rings are made of aluminum,
which was chosen for its low mass, machinability, and thermal
conductivity.  Cooling tubes, in which 15~$^\circ$C water is
circulating, are embedded in the end-ring.

When the SVD electronics are turned on, thermal effects cause the
ladder and end-ring to expand by estimated amounts 3~$\mu$m and
40~$\mu$m, respectively.  To accommodate this expansion, a sliding
mechanism is introduced to allow the ladder to move with respect to
the end-ring in the longitudinal ($z$) direction.  At the backward
side, the pins and holes have the same diameter of 5~mm.  On the other
hand at the forward side, the diameters of pin and hole in the
end-ring are 3 mm, but the diameter of the hole in the heat-sink is 5
mm.  A spring-loaded attachment pushes the 3-mm pin to one side of the
5-mm hole in the heat-sink.  This mechanism ensures that the forward
end of the ladder is able to slide in the longitudinal direction but
is not able to move in the transverse direction.  The principle of
this design is that the ladder end will slide before the ladder bows,
thus preserving the essential planar nature of the ladder module.  The
precision of the machining and the assembly is about 50~$\mu$m and the
position of DSSDs were measured with a precision of $\pm10~\mu$m.

%%DRM resume here

The end-rings are supported by the forward and backward support
cylinders, which are made of 2.5-mm-thick CFRP.  These support
cylinders are connected by the outer cover, which is made of
0.5-mm-thick CFRP.  The outer cover and the support cylinders form a
single overall support envelope of considerable stiffness, $EI = 1
\times 10^4$~Nm$^2$. Based on ANSYS simulation, the gravitational sag
at the center of the structure is expected to be about 10~$\mu$m.
This structure is supported at three points on the forward (one point)
and backward (two points) end plates of CDC.  The backward end of the
backward support cylinder is also fixed to CDC by means of a thin
diaphragm, which rigidly constrains the $r$ and $\phi$ location of
SVD, but allows easy motion in the $z$ direction.  This longitudinal
degree of freedom accommodates thermal expansion and contraction of
SVD and CDC as well as changes in the length of CDC brought about by
changes in atmospheric pressure.

  The beam pipe inside SVD is also supported by the CDC end plates
through diaphragms that are independent of SVD.  The beam pipe support
is designed such that the heat load and any vibrations originating
from the pipe and its cooling system do not affect the performance of
the SVD system.  The end rings, the end ring flanges, and the outer
cover can all be divided into two (clamshell) halves in $r$-$\phi$.
This makes it possible to assemble the SVD ladders in a tight space
around the beam pipe.

\subsection{Front-end Electronics}

%DRM 
Signals from each side of DSSDs are read out by electronics comprising
VA1 front-end integrated circuits~\cite{svd-va1} mounted on ceramic
hybrids.  Each hybrid holds five 128-channel VA1 chips.  Two hybrid
cards are connected to an interface card (designated ABC) by 0.025''
pitch multiconductor cables terminated in nanostrip
connectors~\cite{Omnetics}.  ABCs are connected to a repeater (CORE)
system through 2-m-long flat cables.  Analog current signals from VA1
are converted to voltage signals and buffered in the repeater system
before being transmitted to FADCs in the Belle electronics hut over
30-m-long shielded differential coaxial cables.

A second signal data path, which is bidirectional, transmits the
level-0 trigger signal (the ``hold'' pulse) and various digital
clocking pulses that drive the readout to VA1.  This path is also used
to carry slow control and monitor data between the hybrids and an
online computer.

Figure \ref{eps-svd-core} shows a schematic drawing of the SVD readout system.
%%%%%%%%%%%%% Figure 6 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\centerline{\psfig{file=svd/svd-core.eps,width=12cm,angle=0}}
\caption{Schematic drawing of the SVD readout system.}
\vspace{5mm}
\label{eps-svd-core}
\end{center}
\vspace{3mm}
\end{figure}


\subsubsection{VA1 integrated circuit}

The VA1 chip~\cite{svd-va1} is a 128-channel CMOS integrated circuit
fabricated in the Austrian Micro Systems (AMS) 1.2-$\mu$m CMOS
process.  VA1 was specially designed for the readout of silicon vertex
detectors and other small-signal devices.  It has excellent noise
characteristics (ENC = 200 $e^-$ + 8 $e^-$/pF at 1 $\mu$s shaping
time), and consumes only 1.2 mW/channel.

A block diagram of the VA1 chip is shown in	 
%%%%%%%%%%%%%%(Fig. 6 SVD98)
Fig. \ref{eps-VA1chip}.  Signals from the strips are amplified by
charge sensitive preamplifiers, followed by $CR-RC$ shaping circuits.
The outputs of the shapers are fed to track and hold circuits, which
consist of capacitors and CMOS switches. Under normal conditions, the
switches are closed and the voltages on the capacitors simply follow
the shaper outputs.

  When an external trigger causes the HOLD state to be asserted, the
analog information from all channels is captured on storage capacitors
and then sequentially read using on-chip scanning analog multiplexers.
The multiplexers from the five chips on a single hybrid are daisy
chained and routed to fast analog-to-digital converters (FADCs),
located in the electronics hut about 30 m away from the detector.
Operation of the multiplexer is controlled by a shift register having
one bit per channel.  This simple ``track-and-hold'' architecture is
generally well suited to the Belle DAQ system.

Although input-FET noise considerations of VA1 argue for a somewhat
longer shaping time, the shaping time for VA1s is adjusted to 1~$\mu$s
to minimize the occupancy due to the beam background.  Since this time
is shorter than the nominal Belle level-1 trigger latency
($2.2~\mu$s), a pretrigger signal from the TOF system is used to
assert the VA1 HOLD line until the level-1 signal is formed.  If a
level-1 signal does not occur within $1.2~\mu$s, the HOLD line is
deasserted and the system is immediately ready for another event. If
the level-1 does fire, a normal readout sequence ensues.
 
%%%%%%%%%%%%% Figure 4 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\centerline{\psfig{file=svd/svd-va1.eps,width=14cm,angle=0}}
\vspace{3mm}
\caption{Block diagram of the VA1 chip.}
\label{eps-VA1chip}
\end{center}
\vspace{3mm}
\end{figure}

 VA1 also provides for testing the inputs of individual channels.
This is done by using a shift-register-controlled switching network on
the input side of the chip to sequentially couple an
externally-provided test pulse into each channel.  When in the test
mode, the input and output shift registers track on another, allowing
one to observe the shaper output for each channel directly by holding
the hold switches in the track (closed) position.  Accurate
calibration can be achieved by using a precision external calibration
capacitor.

Radiation hardness tests of VA1 indicate that it is radiation tolerant
to levels of order 200 kRad.  Prior to complete chip failure, a
fractional increase in noise of 1.6 \%/kRad was observed.  After 150
kRad irradiation the noise level for the $p$-$n$ flip sensors
($C_{strip}$ = 29 pF) seems to be marginal.

The detector capacitance, measured noise (ENC, electron noise charge),
and signal-to-noise ratio (S/N) for a normally incident minimum
ionizing particle are summarized in
%%%%%%%%%%%%%%(Table 2 SVD98)
 Table \ref{svd-C-ENC-SN}. 
%%%%%%%%%%%%%%%%% Table 2 %%%%%%%%%%%%%%%%%
\begin{table}
\caption{Detector capacitance (C), expected noise (ENC) and 
signal-to-noise ratio (S/N) at 0 kRad.}
\begin{center}
\vspace{3mm}
\begin{tabular}{c| c c c c}
\hline \hline
Half-ladder & Side & C (pF) & ENC (e$^-$) & S/N\\
\hline
 & p & 7 & 400 & 47\\
\raisebox{1.5ex}[0pt]{S6936 $\times$ 1} & n & 22 & 1000 & 19\\
\hline
S6936 $\times$ 2 & p-n & 29 & 1100 & 17\\
\hline \hline
\end{tabular}
\end{center}
\label{svd-C-ENC-SN}
\vspace{10mm}
\end{table}


The dependence of expected S/N on radiation dose is given in 
%%%%%%%%%%%%%%(Table 3 SVD98)
 Table \ref{svd-rad}. 
%%%%%%%%%%%%%%%%%%%% Table 3 %%%%%%%%%%%%%%%%%%
\begin{table}
\caption{Expected signal-to-noise ratio after irradiation.
The radiation dose values shown are for the inner layer.	
The radiation doses in the middle and outer layers are assumed to 
scale as $r^{-1}$.}
\begin{center}
\vspace{3mm}
\begin{tabular}{c| c| c c c c}
\hline \hline
Half-ladder & Side & 0 kRad & 50 kRad & 100 kRad & 150 kRad\\
\hline
Inner & p & 47&31&23&18\\
(1 DSSD) & n &19&16&13&11\\
\hline
Middle fwd & p & 47&35&28&23\\
(1 DSSD) & n &19&17&15&13\\
\hline
Middle bwd & \\
(2 DSSDs) &\raisebox{1.5ex}[0pt]{p-n} &\raisebox{1.5ex}[0pt]{18}&\raisebox{1.5ex}[0pt]{16}&\raisebox{1.5ex}[0pt]{14}&\raisebox{1.5ex}[0pt]{12}\\
\hline
Outer &	 \\
(2 DSSDs) &\raisebox{1.5ex}[0pt]{p-n} &\raisebox{1.5ex}[0pt]{18}&\raisebox{1.5ex}[0pt]{17}&\raisebox{1.5ex}[0pt]{15}&\raisebox{1.5ex}[0pt]{14}\\
\hline \hline
\end{tabular}
\end{center}
\label{svd-rad}
\vspace{10mm}
\end{table}



\subsubsection{Hybrid}

%%%%%%%%%%%%%%%(Fig. 7 SVD98)
The hybrids comprise VA1 IC's and a small number ($\sim 25$) of
passive surface-mount (SM) components mounted on thermally conductive
aluminum-nitride ceramic substrates.  The inputs of VA1s are
wire-bonded to the strips of DSSDs. The passive components are used
for bypassing, calibration, bias adjustment, and clock
termination. Connections to the external system were made via
``nano-strip'' connectors, which were selected to accommodate space
restrictions (they require only 1.3~mm of vertical clearance).  The
vendor of these connectors~\cite{Omnetics} also provides matching
ribbon-cable versions that are used to carry the connections to the
ABC cards.


%%%%%%%%%%%%%% Figure 5 %%%%%%%%%%%%%%
%\begin{figure}[hbt]
%\vspace{3mm}
%\begin{center}
%\epsfysize=10cm
%\epsfbox{eps/svdhybrid.eps}
%\caption{Plan view of the hybrid of SVD.}
%\label{eps-svd-hybrid}
%\end{center}
%\vspace{3mm}
%\end{figure}

\subsubsection{Repeater system, CORE}
A control and readout (CORE) system~\cite{svd-TanakaA432} is required
to provide the following functions:
\begin{itemize}
\item Analog control of the VA1 front-end chips:\\
The CORE system provides bias voltages and currents.
The ability to adjust the bias parameters remotely is particularly 
important in view of the possibility that the optimal values may 
change with time as the chips are irradiated.\\
\item Digital control of the VA1 front-end chips:\\
Various pulses and digital levels are used to control the
data-acquisition sequence of the VA1 chips.  The CORE serves to
condition, buffer, and distribute these signals to VA1s.\\
\item Analog buffering and amplification:\\
Since the cable length between the detector and the electronics hut is
about 30~m, CORE incorporates differential gain/buffer stages to
provide immunity to electromagnetic interference.  To meet the readout
speed requirements of Belle DAQ, CORE is capable of supporting MUX
scan rates up to 5 MHz.  This allows a single FADC channel to scan all
channels of a 640-channel hybrid in 128~$\mu$s.\\
\item Monitoring of analog levels:\\
CORE monitors a large number ($68\times4\times8$) of analog 
levels to verify the bias parameters of	 VA1 and buffers, 
and the hybrid temperatures.
\end{itemize}
%
%%%%%%%%%%%%% Figure 6 %%%%%%%%%%%%%%
%\begin{figure}[hbt]
%\vspace{3mm}
%\begin{center}
%\epsfysize=10cm
%\epsfbox{eps/svdcore.eps}
%\caption{SVD CORE system.}
%\label{eps-svd-core}
%\end{center}
%\vspace{3mm}
%\end{figure}

%%%%%%%%%%%%%%%
As shown in Fig. \ref{eps-svd-core}, each CORE system module consists
of a backplane motherboard (MAMBO), four boards for readout signal
buffering and front-end control (REBOs), a board for monitoring
(RAMBO), and eight interface cards (ABCs).  There are a total of eight
cooled aluminum and copper housings (DOCKs) which hold the CORE system
boards; four each in the forward and backward regions of the detector.
The temperature at the surface of DOCK is maintained at
$\sim$23~$^\circ$C.

ABCs, which are physically situated about 20 cm from the front-end
hybrids, provide a passive electrical interface between the repeater
system and the hybrids.  In addition, they serve as passive one-to-two
fanouts for the digital control signals running from the repeater to
the hybrids, thereby reducing the number of cables required.  ABCs
also facilitate ready disconnection for maintenance.

%%%%%%%%%%%%%%%(Fig. 16 SVD98)
%Figure \ref{eps-svd-mambo} shows the block diagram of MAMBO which is 
%an interface board for ABC, REBO, RAMBO and cables to the Belle 
%electronics hut.	 
MAMBO is an interface board for ABC, REBO, RAMBO and cables to the
Belle electronics hut.  MAMBO transmits digital signals to and from
TTMs using the low-voltage differential signaling (LVDS) standard.
LVDS reduces EMI to the analog part of CORE as well as to other parts
of the detector.  Mounted on MAMBO are ADCs to monitor temperature,
the bias currents and voltages, the amplitude of a calibration test
pulse, and the logic levels for the digital signals.

REBO consists of an analog block and a digital block.  The analog
block comprises digital-to-analog converters (DACs),
voltage-to-current converters, current-to-voltage converters and
amplifiers.  The digital block controls DACs in the analog block, and
buffers the digital signals to the front-end electronics.


\subsection{Back-end Electronics}

The Belle DAQ system employs a simple track-and-hold architecture, in
which the front-end electronics is temporarily inhibited upon receipt
of a level-1 trigger.  The time required for the analog multiplexer
scan of VA1 outputs determines the readout deadtime.  Since the scan
rate is 5~MHz and each FADC channel must read 640 VA1 front-end
channels, the minimum deadtime is 128~$\mu$s, which is less than the
200~$\mu$s global maximum for Belle.  Once all scans are complete and
the digital data are buffered in FIFOs, the modules release the busy
signal and the system is ready to accept and process another level-1
trigger~\cite{TsujitaPHD}.\\

This data buffering scheme allows the DSP modules to implement
pedestal subtraction and zero suppression, and low-level data
formatting asynchronously, so that no additional deadtime is
introduced.

\subsubsection{FADC system}

%%%%%%%%%%%%%%%%(Fig. 3.17 TsujitaPHD)
Figure \ref{eps-svd-halny} shows the block diagram of the FADC module,
which is called ``Halny\footnote{``Halny'' is the warm and violent
wind blowing in the Tatra mountains. This name was given to paraphrase
the name of the less capable SIROCCO module~\cite{SIROCCO}. (Sirocco is
a mild alpine wind.)}''. Each ``Halny'' module incorporates four
channels of FADC, FIFO buffering, and Motorola DSP56302 digital signal
processor. The DSPs carry out the pedestal subtraction and zero
suppression calculations on a channel-by-channel basis and implement
low-level data formatting.  The DSPs also calculate pedestals and hit
thresholds using a dynamic algorithm that automatically adjusts for
pedestal shifts and changes in noise level.

%%%%%%%%%%%%% Figure 11 %%%%%%%%%%%%%%
\begin{figure}[hbt]
%\vspace{3mm}
\begin{center}
\centerline{\psfig{file=svd/svd-fadc.eps,width=10cm,angle=0}}
%\vspace{3mm}
\caption{Block diagram of a $Halny$ FADC module.}
\label{eps-svd-halny}
\end{center}
%\vspace{8mm}
\end{figure}

%%resume here

\subsubsection{Trigger Timing Modules}

Trigger Timing Modules (TTMs) are used to control the timing of the
front-end readout electronics and to control the Halny FADC modules.
TTMs are in turn driven by the central data-acquisition (CDAQ) system.
In addition to distributing timing signals TTMs are used to transmit
slow control and monitoring information to and from the front-end
electronics. These signals pass through the CORE system, which
provides buffering, fanout and level translation.

The TTM system consists of nine modules: one master and eight slaves.
The two types of modules use the same hardware but achieve different
functionality through differently configured field-programmable
gate-array logic.  The master TTM receives timing control signals from
a timing distributer module (TDM), supplied from CDAQ, and retransmits
these signals to the slave TTMs.  Each slave TTM handles the
interaction with one repeater DOCK via a point-to-point LVDS link.
TTMs are also used to send signals to each FADC in FADC crates.  RS
485 (differential TTL) is used for the communication between TTMs and
FADCs.


\subsection{SVD DAQ System}
The SVD DAQ system consists of a data manager, a run manager and an
interlock. The data manager transfers data from the FADC output FIFOs
to the central event builder and the run manager controls the DAQ
hardware. The interlock monitors running conditions and sends signals
to the run manager when an abnormal state is detected.
Fig. \ref{svddaq-system} shows a block diagram of the data acquisition
and the monitoring system for SVD.  The SVD back-end electronics
system comprises a TTM system in a VME crate and four FADC VME
systems.  An FADC system consists of eight FADC modules and a SPARC
VME module.  The VME modules (TDM, TTMs and HALNYs) are controlled by
a VME master module ({\it i.e.} a SPARC VME module) in each VME crate.

\begin{figure}[hbt]
%\vspace{10mm}
\begin{center}
\centerline{\psfig{file=svd/svddaq-components.eps,width=14cm,angle=0}}
%\vspace{3mm}
\caption{Block diagram of the SVD DAQ system.}
\label{svddaq-system}
\end{center}
%\vspace{5mm}
\end{figure}

The four FADC subsystems collect and transmit data to the event
builder in parallel.  SPARC VME boards resident in each of the four
FADC crates carry out this task.  The SPARC VME boards poll the status
of each HALNY board in its crate and waits for a response indicating
that a digitized event is ready to be read.  HALNY data is transferred
from the output FIFOs on a word-by-word basis.


The SVD run manager controls DAQ VME modules (TDM, TTMs and HALNYs).
It initiates or terminates trigger sequences, downloads firmware to
the TTMs and Halnys, and issues resets at the beginning of each run
and when requested by CDAQ.  It also controls the run mode of the VA1
and CORE front-end electronics, via TTMs.  Finally, it initiates
recovery procedures upon request from the interlock.

The SVD interlock system monitors running conditions of SVD.  The data
integrity is checked by the consistency of the event size and
begin/end event markers.  The condition of VA1s is monitored by
checking the digital outputs from the hybrids and by tracking their
power-supply currents.  Temperatures of the hybrids, heat-sinks and
end-rings, and water circulation are monitored to make sure that the
cooling system is in working order.  The beam abort signal from the
radiation monitor described below is also fed into the interlock.
When the system senses a fault condition on one of its inputs, it
shuts down the power supplies for the front-end electronics and sends
an abort signal to the run manager.



\subsection{Monitor System}
The SVD monitor system consists of temperature monitors using
resistive temperature detectors (RTDs), instantaneous radiation
monitors based on PIN diodes, total-dose measurements using a RADFET
technique (electronic readout) and an alanine dosimeter.  These
sensors are read using a commercial data logger system~\cite{svd-IOTECH}.

A total of 48 RTDs~\cite{svd-MINCO} are attached to the SVD cooling
system and the interaction-region (IR) beam pipe. The monitor data has
been used as the interlock signal for the cooling systems for SVD and
the IR beam pipe. The operation of the cooling system has been highly
reliable: no temperature fault condition has been asserted during the
past year of operation.

The eight sets of unbiased PIN photo diodes (HPK
S3590-08)~\cite{svd-HPK} used as real-time radiation monitors are read
out by an operational amplifier OPA129 (Burr-Brown).  Output signals
are recorded by the data logger.  The signals are also sent to a
discriminator module.  When more than two channels indicate an
instantaneous radiation dose exceeding 1 Rad/s, the multiplicity logic
sends a beam abort request to the KEKB accelerator.  The frequency of
beam aborts varies with accelerator conditions, but in stable
operation is approximately one per day.

 A RADFET is a MOSFET~\cite{svd-Radfet} optimized for radiation
measurement.  When it is irradiated, the threshold voltage of the FET
is shifted due to the accumulation of charge in the gate oxide.  Since
this mechanism is essentially the same as the mechanism of radiation
damage in the VA1 chips, it provides a good measure of radiation
damage in those chips.

The alanine dosimeters are passive device based on electron spin
resonance (ESR) measurement of radiation induced free radicals in an
amino acide alanine (CH$_3$-CH-NH$_2$-COOH).  They provide a measure
of long-term integrated dose and accessible for reading only in
conjunction with major detector accesses.

%resume 
\subsection{Performance}

High SVD strip yields and good S/N ratios are needed to ensure the
efficient matching between tracks detected by CDC and clusters
detected by SVD.  Distributions of normalized cluster energies for
minimum ionizing particles (MIPs) from hadronic events are shown in
Fig. \ref{eps-svd-cluster}, where the cluster energy is normalized to
the same track path length in DSSD (300 $\mu$m).  The measured
most-probable peak height is approximately 19,000~e$^-$.  The noise
level and the S/N ratio for each DSSD side ($p$, $n$ and $p$-$n$) are
summarized in Table \ref{svd-C-ENC-SN}.  Strip yields, which are
defined to be the fraction of channels with S/N ratios larger than
10:1, are measured to be 98.8\% on layer 1, 96.3\% on layer 2, and
93.5\% on layer 3~\cite{svd-Alimonti2000}.

%%%%%%%%%%%%% Figure 19 %%%%%%%%%%%%%%
\begin{figure}[hbt]
%\vspace{8mm}
\begin{center}
\centerline{\psfig{file=svd/cluster_1117.eps,width=10cm,angle=0}}
%\vspace{5mm}
\caption{Cluster energy distribution in which the cluster energy is
normalized by the track path length in DSSD.}
\label{eps-svd-cluster}
\end{center}
%\vspace{10mm}
\end{figure}

The track-matching efficiency is defined as the probability that a CDC
track within the SVD acceptance has associated SVD hits in at least
two layers, and in at least one layer with both the $r$-$\phi$ and
$r$-$z$ information.  Tracks from $K_{S}$ decays were excluded since
these tracks did not necessarily go through SVD.
Fig. \ref{eps-svd-matching} shows the SVD-CDC track matching
efficiency for hadronic events as a function of time.  The average
matching efficiency is better than 98.7\%, although we observe slight
degradation after one year operation as a result of the gain loss of
VA1 from radiation damage ~\cite{svd-Alimonti2000}.

The momentum and angular dependence of the impact parameter resolution
are shown in Fig. \ref{eps-svd-impact} and well represented by the
following formula: \\ $\sigma_{xy}=19 \oplus
50/(p\beta\sin^{3/2}\theta)$ $\mu$m and $\sigma_{z}=36 \oplus
42/(p\beta\sin^{5/2}\theta)$ $\mu$m.

We performed the same study for the MC sample and obtained an IP
resolution of $\sigma_{xy} = 15 \oplus 49/(p\beta\sin^{3/2}\theta)$
$\mu$m and $\sigma_{z}=28 \oplus 41/(p\beta\sin^{5/2}\theta)$
$\mu$m. This indicates that the IP resolution term dominated by the
scattering is well simulated by the MC data.  The flat components are
larger in the real data than in the MC simulation, which indicates the
amount of remaining misalignment errors.  Detailed description of
Belle SVD performance studies can be found
elsewhere~\cite{IEEE2000SVD}.
%%%%%%%%%%%%% Figure 20 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
\centerline{\psfig{file=svd/svd-matching.eps,width=8cm,angle=0}}
\caption{SVD-CDC track matching efficiency as a function of the date
of data taking.}
\label{eps-svd-matching}
\end{center}
\vspace{0mm}
\end{figure}

%%%%%%%%%%%%% Figure 21 %%%%%%%%%%%%%%
\begin{figure}[h]
\vspace{0mm}
\begin{center}
\centerline{\psfig{file=svd/svd-rres.eps,width=70mm}\hspace*{-5mm}
            \psfig{file=svd/svd-zres.eps,width=70mm}}
\vspace{0mm}
\caption{Impact parameter resolution.}
\label{eps-svd-impact}
\end{center}
\vspace{0mm}
\end{figure}
