\section{Time-of-Flight Counters, TOF}

A time-of-flight (TOF) detector system using plastic scintillation
counters is very powerful for particle identification in $e^+e^-$
collider detectors.  For a 1.2 m flight path, the TOF system with 100
ps time resolution is effective for particle momenta below about 1.2
GeV/c, which encompasses 90 \% of the particles produced in
$\Upsilon$(4S) decays.  It can provide clean and efficient $b$-flavor
tagging.\\

In addition to particle identification, the TOF counters provide fast
timing signals for the trigger system to generate gate signals for
ADCs and stop signals for TDCs. To avoid pile-up in the trigger queue,
the rate of the TOF trigger signals must be kept below 70 kHz.  The
gate and stop timing for the CsI calorimeter and CDC sets a time
jitter requirement of less than $\pm$10 ns. Simulation studies
indicate that to keep the fast trigger rate below 70 kHz in any beam
background conditions, the TOF counters should be augmented by thin
trigger scintillation counters (TSC)~\cite{TDR95,Progress97}.

\subsection{Design and Construction of TOF}

To achieve the design goal of 100 ps, the following design strategies
have been adopted:
%
\begin{itemize}
\item use of fast scintillator with an attenuation length longer than
2 m,
\item elimination of light guides to minimize the time dispersion of
scintillation photons propagating in the counter, and
\item use of photo-tubes with large-area photocathodes to maximize
photon collection.
\end{itemize}
%
These considerations led us to a configuration with fine-mesh-dynode
photomultiplier tubes (FM-PMT)~\cite{tof-KichimiA325} mounted
directly on the TOF and TSC scintillation counters and placed in a
magnetic field of 1.5 T.\\

The TOF system consists of 128 TOF counters and 64 TSC counters. Two
trapezoidally shaped TOF counters and one TSC counter, with a 1.5-cm
intervening radial gap, form one module. In total 64 TOF/TSC modules
located at a radius of 1.2 m from the interaction point cover a polar
angle range from 34$^o$ to 120$^o$.  The minimum transverse momentum
to reach the TOF counters is about 0.28 GeV/c.  Module dimensions are
given in
%%%%%%%%%%%%%%%(Fig. 2.16 Progress1997)
Fig. \ref{eps-tof-dimension}. These modules are individually mounted
on the inner wall of the barrel ECL container.  The 1.5 cm gap between
the TOF counters and TSC counters was introduced to isolate TOF from
photon conversion backgrounds by taking the coincidence between the
TOF and TSC counters. Electrons and positrons created in the TSC layer
are impeded from reaching the TOF counters due to this gap in a 1.5 T
field~\cite{tof-KichimiBN138}.
%%%%%%%%%%%%%%%(Table 2.5 Progress1997)
Table \ref{TOF/TSC} provides parameters for the TOF and TSC counters.
The width of the TOF counter is approximately 6 cm.\\

%%%%%%%%%%%%% Figure 1 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig1.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Dimensions of a TOF/TSC module.}
\label{eps-tof-dimension}
\end{center}
\vspace{5mm}
\end{figure}


%%%%%%%%%%%%%
A Monte Carlo study with a full detector simulation including the
effects of the material of the barrel ACC and back-splash from the
barrel ECL indicates that the clean hit probability is about 90 \% for
particles from $B\bar{B}$ events in the 128-segmented barrel TOF solid
angle.

%%%%%%%%%%%%%%%%%%%%%% Table 1 %%%%%%%%%%%%%%%%%%%%

\begin{table}
\caption{Parameters of the TOF and TSC counters.  Scintillator
materials are Bicron BC408 for TOF and Bicron BC412 for TSC.}
\vspace{6mm}
\begin{center}
\begin{tabular}{c c c c c c}
\hline \hline
Counter & Thickness (cm) & z coverage (cm) & r (cm) & $\phi$ segm. & No. of PMTs\\
\hline
TOF & 4.0 & -72.5 to +182.5 & 122.0 & 128 & 2\\
TSC & 0.5 & -80.5 to +182.5 & 117.5	 & 64 & 1\\
\hline \hline
\end{tabular}
\label{TOF/TSC}
\end{center}
\vspace{8mm}
\end{table}


\subsubsection{FM-PMTs}

Hamamatsu (HPK) type R6680 fine-mesh photomultipliers, with a 2-inch
diameter and 24 stages of 2000 mesh/inch dynodes, have been selected
for the TOF counter. The 24 dynode stages provide a gain of $3 \times
10^6$ at a high voltage below 2800 V in a magnetic field of 1.5 T.
The bialkali photocathode with an effective diameter of 39 mm covers
50 \% of the end area of each TOF counter.  The transit time spread is
320 ps (rms), the rise and fall times are 3.5 and 4.5 ns,
respectively, and the pulse width is about 6 ns at FWHM.
%%%%%%%%%%%%%%(tof-KichimiHamamatsu99)
Figs. \ref{eps-tof-fieldtest}(a) and (b) show gains and time
resolutions of eight typical FM-PMTs as a function of magnetic field
strength.  The measurements were performed with the field parallel to
the tube axis, using an N2-dye laser of 420 nm wavelength. Although
the gain was reduced by a factor of about 200 at 1.5 T, the time
resolution was not significantly affected.  Degradation of the time
resolution of about 10 to 15 \% was observed when the magnetic field
was raised from 0 to 1.5 T.  All FM-PMTs were tested for their gains
and time resolutions before the final module assembly.\\

%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure}[hbt]
%\vspace*{-5mm}
\begin{center}
%\epsfysize=10cm
%\centerline{\psfig{file=tof/toffig2.eps,width=12cm,angle=0}}
\centerline{\psfig{file=tof/fmpmt2.eps,width=10cm,angle=0}}
\vspace*{5mm}
\caption{1.5 T field test of FM-PMTs. (a) Gains and (b) time
resolutions as a function of magnetic field.}
\label{eps-tof-fieldtest}
\end{center}
\vspace*{-5mm}
\end{figure}

FM-PMTs were attached to the TOF counter ends with an air gap of
$\sim$0.1 mm. In the case of the TSC counters the tubes were glued to
the light guides at the backward ends. The air gap for the TOF counter
selectively passes earlier arrival photons and reduces a gain
saturation effect of FM-PMT due to large pulses at a very high
rate. As the time resolution is determined by the rising edge of the
time profile of arrival photons at PMT, the air gap hardly affects the
time resolution.

\subsubsection{Scintillators}

The TOF and TSC scintillators (BC408, Bicron) were wrapped with one
layer of 45 $\mu$m thick polyvinyl film (Tedlar) for light tightness
and surface protection.  This thin wrapping minimizes the dead space
between adjacent TOF counters.  Careful measurements of the physical
dimensions of all counters were made to ensure the dimensions of each
counter consistent with the TOF assembly tolerance.\\

The attenuation length and light yield of all of the TOF scintillators
were measured using a cosmic-ray test stand.  A lucite \v{C}erenkov
counter was used to suppress low momentum cosmic rays.
%%%%%%%%%%%%%%%
Fig. \ref{eps-tof-attenuation}(a) shows the result of attenuation
length measurements for some of TOF scintillators.  The attenuation
length was evaluated by using a ratio of signal charges and a time
difference between two PMTs of a TOF counter. The measured light
propagation velocity was 14.4 cm/ns.  The average attenuation length
was 3.9 m, which satisfies the specification for the minimum
attenuation length of 2.5 m.
%%%%%%%%%%%%%%%(Fig. 3.33(b) Progress1997)
Fig. \ref{eps-tof-attenuation}(b) shows the result of measurements of
light yields seen by each PMT for cosmic rays at the center of a
counter.

%%%%%%%%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig3.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{(a) Attenuation lengths and (b) light yields of 38 TOF scintillators.}
\label{eps-tof-attenuation}
\end{center}
\vspace{5mm}
\end{figure}

\subsubsection{Readout electronics}

A block diagram of a single channel of the TOF front-end electronics is
shown in
%%%%%%%%%%%%%%%(Fig.2.43 Progress1997)
Fig. \ref{eps-tof-electronics}.
%%%%%%%%%%%%% Figure 4 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig4.eps,width=15cm,angle=0}}
\vspace{5mm}
\caption{Block diagram of a single channel of TOF front-end electronics.}
\label{eps-tof-electronics}
\end{center}
\vspace{8mm}
\end{figure}
Each PMT signal is split into two. One is sent to Q-to-T and then to a
multihit TDC for charge measurement.  The other generates signals
corresponding to two different threshold levels: a high level (HL) and
a low level (LL).  Two LeCroy MVL107s are used for discriminators,
with threshold levels set between 0.3$\sim$0.5 mips for HL and
0.05$\sim$0.1 mips for LL.  The LL output provides the TOF timing and
the HL output provides a trigger signal.  HL is used to make a self
gate for LeCroy MQT300A Q-to-T conversion and also to gate the LL
output.  A common trigger is prepared for pedestal calibration of
MQT300A. The signal T is further processed in a time stretcher for
readout by TDC 1877S. The MQT output Q is a timing signal
corresponding to the charge, which is directly recorded with TDC
1877S.
	 
%%%%%%%%%%%%%%%%(Fig. 3(a) and (b) tof-KichimiBN138)
Figures \ref{eps-tof-triggerel}(a) and (b) show block diagrams of the
TOF front-end electronics for (a) event trigger and (b) readout of
charge and timing of TOF signals.  The gated LL signals from two ends
of a counter are mean-timed and coincidenced with the TSC signals to
create a fast trigger signal.  The nominal coincidence arrangement
between one TSC counter and four TOF counters is used to ensure
triggers for low momentum tracks.
%%%%%%%%%%%%%%%%%%(Fig. 5 tof-KichimiBN138)
Fig. \ref{eps-tof-trigrate} shows the TSC trigger rates calculated by
a Monte Carlo simulation in various coincidence arrangements with TOF
counters as a function of discrimination level in units of mips for
Bhabha and spent electrons~\cite{tof-YamaguchiBN133}.  The time jitter
of the signal is smaller than 3.5 ns in each event, and provides a
precise event timing to the Belle trigger system to make TDC-stop to
CDC and ADC-gate to CsI readout.  The time jitter is reduced to 0.5 ns
by applying a correction of the hit position in TOF counters.\\

%%%%%%%%%%%%% Figure 6 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig5.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{Block diagrams of the TOF electronics for (a) trigger and (b) readout.}
\label{eps-tof-triggerel}
\end{center}
\vspace{8mm}
\end{figure}

%%%%%%%%%%%%% Figure 7 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig7.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Event trigger rates due to background photons from Bhabha and
spent electrons.}
\label{eps-tof-trigrate}
\end{center}
\vspace{8mm}
\end{figure}

The time-stretcher (TS) circuit~\cite{tof-KichimiBN223} expands the
time difference between the TOF pulse and the reference clock by a
factor 20, which enables us to measure the time with a 25 ps precision
with the Belle standard 0.5 ns multi-hit TDC readout scheme. The
timing of each TOF signal is measured relative to an edge of the
reference clock.  The time interval T between the TOF signal and the
second reference clock edge is measured as shown in
%%%%%%%%%%%%%%%(Fig. 8(b) tof-KichimiBN138)
Fig. \ref{eps-tof-timestr}.  The output contains an expanded time,
where the time interval between the second (trailing) and third
(rising) edges represents the time of T expanded by a factor of $f$ (=
20).  In this scheme the time interval to be measured is always in the
range between 16 ns and 32 ns.\\

%%%%%%%%%%%%% Figure 5 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig6.eps,width=15cm,angle=0}}
\vspace{5mm}
\caption{Time stretcher scheme for TOF.}
\label{eps-tof-timestr}
\end{center}
\vspace{8mm}
\end{figure}

The TS reference clock is provided by an RF clock that is precisely
synchronized with the beam collisions. The reference clock with a
period of approximately 16 ns can be generated from the RF signal of
508.8875 $\pm 10^{-6}$ MHz, with a time jitter of 20 ps. As the RF
clock is used for the whole KEKB timing control, the reference clock
is necessarily synchronized with the beam collision timing.  A
collision bunch number from 0 to 8 is determined in an offline
analysis.  Due to the small expansion factor of 20 and use of a 16 ns
clock, this system provides a virtually dead-time-less TDC (1 $\mu$s).
The final TOF information is obtained by applying a time walk
correction to the time information.
	 


The main parameters for the TOF readout system are summarized in
%%%%%%%%%%%%%%%%(Table 2.17 Progress1997
 Table \ref{TOF-electronics}.\\
%%%%%%%%%%%%%%%%%% Table 2 %%%%%%%%%%%%%%%%%%
\begin{table}
\caption{Parameters for TOF/TSC electronics.}
\vspace{5mm}
\begin{center}
\begin{tabular}{|c|c|c|}
\hline \hline
Input signal & BC408 (Scint.) & $\tau_{rise} \sim$ 4 ns (PMT $\sim$ 2.5 ns)\\
 & R6680 (PMT) & FWHM $\sim$ 10 ns\\
 & & V$_{peak} \leq $ 5 V (1.2 $\sim$ 1.5 V/mip)\\
 & & Signal rate $\leq$ 17 kHz at $\geq$ 0.2 mip\\
\hline
Q-to-T & MQT300 & Self gate (width $\leq$ 100 ns)\\
 & & Dynamic range: 0 $\sim$ 500 pC (11 bits)\\
 & & Nonlinearity $\sim$ 0.1 \%\\
 & & Dead time = 5 $\mu$s max.\\
\hline
Discriminator & MDC100A & Level : 0 $\sim$2 V\\
 & (MVL407S) & HL for self gate/trigger\\
 & & LL for timing\\
\hline
Mean timer & Delay line & Input signal width = 30 ns\\
 & & Delay = 32 ns\\
 & & Time jitter $\leq$ 5 ns\\
\hline
Time stretcher && Clock period = 16 ns (8 RF)\\
 & & Expansion factor = 20\\
 & & Time jitter $\leq$ 20 ps\\
 & & Dead time $\leq$ 1 $\mu$s\\
\hline
 Reference time & RF clock & Total uncertainty $\sim$ 30 ps\\
\hline \hline
\end{tabular}
\end{center}
\label{TOF-electronics}
\vspace{10mm}
\end{table}

\subsection{Beam Test}

A TOF module built in accordance with the final design was assembled
and tested using the $\pi$2 test beam line of KEK-PS. The test counter
was set on a movable stage that enabled the position and incident
angle of the beam on the counter to vary simultaneously so that the
geometry of the Belle TOF detector was reproduced.\\

%%%%%%%%%%%%%%(Fig. 3.29(a) Progress1997)
Figure \ref{eps-tof-timeresol} shows the TOF time resolution as a
function of beam position. A time-walk correction was applied at each
position and the start-counter time-jitter of 35 ps was subtracted in
quadrature.  An intrinsic time resolution of approximately 80 ps was
obtained over the whole counter. In the Belle experiment we expect
some additional contributions to the time resolution:
\begin{itemize}
\item a 10 \% degradation of the intrinsic resolution caused by the
1.5 T magnetic field as was observed in a beam test of a same-size TOF
counter with a similar fine-mesh PMT,
\item a 20 ps contribution due to the 4 mm beam bunch length and
jitter in the RF signal used as the reference time, and
\item a 20 ps contribution from time stretcher readout electronics.
\end{itemize}

%%%%%%%%%%%%% Figure 8 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig8.eps,width=14cm,angle=0}}
\vspace{0mm}
\caption{Time resolution of a Belle TOF module as a function of hit position.}
\label{eps-tof-timeresol}
\end{center}
\vspace{5mm}
\end{figure}

Including these errors in quadrature gives an expected overall time
resolution that meets the Belle design goal of 100 ps.\\

%%%%%%%%%%%%%%%(Fig. 3.29(b) Progress1997)
Figure \ref{eps-tof-pipseparation} shows time-of-flight distributions
for $\pi^+$ and protons at 2.5 GeV/c which indicate 5$\sigma$
separation of $\pi^+$/p at 2.5 GeV/c for the beam at a forward region.
This implies approximately 3$\sigma$ $\pi$/K separation at 1.25
GeV/c. A tail at later times is attributed to a gain saturation of the
PMT at high signal rates.  A rate of minimum ionizing particles on a
whole TOF counter was observed to be about 14 kHz during the test with
a positive beam. At this rate and with a PMT high voltage of 1500 V,
the average anode current was about 10 $\mu$A of a total base current
of 60 $\mu$A. In order to minimize the saturation, the ratio of anode
to base currents should be kept below 10 \%.  With the present IR and
beam pipe design and assuming a PMT gain of $3 \times 10^6$, a Monte
Carlo simulation predicts an anode current of 2.4 $\mu$A.  This
indicates that the beam test conditions described above were much
severer than those expected in the Belle environment. In order to
guarantee a further safety margin under high beam background
conditions, the nominal base current was increased to 300 $\mu$A in
the final design.\\

%%%%%%%%%%%%% Figure 9 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig9.eps,width=14cm,angle=0}}
\vspace{-5mm}
\caption{$\pi/p$ separation at 2.5 GeV/c.}
\label{eps-tof-pipseparation}
\end{center}
\vspace{5mm}
\end{figure}

%%%%%%%%%%%%%%(Fig. 3.30 Progress1997)
Figure \ref{eps-tof-tsclight} shows the effective light yield seen by
a TSC PMT as a function of beam position.  A minimum ionizing track
produces more than 25 photoelectrons over the whole counter.  This
ensures a high efficiency of 98 \% for TOF trigger even at a nominal
discrimination level of 0.5 mips.
%%%%%%%%%%%%% Figure 10 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig10.eps,width=12cm,angle=0}}
\vspace{-5mm}
\caption{Light yield of a TSC counter as a function of hit position.}
\label{eps-tof-tsclight}
\end{center}
\vspace{7mm}
\end{figure}


\subsection{Performance}

\subsubsection{PMT gain adjustment}

The gain of each PMT was adjusted using pulses provided by a laser
calibration system, which consists of an $N_2$-dye laser source (420
nm) and three optical fiber bundles for the TOF forward and backward
PMTs and the TSC PMTs.  The variation of the PMT gains was 18 \%
(rms).

\subsubsection{Calibration and time resolution using $\mu$-pair events}

Muons from 150 $\times$ $10^3$ $\mu$-pair events corresponding to an
accumulated luminosity of 250 $pb^{-1}$ were used for
calibration~\cite{tof-KichimiHamamatsu99}.  Electrons in Bhabha events
are frequently accompanied by back splash from ECL behind the TOF
counters and thus can not be used to simulate hadrons reliably.\\

The following empirical formula was used for the time walk correction
to get a precise observed time,
\begin{equation}
T^{twc}_{obs} = T_{raw} - \left(\frac{z}{V_{eff}} + \frac{S}{\sqrt{Q}} +F(z) \right)
\end{equation}
where $T_{raw}$ is the PMT signal time, $z$ is the particle hit
position on a TOF counter, $V_{eff}$ is the effective velocity of
light in the scintillator, Q is the charge of the signal, S is the
coefficient of time walk, and
\begin{equation}
F(z) = \sum_{n=0}^{n=5} A_n z^n.
\end{equation}
%
The coefficients, $1/V_{eff}, S$ and $A_n$ for n = 0 to 5, were
determined by minimizing residuals defined as $\delta t =
T_{obs}^{twc} - T_{pred}$ for all available TOF hits.  Here,
$T_{pred}$ is the time of flight predicted using the track length
calculated from the fit to hits in CDC. The optimization can be done
for all PMTs together or for each PMT separately.\\

%%%%%%%%%%%%%%%%(Fig. 5 tof-KichimiHamamatsu99)
Figure \ref{eps-tof-mutimeres} shows time resolutions for forward and
backward PMTs and for the weighted average time as a function of
$z$. The resolution for the weighted average time is about 100 ps with
a small $z$ dependence.  This satisfies the design goal, and we expect
further improvement.  The distribution of fitted residuals $\delta t$
shows an oscillatory behavior of amplitude of $\pm$25 ps as a function
of $z$ after calibration.  This indicates that the 5-th order
polynomial $F(z)$ does not match the data well, and a better choice of
the formula may give us further improvement.

%%%%%%%%%%%%% Figure 11 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{-20mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=tof/toffig11.eps,width=12cm,angle=0}}
\caption{Time resolution for $\mu$-pair events.}
\label{eps-tof-mutimeres}
\end{center}
\vspace{10mm}
\end{figure}


\subsubsection{$\pi^{\pm}/K^{\pm}$ separation}

%%%20000907 addition by Jorge
After the calibration constants (TWC), optimized on $\mu$-pair events,
were applied to hadronic events, sizeable deviations from zero for the
$\delta t$ residuals were observed. In particular the deviations were
found to be momentum dependent and different for each of the hadron
species. Deviations were observed as large as two sigma away from zero
for low momentum tracks identified as kaons and protons by other
detector subsystems. Further investigation revealed that the observed
momentum and hadronic species dependence could in fact be reasonably
modeled by a single linear function in terms of the track's velocity,
see the left side of Fig. \ref{eps-tof-systematics}. A velocity or
$\beta$ parameterization was then applied in the TOF reconstruction
algorithm to correct for this effect. The $\beta$ parameterization was
determined from an analysis of a large sample of hadronic events. In
the right side of Fig. \ref{eps-tof-systematics} we show the momentum
distribution of the $\delta t$ residual after the application of the
$\beta$ correction. The large $\delta t$ systematics observed earlier
were largely removed over the relevant momentum range and for each of
the hadron types. Further study is planned to establish the origin of
the velocity dependence in the $\delta t$ distribution.
Fig. \ref{eps-tof-systematics-error} shows the TOF resolution averaged
over all counters and $z$ as a function of momentum for each hadron
species.

\begin{figure}[hbt]
%\centerline{\epsfysize 8.0 truecm \epsfbox{file=tof/dtvsbeta.eps}\epsfysize% 8.0 truecm 
%\epsfbox{file=tof/dtvspmag-vcorr.eps}}
\centerline{\psfig{file=tof/dtvsbeta.eps,width=7cm,angle=0}\hspace*{0mm}
\psfig{file=tof/dtvspmag-vcorr.eps,width=7cm,angle=0}}
\caption{Systematics of TOF $\delta t$ residuals. The figure on the
left shows the $\beta$ dependence of the $\delta t$ residual after
application of the $\mu$-pair optimized calibration constants but
before the $\beta$ correction for each hadron species. The figure on
the right shows the distribution of the residuals as a function of
momentum after application of all calibration constants.}
\label{eps-tof-systematics}
\vspace{10mm}
\end{figure}

\begin{figure}[hbt]
%\centerline{\epsfysize 10.0 truecm \epsfbox{dtvsresol.eps}}
\centerline{\psfig{file=tof/dtvsresol.eps,width=10cm,angle=0}}
\caption{The TOF resolution, averaged over all counters and $z$, as a
function of momentum for each hadron species.}
\label{eps-tof-systematics-error}
\vspace{10mm}
\end{figure}
%%%20000907 end of addition by Jorge

%%%%%%%%%%%%%%%%%(Fig. 6 tof-KichimiHamamatsu99)
Figure \ref{eps-tof-massdis} shows the mass distribution for each
track in hadron events, calculated using the equation
\begin{equation}
mass^2 = \left( \frac{1}{\beta^2} - 1\right) P^2 =
\left(\left( \frac{c T_{obs}^{twc}}{L_{path}}\right)^2 - 1\right) P^2
\end{equation}
%
where $P$ and $L_{path}$ are the momentum and path length of the
particle determined from the CDC track fit assuming the muon mass,
respectively.  Clear peaks corresponding to $\pi^{\pm}$, $K^{\pm}$ and
protons are seen. The data points are in good agreement with a Monte
Carlo prediction (histogram) obtained by assuming $\sigma_{TOF}$ = 100
ps.\\

%%%%%%%%%%%%% Figure 12 %%modified%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=tof/toffig12.eps,width=10cm,angle=0}}
%\centerline{\epsfysize 8.0 truecm \epsfbox{toffig12.eps}}
\caption{Mass distribution from TOF measurements for particle momenta
below 1.2 GeV/c.}
\label{eps-tof-massdis}
\end{center}
\vspace{1mm}
\end{figure}

The identification power of $\pi^{\pm}/K^{\pm}$ separation is shown in
%%%%%%%%%%%%%%%%(Fig. 8 tof-KichimiHamamatsu99)
Fig. \ref{eps-tof-pikseparation} as a function of momentum.  The
identification power is defined as
\begin{equation}
\sigma_{\pi^{\pm}/K^{\pm}} = \frac{T_{obs}^{twc}(K) -
T_{obs}^{twc}(\pi)}{\sqrt{\sigma_K^2 + \sigma_{\pi}^2}},
\end{equation}
where $\sigma_K$ and $\sigma_{\pi}$ are the time resolution for $K$
and $\pi$, respectively, at each momentum. This demonstrates clear
2$\sigma$ separation for particle momenta up to 1.25 GeV/c.

%%%%%%%%%%%%% Figure 14 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{-30mm}
\begin{center}
\epsfysize=10cm
%\centerline{\epsfysize 12.0 truecm \epsfbox{toffig14.eps}}
\centerline{\psfig{file=tof/toffig14.eps,width=12cm,angle=0}}
\caption{$\pi^{\pm}/K^{\pm}$ separation by TOF.}
\label{eps-tof-pikseparation}
\end{center}
\vspace{0mm}
\end{figure}
