\section{Aerogel \v{C}erenkov Counter System, ACC}

Particle identification, specifically the ability to distinguish
$\pi^{\pm}$ from $K^{\pm}$, plays a key role in the elucidation of CP
violation in the $B$ system.  An array of silica aerogel threshold
\v{C}erenkov counters has been selected as part of the Belle particle
identification system to extend the momentum coverage beyond the reach
of $dE/dx$ measurements by CDC and time-of-flight measurements by
TOF~\cite{TDR95,Progress97}.

\subsection{Detector Design}

The configuration of the silica aerogel \v{C}erenkov counter system,
ACC, in the central part of the Belle detector is shown in
Fig. \ref{eps-acc-arrangement}~\cite{acc-IijimaA379,acc-SumiyoshiA433}.
ACC consists of 960 counter modules segmented into 60 cells in the
$\phi$ direction for the barrel part and 228 modules arranged in 5
concentric layers for the forward end-cap part of the detector.  All
the counters are arranged in a semi-tower geometry, pointing to the
interaction point.  In order to obtain good pion/kaon separation for
the whole kinematical range, the refractive indices of aerogels are
selected to be between 1.01 and 1.03, depending on their polar angle
region.  A typical single ACC module is shown in
%%%%%%%%%%%%%%(acc-IijimaHamamatsu1999)
Figs. \ref{eps-acc-module}(a) and (b) for the barrel and the end-cap
ACC, respectively.  Five aerogel tiles are stacked in a thin (0.2 mm
thick) aluminum box of approximate dimensions $12 \times 12 \times 12
~cm^3$.  In order to detect \v{C}erenkov lights effectively, one or
two fine mesh-type photomultiplier tubes (FM-PMTs), which are
operated in a magnetic field of 1.5 T~\cite{acc-IijimaA387}, are
attached directly to the aerogels at the sides of the box. We use PMTs
of three different diameters: 3 in. (R6683), 2.5 in. (R6682), and 2
in. (R6681) of Hamamatsu Photonics, depending on refractive indices,
in order to get uniform response for light velocity particles.

%%%%%%%%%%%%% Figure 1 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/accarrangement.eps,width=15cm,angle=0}}
\vspace{-3mm}
\caption{The arrangement of ACC at the central part of the Belle detector.}
\label{eps-acc-arrangement}
\end{center}
\vspace{3mm}
\end{figure}

%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/accmodule.eps,width=10cm,angle=0}}
\vspace{3mm}
\caption{Schematic drawing of a typical ACC counter module: (a) barrel
and (b) end-cap ACC.}
\label{eps-acc-module}
\end{center}
\vspace{3mm}
\end{figure}



\subsubsection{Production of hydrophobic silica aerogels}

Since aerogels having the required low refractive indices were not
commercially available, we decided to produce aerogels by our own
efforts.  We adopted a new production method for the preparation of
aerogels, in which methylalkoxide oligomer is used as a
precursor~\cite{acc-AdachiA355}.  This oligomer is hydrolyzed and
polymerized in a basic catalyst ($NH_4OH$) in a solution of methyl or
ethyl alcohol.  The average size of alcogels is $120 \times 120 \times
24~ mm^3$ and they are formed in aluminum molds coated with a thin
PTFE film.  The gelation time ranges from a few minutes to 10 min
depending on densities. \\

Silica aerogels have been used in several experiments, but their
transparencies became worse within a few years of use, which was our
great worry to adopt an aerogel \v{C}erenkov counter as a particle
identification device.  This phenomenon may be attributed to the
hydrophilic property of silica aerogels. In order to prevent such
effects, we have made our silica aerogels highly hydrophobic by
changing the surface hydroxyl groups into trimethylsilyl
groups~\cite{acc-Yokoyama}.  This modification is applied before the
drying process. As a result of this treatment, our silica aerogels
remain transparent even four years after they were produced.\\

After three weeks of aging including the surface modification, the
alcogels were dried by a super critical drying method of $CO_2$. This
drying process took 48 h.  The volume of extractor is 140 $l$ and we
could produce about 38 $l$ of silica aerogel in one batch.  After
seven months of operation (two batches/week), we produced about
2~$m^3$ of silica aerogel.  Details of the production method can be
found in~\cite{acc-AdachiA355,acc-Sumiyoshi225}.

\subsubsection{Quality of the aerogels}

All the aerogel tiles thus produced have been checked for optical
transparency, transmittance of unscattered light, refractive index,
dimension, etc.
%%%%%%%%%%%%%%%%(Fig. 3 acc-SumiyoshiA433)
Fig. \ref{eps-acc-transmittance} shows typical transmittance curves
obtained by a photo-spectrometer for aerogels of four different
refractive indices. The $n$ = 1.028 aerogels have better transmittance
than the others.  Their average transmission length ($\Lambda$) at 400
nm is 46 mm, while the others are around 25 mm.  Here $\Lambda$ is
defined by the function: $T = T_0 ~exp(- d/\Lambda)$, where $T_0$ and
$T$ are the incident and transmitted light intensities, respectively,
and d is the thickness of an aerogel tile.  These aerogels were
produced from the alcogel which was prepared by using methyl alcohol
as a solution.\\

%%%%%%%%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/acctransmit.eps,width=10cm,angle=0}}
\vspace{2mm}
\caption{Light transmittance spectra for silica aerogels (thickness =
23.3 mm) of $n$ = 1.01, 1.015, 1.02 and 1.028.  The silica aerogels of
$n$ = 1.028 prepared by using methanol as the preparation solvent
shows the best transmittance data.}
\label{eps-acc-transmittance}
\end{center}
\vspace{3mm}
\end{figure}


The refractive indices are well controlled as $\Delta n/(n - 1) \sim$
3\% for all the produced aerogel tiles, which is essentially the same
as the measurement error of the refractive index determined by
measuring a deflection angle of laser light (He-Ne: 543.5 nm) at a
corner of each aerogel tile.\\

We carried out a test to ensure the radiation hardness of aerogels by
placing aerogel samples ($n$ = 1.012, 1.018 and 1.028) in
high-intensity $\gamma$-rays from a $^{60}$Co
source~\cite{acc-SahuA382}. Transparencies and refractive indices of
aerogels were measured up to 9.8 Mrad, which corresponds to more than
10 years of running at KEKB.  Neither deterioration on the
transparency nor change in the refractive indices were observed within
the errors of measurements after the irradiation. Measurement
accuracies were 0.8 \% for the transparency and $<$0.0006 for the
refractive index.  Deterioration in transparency and changes of
refractive index after 9.8 Mrad $\gamma$-ray irradiation were observed
to be less than 1.3\% and 0.001 at 90\% confidence level,
respectively.

\subsubsection{Fine-mesh photomultiplier tubes}

Since ACC is placed in a high magnetic field of 1.5 T in the Belle
detector, we decided to use fine-mesh photomultiplier tubes (FM-PMTs)
for the detection of \v{C}erenkov lights, taking advantage of its
large effective area and high gain~\cite{acc-IijimaA387}.  Other
candidate photo-sensors such as microchannel-plate photomultiplier
tubes (MCP-PMT) and hybrid photodiodes (HPD) were still at R\&D stages
or extremely expensive when this decision was made.  The FM-PMTs were
produced by Hamamatsu Photonics~\cite{acc-HPK}.\\

A sectional view of an FM-PMT is shown in 
%%%%%%%%%%%%%%%%(Fig. 4 acc-SudaA406)
Fig. \ref{eps-acc-fmpmt} .  Each FM-PMT has a borosilicate glass
window, a bialkali photo-cathode, 19 fine-mesh dynodes, and an
anode. Three types of FM-PMTs of 2, 2.5, and 3 inches in diameter are
used in ACC.  The effective diameters ($\phi$) of these FM-PMTs are
39, 51, and 64 mm.  The cathode-to-anode distance ($L$) is about 20
mm.  The average quantum efficiency of the photo-cathode is 25 \% at
400 nm wavelength.  The optical opening of the mesh is about 50 \%.\\

%%%%%%%%%%%%% Figure 4 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/accfmpmt.eps,width=10cm,angle=0}}
\vspace{3mm}
\caption{Sectional view of FM-PMT.}
\label{eps-acc-fmpmt}
\end{center}
\vspace{3mm}
\end{figure}


The FM-PMTs with 19 dynode stages of fine mesh have high gain ($\sim
10^8$) with moderate HV values ($<$ 2500 V).  The gain of FM-PMT
decreases as a function of field strength as shown in
%%%%%%%%%%%%%%%%(Fig. 4 acc-SumiyoshiA433)
Fig. \ref{eps-acc-tubegain}. The gain reduction is $\sim 10^{-3}$ for
FM-PMTs placed parallel to the direction of magnetic field and
slightly recovers when they are tilted.  The FM-PMTs used for ACC were
produced with dynodes with a finer mesh spacing than conventional
products at that time, to give approximately 10 times higher gain as
shown in in Fig. \ref{eps-acc-tubegain}.\\
%Recent products of FM-PMTs by Hamamatsu Photonics with 
%finer meshes than conventional ones with the typical mesh width and 
%spacing of about 6 $\mu$m and 17 $\mu$m, respectively, have
%approximately 10 times better gain as shown in Fig. \ref{eps-acc-tubegain}.\\

%%%%%%%%%%%%% Figure 5 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/accgain.eps,width=12cm,angle=0}}
\vspace{0mm}
\caption{Relative gains of conventional and improved FM-PMTs in
magnetic fields.  The tubes were placed parallel (0$^o$) or tilted by
30$^o$ with respect to the field direction.}
\label{eps-acc-tubegain}
\end{center}
\vspace{3mm}
\end{figure}


Effects of a magnetic field on the pulse height resolution have been
evaluated by tracing the change of a quantity $N_{eff}$, which is
defined as $N_{eff} = (\mu /\sigma)^2$, by using the mean ($\mu$) and
sigma ($\sigma$) of the recorded ADC spectrum.  The quantity $N_{eff}$
represents the effective photostatistics for the spectrum, which is a
convolution of a single-photoelectron (pe) response of the device and
the Poisson statistics with $N_{pe}$, the average number of
photoelectrons emitted from the photo-cathode.  For FM-PMTs, the ratio
$N_{pe}/N_{eff}$ is about 2 at $B$ = 0.  The relatively large excess
noise factor is due to the fact that a single-photoelectron spectrum
does not have any characteristic peak~\cite{acc-EnomotoA332}.
Reduction of $N_{eff}$ in a magnetic field has been measured for 2
in., 2.5 in. and 3 in. FM-PMTs, with $N_{eff}$ at $B$ = 0
($N^0_{eff}$) of about 20.
%%%%%%%%%%%%%%%%%%%%%%%%%%%(Table 2	acc-IijimaA387)
Table \ref{ACC} shows the ratio $N_{eff}/N^0_{eff}$ at $B$ = 1.5 T for
two ranges of applied HVs.  As the field $B$ increases, the resolution
deteriorates and the quantity $N_{eff}$ decreases. The decrease in
$N_{eff}$ is larger when the field is at a large angle such as 30$^o$
than the case of $\theta = 0^o$, and the decrease in $N_{eff}$ at a
large angle is more significant for smaller FM-PMTs.  However, it is
noticed that, in a magnetic field, the resolution improves, namely the
quantity $N_{eff}$ increases, by applying higher voltage, while no
significant change is found in absence of the field.\\
%%%%%%%%%%%%%%%% Table 1 %%%%%%%%%%%%%%
\begin{table}
\caption{Measured ratio $N_{eff}/N^0_{eff}$ at B = 1.5 T.  The number
of tested samples for each condition is shown in parentheses.}
\vspace{6mm}
\begin{center}
\begin{tabular}{c c c c c}
\hline \hline
PMT & \multicolumn{2}{c}{2000 - 2200 V}& \multicolumn{2}{c}{2600 - 2800 V}\\
\cline{2-5}
diam. &\multicolumn{1}{c}{	}&\multicolumn{1}{c}{  }&\multicolumn{1}{c}{  }&\multicolumn{1}{c}{	 }\\

[in.] & $\theta$ = 0$^o$ & $\theta$ = 30$^o$ & $\theta$ = 0$^o$ & $\theta$ = 30$^o$\\
\hline
2 & 0.74 (7) & 0.64 (10) & 0.75 (10) & 0.73 (10)\\
2.5&0.80 (5) & 0.73 (5) & 0.85 (8) & 0.85 (8)\\
3 & 0.79 (2) & 0.77 (4) & 0.87 (6) & 0.99 (7)\\
\hline \hline
\end{tabular}
\label{ACC}
\end{center}
\vspace{3mm}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACC electronics (for NIM-A) added 09-Jan-2000,
%%% written by D. Marlow (Princeton)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% NIM paper section on ACC Readout Electronics
\subsection{ACC Readout Electronics}

Signals from the FMPMT's are processed using the signal chain shown in
Fig.~\ref{ACCRO:fig1}.  The system comprises three main elements: i)
low-noise preamplifiers mounted directly on the FMPMT bases, ii)
specially designed charge-to-time converter boards, and iii) LeCroy
Model~1877 pipeline TDCs.  The time-to-converter approach makes
efficient use of the data acquisition system by combining both pulse
height and timing information into a single channel.  Moreover, the
pipelined nature of the TDC eliminates the need for the bulky delay
cables found in traditional gated-integrator data acquisition schemes.

\begin{figure}[htbp]
 \begin{center}
  \epsfxsize 5.0 truein \epsfbox{acc/system_block.eps} 
 \end{center} 
 \caption{System-level block diagram of the ACC readout electronics.}
 \label{ACCRO:fig1}
\end{figure}

\subsubsection{Preamplifier Base Assemblies}

The preamplifier base assemblies consist of resistive dividers formed
using surface mount resistors attached to a printed circuit boards
soldered directly to the wire leads of the PMTs.  The total string
resistance was chosen to be fairly high ($\sim 25$~M$\Omega$) so as to
keep the power dissipation to a minimum ($\sim 0.25$~W per base under
normal conditions).

The preamplifiers are based on the MAXIM MAX4107 low-noise
preamplifier chip.  This device was selected for its large bandwidth
(350~MHz at $A_V=10$) and its low noise ($0.75~{\rm nV}\sqrt{\rm Hz}$
equivalent input noise).  The 150~mW power dissipation, while not
negligible, did not present a significant thermal management problem.

The FMPMTs were sorted according to gain and matched to preamplifiers,
which were fabricated with fixed voltage gains of 10, 20, 40, and 70.
The outputs of the preamplifiers drive 30-m-long coaxial cables
connected to the charge-to-time converter boards, which are situated
in the electronics hut.

Residual tube-to-tube gain variations and gain changes over time were
compensated by adjusting the high voltage applied to the divider
string.  Each FMPMT in the system was biased by a separate channel of
the LeCroy Model 1461 12-channel high voltage modules.  These modules
were computer controlled using special software developed for the
purpose.

\subsubsection{Charge-to-Time Converter Modules}

A charge-to-time (QTC) circuit is used to convert the area of the
analog pulses from the FMPMTs to a digital pulses having widths
proportional to the input charges.  The leading edge of the QTC output
pulse is the same as leading edge time of its input pulse (plus a
constant offset) so that timing and pulse height information is
encoded in a single pipeline TDC channel.  Each QTC board comprises 26
channels, which is the number of FMPMTs in a single $\phi$-sector of
the barrel ACC array.  Physically the boards are 9U$\times$400 VME
boards, but the modules do not implement a standard VME interface.

Fig.~\ref{ACCRO:fig2} is a block diagram of one QTC channel.  The
input circuit of the QTC employs a Maxim MAX4144 differential line
receiver.  The input of the line receiver can be set to a single-ended
configuration using jumpers, as was done in this case.  The next stage
employs an Analog Devices AD603 variable gain amplifier (VGA), which
provides computer controlled gain adjustment over a range of $-11$~db
to $+31$~db.  The VGA stage is followed by a filter circuit which
eliminates high frequency noise.

\begin{figure}[htbp]
 \begin{center}
  \epsfxsize 5.0 truein \epsfbox{acc/mqt301.eps} 
 \end{center} 
 \caption{Block diagram of a single charge-to-time converter channel.}
 \label{ACCRO:fig2}
\end{figure}

The output of the filter amp is routed through a lumped delay line
($t_d = 50$~ns) to the input of the LeCroy MQT301 charge-to-time
integrated circuit.  The MQT301 monolithic is the heart of the circuit
and performs the actual charge-to-time conversion.

The output signal from the filter amp is routed to a second amplifier
and then on to a discriminator circuit based on the Analog Devices
AD96687 ECL comparator.  This output from this discriminator is
ultimately used to gate the internal integrator in the MQT301 chip.
The additional gain stage in front of the AD96687 ensures that the
circuit will be sensitive to the smallest signals of interest.
(Signals too small to fire the dicriminator are obviously ignored,
even if they result in a measurable charge at the MQT301 input.)  A
pair of ECL flip-flops, a second AD96687 channel, and a collection of
ECL AND gates are used to implement logic circuitry that sets the MQT
gate width and guards against fault conditions caused by input pileup.

Each of the 26 channels on a QTC board has three independent
adjustments: i) a VGA gain setting, ii) a discriminator threshold
setting, and iii) an MQT301 gate-width adjustment.  These settings are
controlled using MAXIM MAX529 octal serial DACs.  The serial DACs are
set remotely from a computer using a simple serial interface.

%%% commented for insertion in total paper
%\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Monitoring system}

A monitoring system for ACC has been designed and
constructed~\cite{acc-KhanA413}. It consists of blue LEDs, a diffuser
box, and optical distributors. The monitoring system is required to
supply roughly the same light intensity as that of \v{C}erenkov lights
generated by light velocity particles in each ACC module,
i.e. approximately 18 photoelectrons.  The monitoring light intensity
should be stable ($\sim$95 \%) in terms of aging and temperature
variation to measure possible drifts of PMT gains and light yields of
aerogels in a long period and those due to temperature changes.\\

The employed LED light source has been observed to have the long term
stability of 98.0 $\pm$ 0.4 \%.  The measured temperature coefficient
of the LED is (- 0.003 $\pm$0.035)\%/$^oC$ over a temperature range of
5 - 70$^oC$.  The uniformity of light intensity of the monitoring
system has been measured to be 94.20 $\pm$ 0.09 \%. The overall
performance on uniformity and intensity of the light output has been
found to satisfy all the requirements for the ACC monitoring system.

\subsubsection{Monte Carlo simulation}

In order to understand the performance of ACC, a Monte Carlo
program~\cite{acc-SudaA406} has been developed to simulate the
behavior of \v{C}erenkov photons in the aerogel as realistically as
possible.  The program consists of two parts: photon transportation
inside aerogel tiles and one dimensional amplification in an FM-PMT.
All the conceivable effects are taken into account as functions of
wavelength. The only unknown factor is the absorption in the aerogel,
which is treated as a free parameter and later determined by comparing
simulation results with the test beam data.\\

A \v{C}erenkov photon is transported inside the aerogel radiator
according to the photon-transport algorithm developed in the present
simulation. The program simulates the output photo-electron yields as
accurately as 5 \% with only a single parameter.  The experimental
data of a pulse-height spectrum for single-photoelectron events is
well reproduced~\cite{acc-EnomotoA332}.  The agreement between the
simulation and data for the excess noise factor with and without
magnetic fields is satisfactory.  The incident position dependence of
the light yields is also well reproduced by this simulation.\\

This simulator is implemented in a Belle standard detector simulator
based on the GEANT 3.  Good performance of pion/kaon separation by ACC
has been demonstrated successfully.


\subsection{Performance of ACC}

The performance of prototype ACC modules has been tested using the
$\pi$2 beam line at KEK PS.
%The number of photoelectrons obtained for 3.5 GeV/c negative pions is
%18.2, 20.3, and 20.3 for $n$ = 1.01, 1.015, and 1.02 silica 
%aerogels, respectively.	 
Typical pulse-height distributions for 3.5 GeV/c pions and protons
measured by an aerogel counter, with $n$ = 1.015 and read by two 2.5
in. FM-PMTs, are shown in
%%%%%%%%%%%%%%%%(Fig. 5 acc-SumiyoshiA433)
Figs. \ref{eps-acc-ppispectra}(a) and (b), with and without a magnetic
field of 1.5 T, respectively.  The number of photoelectrons ($N_{pe}$)
for 3.5 GeV/c negative pions is measured to be 20.3.  In the case of
Fig. \ref{eps-acc-ppispectra}(b) we used a preamplifier and applied a
higher HV to get almost the same gain as that without a magnetic
field.  Pions and protons are clearly separated by more than
3$\sigma$.  It was found that cracks in the aerogel do not make a
difference in the light yield.\\

%%%%%%%%%%%%% Figure 6 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{-25mm}
\begin{center}
\centerline{\hspace{25mm}\psfig{file=acc/pulse_height.eps,width=18cm,angle=0}}
\vspace{-17cm}
\caption{Pulse-height spectra for 3.5 GeV/c pions (above threshold)
and protons (below threshold) obtained by a single module of ACC in
(a) non-magnetic field and (b) a magnetic field of 1.5 T. Silica
aerogels with $n$ = 1.015 were stacked to form the module.}
\label{eps-acc-ppispectra}
\end{center}
\vspace{3mm}
\end{figure}

%%%%%%%%%%%%% Figure 7 %%%%%%%%%%%%%%
\begin{figure}[hbt]
%\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/accbhabha.eps,width=9cm,angle=0}}
%\vspace{2mm}
\caption{Pulse-height spectra in units of photoelectrons observed by
barrel ACC for electrons and kaons.  Kaon candidates were obtained by
$dE/dx$ and TOF measurements.  The Monte Carlo expectations are
superimposed.}
\label{eps-acc-bhabha}
\end{center}
%\vspace{3mm}
\end{figure}

After ACC was installed into the Belle detector in December 1998, the
initial calibration of the detector was carried out using cosmic
rays. The Belle detector has been rolled into the interaction point
and commissioned with $e^+e^-$ beams since May 1999.\\

%%%%%%%%%%%%%%%%%(Fig. 5 acc-IijimaHamamatsu1999)
Figure \ref{eps-acc-bhabha} shows the measured pulse height
distribution for the barrel ACC for $e^{\pm}$ tracks in Bhabha events
and also $K^{\pm}$ candidates in hadronic events, which are selected
by TOF and $dE/dx$ measurements~\cite{acc-IijimaHamamatsu1999}. The
figure demonstrates a clear separation between high energy electrons
and below-threshold particles. It also indicates good agreement
between the data and Monte Carlo simulations~\cite{acc-SudaA406}.\\

A careful calibration for the pulse height of each FM-PMT signal has
been performed with $\mu$-pair events.
%%%%%%%%%%%%%%%%%%(Fig. 6 acc-IijimaHamamatsu 1999)
Figs. \ref{eps-acc-photoelectron}(a) and (b) show the average number
of photoelectrons $<N_{pe}>$ for each counter row in the barrel ACC
and each layer of the end-cap ACC, respectively.  In the barrel ACC
each row has on average 60 boxes and the row number is given from left
to right in Fig. \ref{eps-acc-arrangement}.  The layer number of the
end-cap ACC is given from the inner to the outer side. The light yield
for the $\mu$ tracks depends on the refractive index of aerogel
radiators, size and number of FM-PMTs attached on the counter module,
and geometry of the counter module box.  The light yield ranges from
10 to 20 for the barrel ACC and from 25 to 30 for the end-cap ACC, high
enough to provide useful $\pi/K$ separation.

%%%%%%%%%%%%% Figure 8 %%%%%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/accnope.eps,width=12cm,angle=0}}
\vspace{2mm}
\caption{Average number of photoelectrons $<N_{pe}>$ for (a) each
counter row in barrel ACC and (b) each layer in end-cap ACC.}
\label{eps-acc-photoelectron}
\end{center}
\vspace{0mm}
\end{figure}
\clearpage

%%%% Hanagaki 11-20-00
%%%%\begin{document}

\subsection{Particle Identification of Electrons and Charged Kaons, EID and KID}
In the analyses of physics data of Belle, EID and KID are performed by
using data samples measured with CDC, ACC, TOF, and ECL.

\subsubsection{EID and fake rate}

Electrons are identified by using the following discriminants:
\begin{itemize}
\item  the ratio of energy deposited in ECL
and charged track momentum measured by CDC, 
\item  transverse shower shape at ECL, 
\item  the matching between a cluster at ECL
and charged track position extrapolated to ECL,
\item  $dE/dx$ measured by CDC, 
\item  light yield in ACC, and
\item  time of flight measured by TOF.
\end{itemize}

We made probability density functions (PDF) for the discriminants
beforehand.  Based on each PDF, likelihood probabilities are
calculated with track-by-track basis, and unified into a final
likelihood output.  This likelihood calculation is carried out taking
into account the momentum and angular dependence.  Fig.~\ref{fig:leid}
shows the output from the above procedure.  Closer to unity the
particle is more likely to be an electron.  The solid (dashed)
histogram shows for $e^{\pm}$ in $e^{+}e^{-} \rightarrow
e^{+}e^{-}e^{+}e^{-}$ data ($\pi^{\pm}$ in $K_{S} \rightarrow
\pi^{+}\pi^{-}$ decays in data).  The clear separation can be seen.

\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/leid_1120.eps,width=10cm,angle=0}}
\vspace{2mm}
  \caption{
			 The distribution of final unified discriminant to
			 identify electrons. 
			 The solid histogram is for electrons in
			 $e^{+}e^{-} \rightarrow e^{+}e^{-}e^{+}e^{-}$ events
			 and the dashed one for charged pions.
			}
	\label{fig:leid}
  \end{center}
\end{figure}
	
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/eff_fake_1120.eps,width=10cm,angle=0}}
\vspace{2mm}
   \caption{Electron identification efficiency (circles) and fake
			 rate for charged pions (squares).
			 Note the different scales for the efficiency and fake
			 rate.
			 }
	\label{fig:eff_fake}
  \end{center}
\end{figure}

The efficiency and fake rate are displayed in Fig.~\ref{fig:eff_fake}
using electrons in real $e^{+}e^{-} \rightarrow e^{+}e^{-}e^{+}e^{-}$
events for the efficiency measurement, and
$K_{S}\rightarrow\pi^{+}\pi^{-}$ decays in real data for the fake rate
evaluation.  For momentum greater than 1~GeV/$c$, the electron
identification efficiency is maintained to be above 90\% while the
fake rate to be around 0.2 to 0.3\%.


\subsubsection{KID and pion fake rate}

The $K/\pi$ identification is carried out by combining information
from three nearly-independent measurements:

\begin{itemize}
\item  $dE/dx$ measurement by the CDC,
\item  TOF measurement, and
\item  measurement of the number of photoelectrons ($N_{pe}$) in the ACC.
\end{itemize}

As in the case of EID, the likelihood function for each measurement
was calculated and the product of the three likelihood functions
yields the overall likelihood probability for being a kaon or a pion,
$P_K$ or $P_{\pi}$.

A particle is then identified as a kaon or a pion by cutting on the
likelihood ratio ($PID$):
\begin{equation}
PID(K)	  =	 \frac{P_K}{P_K+P_{\pi}} 
\end{equation}
\begin{equation}
PID(\pi)  =	 1 - PID(K) ~. 
\end{equation}

The validity of the $K/\pi$ identification has been demonstrated using
the charm decay, $D^{\ast +} \rightarrow D^0 \pi^+$, followed by $D^0
\rightarrow K^- \pi^+$.  The characteristic slow $\pi^+$ from the
$D^{\ast +}$ decay allows these decays to be selected with a good
$S/N$ ratio (better than 30), without relying on particle
identification.  Therefore, the detector performance can be directly
probed with the daughter $K$ and $\pi$ mesons from the $D$ decay,
which can be tagged by their relative charge with respect to the slow
pion.  Fig.~\ref{fig:dstar_2d} shows two dimensional plots of the
likelihood ratio $PID(K)$ and measured momenta for the kaon and pion
tracks.  The figure demonstrates the clear separation of kaons and
pions up to around 4\,GeV/$c$.  The measured $K$ efficiency and $\pi$
fake rate in the barrel region are plotted as functions of the track
momentum from 0.5\, to 4.0 GeV/$c$ in Fig.~\ref{fig:dstar_momdep}.
The likelihood ratio cut, $PID(K) \geq 0.6$, is applied in this
figure.  For most of the region, the measured $K$ efficiency exceeds
80\%, while the $\pi$ fake rate is kept below 10\%.

\begin{figure}[hbt]
\vspace{-5mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/dstar_2d.eps,width=10cm,angle=0}}
\vspace{-2mm}
 \caption{Likelihood ratio $PID(K)$, versus	 momenta for daughter
	tracks from $D^0 \rightarrow K^-\pi^+$ decays, tagged by the
	charge of the slow $\pi^+$'s.
	The open circles correspond to kaons and the cross points to pions.}
  \label{fig:dstar_2d}
\end{center}
\end{figure}

\begin{figure}[hbt]
\vspace{-10mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=acc/dstar_momdep.eps,width=10cm,angle=0}}
\vspace{2mm}

  \caption{ $K$ efficiency and $\pi$ fake rate, measured with
		   $D^{\ast +} \rightarrow D^{0}(K \pi) + \pi^+$ decays, 
		   for the barrel region.
		   The likelihood ratio cut $PID(K) \geq 0.6$ is applied.}
  \label{fig:dstar_momdep}
\end{center}
\end{figure}
