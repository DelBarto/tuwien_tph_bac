\section{Introduction}

The Belle collaboration evolved from the $B$-factory task force that
was organized to study the physics potential of a high-luminosity,
asymmetric e$^+$e$^-$ collider operating at the $\Upsilon$(4S)
resonance.  In particular, the task force addressed the possibilities
for experiments that tested the Kobayashi-Maskawa mechanism for
$CP$-violation~\cite{KM}. It was demonstrated that such tests could be
done with a data of $\sim$ 10$^7$ $B$-meson decays, corresponding to
integrated luminosities at the $\Upsilon$(4S) of order 100 $fb^{-1}$,
accumulated with a 4$\pi$ detector with state-of-the-art
capabilities~\cite{KEK90_92_93}.\\

The scientific goals of the Belle collaboration were discussed in a
Letter of Intent~\cite{loi} submitted to the March 1994 TPAC (TRISTAN
Program Advisory Committee) meeting. The LoI describes the
implications of these goals for the detector and provides a reference
detector design based on the R\&D program initiated by the task
force. With the approval of the LoI the Technical Design Report was
written by the Belle collaboration~\cite{TDR95}.\\

Figure \ref{int-sideview} shows the configuration of the Belle
detector. The detector is configured around a 1.5 T superconducting
solenoid and iron structure surrounding the KEKB beams at the Tsukuba
interaction region~\cite{KEKB}.  The beam crossing angle is $\pm$ 11
mr.  $B$-meson decay vertices are measured by a silicon vertex
detector (SVD) situated just outside of a cylindrical beryllium
beam-pipe.  Charged particle tracking is provided by a wire drift
chamber (CDC).  Particle identification is provided by $dE/dx$
measurements in CDC, and aerogel \v{C}erenkov counters (ACC) and
time-of-flight counters (TOF) situated radially outside of CDC.
Electromagnetic showers are detected in an array of CsI($Tl$) crystals
located inside the solenoid coil. Muons and $K_L$ mesons are
identified by arrays of resistive plate counters interspersed in the
iron yoke. The detector covers the $\theta$ region extending from
17$^o$ to 150$^o$.  A part of the otherwise uncovered small-angle
region is instrumented with a pair of BGO crystal arrays (EFC) placed
on the surfaces of the QCS cryostats in the forward and backward
directions.  The expected (or achieved) performance of the detector is
summarized in Table \ref{int-performance}.

%%%%%%%%%%%%% Figure   Progress97 Fig.2.1  %%%%%%%%%%%%%%%%%%

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=int/side-color.eps,width=13.5cm}}
%\centerline{\psfig{file=int/belle_cros.eps,width=11cm}}
\vspace{5mm}
\caption{Side view of the Belle detector.}
\label{int-sideview}
\end{center}
\vspace{8mm}
\end{figure}

\begin{table}
\caption{Performance parameters expected (or achieved) for the Belle detector.}
\begin{center}
\vspace{3mm}
\begin{tabular}{|c|c|c|c|c|}
\hline \hline
Detector & Type & Configuration & Readout & Performance\\
\hline
Beam pipe & Beryllium & Cylindrical, r = 20 mm && He gas cooled\\
& double-wall &0.5/2.5/0.5(mm) = Be/He/Be & & \\
\hline
EFC & BGO & Photodiode readout & 160 $\times$ 2 & Rms energy resolution:\\
&& Segmentation :&& 7.3 \% at 8 GeV\\
&&32 in $\phi$; 5 in $\theta$&&5.8 \% at 3.5 GeV\\
\hline

SVD &Double&Chip size: 57.5$\times$33.5 mm$^2$& $\phi$ : 40.96 k& $\sigma_{\Delta_z} \sim$ 80 $\mu$m \\
& sided & Strip pitch: 25 (p)/50 (n) $\mu$m	 &	$z$ : 40.96 k& \\
& Si strip & 3 layers: 8/10/14 ladders	&& \\
\hline
CDC & Small cell & Anode : 50 layers & A : 8.4 k & $\sigma_{r\phi}$ = 130 $\mu$m\\
& drift & Cathode : 3 layers & C : 1.8 k & $\sigma_z$ = 200 $\sim$ 1400 $\mu$m\\& chamber & r = 8.3 - 86.3 cm & & $\sigma_{p_t}/p_t$ = 0.3 \%$\sqrt{p^2_t + 1}$\\
&& - 77 $\leq$ $z$ $\leq$ 160 cm && $\sigma_{dE/dx}$ = 6 \%\\
\hline
ACC & Silica & 960 barrel/228 end-cap& &$N_{p.e.} \geq$ 6\\
& aerogel & FM-PMT readout&&K/$\pi$ separation :\\
&&&& 1.2 $<$ p $<$ 3.5 GeV/c\\
\hline
TOF& Scintillator & 128 $\phi$ segmentation & 128 $\times$ 2 &$\sigma_t$ = 100 ps\\
&& $r$ = 120 cm, 3-m long &&K/$\pi$ separation :\\
TSC&&64 $\phi$ segmentation&64& up to 1.2 GeV/c\\
\hline
ECL & CsI & Barrel : $r$ = 125 - 162 cm	 &6624& $\sigma_E/E$ = 1.3 \%/$\sqrt{E}$\\
&(Towered-& End-cap : $z$ =	& 1152 (F)& $\sigma_{pos} $ = 0.5 cm/$\sqrt{E}$\\
&structure)& -102 cm and +196 cm &960 (B) & (E in GeV)\\
\hline
KLM & Resistive & 14 layers & $\theta$ : 16 k & $\Delta\phi = \Delta\theta = 30 ~mr$\\
& plate &(5 cm Fe + 4 cm gap) &$\phi$ : 16 k & for $K_L$\\
& counters & 2 RPCs in each gap && $\sim$ 1 \% hadron fake\\
\hline
Magnet& Supercon. & Inner radius = 170 cm && B = 1.5 T\\
\hline \hline

\end{tabular}
\end{center}
\label{int-performance}
\vspace{10mm}
\end{table}

At the time of writing of TDR the detector technologies for particle
identification and extreme forward calorimeters were not finalized,
and R\&D works were continued.  All the other detector components
entered the full construction stage.  After extensive studies and
tests of a few options for particle identification techniques the
aerogel \v{C}erenkov counter (ACC) system was chosen as the particle
identification system. The extreme forward calorimeter system with BGO
crystal arrays was also chosen as EFC over the option of a
silicon-tungsten sandwich calorimeter.  Confronted with various
technical difficulties the design of SVD was changed to the present
design following the recommendation made by the SVD review committee
of June 1997.\\

Along with development and construction works of readout electronics
for all the detector components, the trigger, data acquisition, and
computing systems are also developed.\\

The present report summarizes the results of works by the Belle
collaboration during the design, construction, testing, and
commissioning stages of the Belle detector.
