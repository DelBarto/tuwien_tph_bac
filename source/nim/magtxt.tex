
\section{Detector Solenoid and Iron Structure}

\subsection{Solenoid Magnet}
A superconducting solenoid provides a magnetic field of 1.5 T in a
cylindrical volume of 3.4 m in diameter and 4.4 m in
length~\cite{solenoid,TDR95,Progress97}.  The coil is surrounded by a
multi layer structure consisting of iron plates and calorimeters,
which is integrated into a magnetic return circuit. The main coil
parameters are summarized in
%%%%%%%%%%%% Table 2.10 Progress97 %%%%%%%%%%%
Table \ref{solenoid}.  The overall structure of the cryostat and the
schematic drawing of the coil cross section are shown in
%%%%%%%%%%%% Fig. 8.1 TDR95 %%%%%%%%%%%%
Fig. \ref{coil-config}.

\subsection{Iron Yoke}
The iron structure of the Belle detector serves as the return path of
magnetic flux and an absorber material for
KLM~\cite{TDR95,Progress97}.  It also provides the overall support for
all of the detector components.  It consists of a fixed barrel part
and movable end-cap parts, both on the base stand.  The barrel part,
shown in
%%%%%%%%%%%% Fig. 2.27 Progress97 %%%%%%%%%%%%%%%%
Fig. \ref{barrel-yoke}, consists of eight KLM blocks and 200-mm thick
flux-return plates surrounding the outermost layers of of the KLM
blocks.  Neighboring KLM blocks are joined using fitting blocks.  Each
end-cap part can be retracted for an access to the inner detectors. \\

The weight of the iron yoke is 608 and 524 (= 262 $\times$ 2) tons for
the barrel yoke and end-cap yokes, respectively.

\begin{table}
\caption{Main parameters of the solenoid coil.}
\vspace{3mm}
\begin{center}
\begin{tabular}{ll}
\hline \hline
Items & \hspace{2cm} Parameters\\
\hline
Cryostat&\\
\hspace{1cm} Radius: outer/inner &\hspace{2cm}	2.00~m/1.70~m\\
Central field & \hspace{2cm} 1.5~T\\
Total weight & \hspace{2cm} 23~$t$\\
Effective cold mass &\hspace{2cm}  $\sim$ 6~$t$\\
 Length & \hspace{2cm} 4.41~m\\
Coil&\\
\hspace{1cm} Effective radius &\hspace{2cm}	 1.8~m\\
\hspace{1cm} Length &\hspace{2cm}  3.92~m\\
\hspace{1cm} Conductor dimensions &\hspace{2cm}	 3 $\times$ 33 mm$^2$\\
\hspace{1cm} Superconductor &\hspace{2cm} NbTi/Cu\\
\hspace{1cm} Stabilizer & \hspace{2cm} 99.99 \% aluminum\\
\hspace{1cm} Nominal current &\hspace{2cm}	4400~A\\
\hspace{1cm} Inductance & \hspace{2cm} 3.6~H\\
\hspace{1cm} Stored energy & \hspace{2cm} 35~MJ\\
\hspace{1cm} Typical charging time &\hspace{2cm}  0.5~h\\
Liquid helium cryogenics &\hspace{2cm} Forced flow two-phase\\
Cool down time &\hspace{2cm} $\le$ 6 days\\
Quench recovery time &\hspace{2cm} $\le$ 1 day\\
\hline \hline
\end{tabular}
\label{solenoid}
\end{center}
\vspace{20mm}
\end{table}

\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=mag/coil.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{An outlook of the solenoid and the cross sectional view of
the coil.}
\label{coil-config}
\end{center}
\vspace{0mm}
\end{figure}

\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=mag/iron_yoke.eps,width=16cm,angle=0}}
\vspace{-10mm}
\caption{Barrel part of the iron yoke.}
\label{barrel-yoke}
\end{center}
\vspace{5mm}
\end{figure}

\subsection{Magnetic Field Mapping of the Belle Solenoid}

The magnetic field mapping was carried out with the accelerator
magnets located inside the Belle solenoid excited to their nominal
field values~\cite{field_mapping}. The two superconducting magnet
complexes, QCS-R and QCS-L, are inserted in the holes along the axis
of the end yokes.  Each magnet complex consists of a solenoidal coil
for compensation of the Belle solenoidal field, a quadrupole for
focusing of the beams onto the interaction point, and several
correction coils all located in a single cryostat~\cite{qcs_complex}.

Figure \ref{mgf_end_side} shows the side and end views of the field
mapping arrangement and a conceptual illustration of the field mapping
device is shown in Fig. \ref{conceptual}.  The volume to be mapped was
reasonably isolated from the outside because of the limited gap
between the pole tips and the QCS cryostat.  Since the accessibility
was limited, the field mapping device must be fully contained inside
the Belle solenoid.  Conventional magnetic motors could not be used to
drive the device.  We used an ultrasonic motor as a
driver~\cite{usm-drive}.

\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=mag/MGF_end_side_view2.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Side and end views of the field mapping device.  Some of the
rods connecting both end plates are not shown.}
\label{mgf_end_side}
\end{center}
\vspace{0mm}
\end{figure}


\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=mag/concept2.eps,width=12cm,angle=0}}
\caption{Conceptual illustration of the field mapping device.}
\label{conceptual}
\end{center}
\vspace{5mm}
\end{figure}

A commercial field probe with three orthogonally oriented Hall sensors
was used to measure the $r, z$ and $\phi$ field
components~\cite{hall_probe}.  An NMR probe installed on one of the
connecting rods provided an absolute calibration of field strength
with a precision better than 1 Gauss~\cite{nmr_probe}.  It was also
used to monitor the stability of the magnet during the measurement.

A field map of 100,000 points was made for a period of a
month. Fig. \ref{field_contour} shows a contour plot of fields
measured inside the tracking volume for the nominal magnet settings.
The field strength is shown in Fig. \ref{bzbr} as a function of z for
various radii.  The tracking performance was evaluated with the
reconstructed mass peak of $J/\psi$ particles from $B$ meson decays.
The fitted value of the peak mass was 3.089 GeV/c$^2$ compared with
the established value of 3.0969 GeV/c$^2$.  Thus, the uncertainty in
the absolute calibration of the present measurement is estimated to be
approximately 0.25\%.


\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=mag/contour_accpt.eps,width=10cm,angle=0}}
\caption{Contour plot of magnetic fields measured in the Belle
coordinate system with the origin at the interaction point.}
\label{field_contour}
\end{center}
\vspace{5mm}
\end{figure}

\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=mag/bzbr.eps,width=10cm,angle=0}}
\caption{Field strength as a function of $z$ for $r$ = 0, 50, and 80 cm.}
\label{bzbr}
\end{center}
\vspace{5mm}
\end{figure}
