\section{Data Acquisition}

In order to satisfy the data acquisition requirements so that it works
at 500 Hz with a deadtime fraction of less than 10\%, the
distributed- parallel system has been devised. The global scheme of
the system is shown in Fig.~\ref{daq-overview}.  The entire system is
segmented into 7 subsystems running in parallel, each handling the
data from a sub-detector.  Data from each subsystem are combined into
a single event record by an event builder, which converts
``detector-by-detector'' parallel data streams to an
``event-by-event'' data river. The event builder output is transferred
to an online computer farm, where another level of event filtering is
done after fast event reconstruction. The data are then sent to a mass
storage system located at the computer center via optical fibres.

A typical data size of a hadronic event by $B\bar{B}$ or $q\bar{q}$
production is measured to be about 30 kB, which corresponds to the
maximum data transfer rate of 15 MB/s.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/daq-overview.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Belle DAQ system overview.}
\label{daq-overview}
\end{center}
\vspace{8mm}
\end{figure}

\subsection{Readout}
\subsubsection{FASTBUS TDC readout systems}

We adopted a charge-to-time (Q-to-T) technique to read out signals
from most of the detectors.  Instead of using ADC to digitize the
amplitude of a signal, the charge is once stored in a capacitor and
discharged at a constant rate. Two pulses, the separation of which is
proportional to the signal amplitude, are generated at the start and
stop times of the discharge. By digitizing the time interval of the
two timing pulses with respect to a common stop timing, we can
determine both the timing and the amplitude of the input signal. The
hold timing is generated either by a self-gate method or the trigger
signal from a timing distributer module, TDM.  The mechanism is
illustrated in Fig.~\ref{daq-q-to-t}.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/q-to-t.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{Concept of the Q-to-T and TDC based digitization.}
\label{daq-q-to-t}
\end{center}
\vspace{8mm}
\end{figure}

For time digitization, we use a multi-hit FASTBUS TDC module, LeCroy
LRS1877S.  Up to 16 timing pulses are recorded for 96 channels in a
single width module with a sparsification capability.  The least
significant bit is 500 ps. A programmable time window has a 16-bit
range, which corresponds to a full scale of 32 $\mu$s.

Most of the detectors, CDC, ACC, TOF, ECL and EFC, are read out by
using the Q-to-T and TDC technique.  The use of the Q-to-T technique
reduces the number of cables for CDC wires into a half.  In the case
of TOF, the time resolution of 100 ps is achieved by using a time
stretcher which expands the pulse width by a factor of 20. In the case
of ECL, a 16-bit dynamic range is achieved by using three ranges. A
signal is divided and fed into three preamplifiers of different gains,
converted to the time pulse and merged with an exclusive-$OR$ logic
circuit.  For a small signal, all the three ranges give short pulses
and the final output has 4 time pulses.  By contrast, for a large
signal, high and middle gain pulses overflow and the final output has
only 2 time pulses from the low gain range.  After digitization, the
range is identified by the number of time pulses for the pulse.

The KLM strip information is also read out by using the same type of
TDC.  Strip signals are multiplexed into serial lines and recorded by
TDC as the time pulses.  These pulses are decoded to reconstruct hit
strips.  Similarly, a large number of trigger signals including those
for the intermediate stages are recorded using TDC.  A full set of
trigger signals gives us complete information for trigger study.

We developed a unified FASTBUS TDC readout subsystem that is
applicable to all the detectors except SVD.  A FASTBUS processor
interface, FPI, developed by us controls these TDC modules, and FPI is
controlled by a readout system controller in a master VME crate.
Readout software runs on the VxWorks real time operating system on a
Motorola 68040 CPU module, MVME162. Data are passed to an event
builder transmitter in the same VME crate. A schematic view of the
system is shown in Fig.~\ref{daq-tdc-readout}. The overall transfer
rate of the subsystem is measured to be about 3.5 MB/sec.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/tdc-readout.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{Schematic view of the FASTBUS TDC readout system.}
\label{daq-tdc-readout}
\end{center}
\vspace{8mm}
\end{figure}

\subsubsection{SVD readout system}

SVD data are read out from 81,920 strip channels, whose occupancy is
typically a few percent depending on the beam condition.  The charge
information is read out by intelligent flash ADC modules with an
embedded DSP, which performs data sparsification~\cite{daq1-svd}.  The
resulting data size is 10 to 20 kB, which is larger than the capacity
of a single event builder transmitter.  The SVD readout system is
divided into four VME crates, in each of which signals are read out in
the flash ADC modules and transferred to the event builder in
parallel.  The SVD readout sequence is synchronized from an external
control crate which receives the timing signal from the sequence
controller.  The schematic diagram of the SVD readout system with 4
readout VME crates is shown in Fig.~\ref{daq-svd}.

The readout software for flash ADC is combined with the event builder
transmitter software that runs on a SPARC VME CPU.  By doing this, the
overhead of extra data copying is minimized.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/svd.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Schematic view of the SVD readout system with 4 readout VME
crates.}
\label{daq-svd}
\end{center}
\vspace{8mm}
\end{figure}


\subsubsection{Readout sequence control}

A timing signal for a readout sequence is handled by a central
sequence controller and TDMs.  The sequence controller receives a
timing signal from the trigger system and distributes it to TDM
located at the VME crate of each detector readout system.  The
handshake sequence is simply accomplished with the level of a single
busy line from TDM to the sequence controller.  Upon receiving a
trigger signal, TDM generates an interrupt or raise a data ready flag
to start a readout sequence. The busy line has to be raised and reset
either by an external signal or by setting an internal register to
receive the next trigger.  The length of the deadtime after resetting
the busy line is programmable, in order to compensate the cable delay
between the readout crate and the front end electronics.

If no busy lines from the TDM modules are raised, the sequence
controller initiates a readout sequence by a timing signal received
from the trigger system.  The trigger related information, such as
trigger rates, deadtimes, and the beam bunch number, is monitored.
In addition, the response time of the busy line from each sub-detector
TDM is separately monitored, so that the location of a problem can be
easily identified.  The system is driven by 16/64 MHz clock pulses
derived from the accelerator RF, and 16 MHz clock pulses are also
distributed to TDM.

\subsection{Event Builder}

The event builder constructs a complete event data from detector
signals.  Data are sent from seven VME crates of the FASTBUS TDC
readout systems and four SVD VME crates. The output of the event
builder is connected to six VME crates of the online computer farm.
We use 1.2 Gbps ECL-level line, G-Link, for the data transfer layer
\cite{daq1-event-builder} . The event builder is a $12 \times 6$
barrel shifter consisting of 12 transmitters, 6 receivers and a set of
$4 \times 4$ and $2 \times 2$ barrel shifter switches. The data path
and the control path are shown in Fig.~\ref{daq-evb}.

The barrel shifting sequence is controlled by an external controller
through a 50 Mbps serial line, TAXI.  The barrel shifter switches the
configuration event-by-event according to the configuration given by
the external controller to form an event.

The readout VME crates are located in the electronics hut near the
detector, while the farm crates are in the control room.  Thus a long
connection of about 100 m is required.  G-Link signals are transferred
via optical fibers, and TAXI signals are transferred by unshielded
twisted pair cables.  The transmitter and receiver modules are built
as S-Bus adaptor cards, so that the VME SPARC CPU, FORCE CPU-7V, can
be used as the controller.  We are using Solaris OS for the control
software.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/evb.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{The data flow and the control flow of the event builder.}
\label{daq-evb}
\end{center}
\vspace{8mm}
\end{figure}

\subsection{Online Computer Farm}

The role of the online computer farm \cite{daq1-FARM} is to format an
event data into an off-line event format and to proceed background
reduction (the Level 3 trigger) using a fast tracking program
\cite{daq1-fast-tracking}.  Fig.~\ref{daq-farm} shows the architecture
of the online farm.  The farm consists of six VME crates. Each VME
crate contains a Power-PC based control processor, Motorola MVME-2604,
and 16 MC88100 event processors in four VME modules, Motorola
MVME-188s.  The interfaces for the event builder and the mass storage
system are also implemented in each crate.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/farm-dqm.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{The online computer farm, that consists of six VME crates of
processors. Also shown is the data monitoring system using an external 
Linux-SMP PC server for data quality monitoring.}
\label{daq-farm}
\end{center}
\vspace{8mm}
\end{figure}

In each crate, data from the event builder interface are distributed
to 16 event processors through the VME backplane, and the event
formatting and background reduction are performed in parallel. The
processed data are then sent to the mass storage interface. The
throughput of a crate is 2.8 MB/sec which corresponds to more than a
15 MB/sec transfer rate with all the six crates.

The software for the online farm can be developed on a host
workstation which has the off-line software development environment
and the executable image developed on the workstation can be directly
downloaded to the online farm.

\subsection{Data Monitoring System}

A fraction of events are sampled on the online computer farm and sent
to a PC server via Fast Ethernet for the real-time monitoring of data
quality. The sampling rate is typically up to 20 Hz.  On the PC server
which is operated with Linux, the sampled data are placed on the
shared memory and delivered to a set of monitoring programs under a
control of a buffer manager, NOVA.  Multiple monitoring programs
including event displays run in parallel.  The monitoring programs are
developed in the off-line environment and can be plugged into the
sampled data stream without affecting the execution of others.


\subsection{Mass Storage System}

Our mass storage system consists of two components. One is the data
transfer system to send the data from the online computer farm to the
tape library at the computer center which is located 2 km away from
the experimental hall. The other is the high-speed tape drive.  The
system is shown in Fig.~\ref{daq-farm-stor}.  The data transfer system
is based on a set of one-way reflective memory modules connected via
optical fibres. The point to point transfer rate is about 8
MB/sec. Data from the six online farm crates are first transferred to
three intermediate crates and then sent to the computer center via
three optical fibres.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/farm-stor.eps,width=8cm,angle=0}}
\vspace{5mm}
\caption{Data storage system that transfers data from the online
computer farm to the tape library system.}
\label{daq-farm-stor}
\end{center}
\vspace{8mm}
\end{figure}

The data are received by a SUN workstation using a reflective memory
module with the S-Bus interface and then written on a tape.  The tape
drive, SONY DIR-1000, has a capability to record data at a 15 MB/s
rate.  Three drives are connected to the SUN workstation via the SCSI
interface.  One tape can contain 80 MB, which corresponds to a total
capacity of 24 TB in the tape library.  While one of three tape drives
is used for recording, the second one stands by with a loaded tape,
and the third one is reserved as a backup drive. By loading tapes on
two drives, the tape change procedure completes in a few second
although the loading and unloading procedure usually takes 2 minutes.

\subsection{Run Control}
The control of all the Belle DAQ components is done based on the
Ethernet/Fast Ethernet connection. A communication software called
NSM, Network Shared Memory, is developed for the run control. NSM has
two functionalities. One is the shared memory facility over the
network space and the other is the function execution on a remote node
by sending a message. Fig.~\ref{daq-nsm} shows the architecture of
NSM. The run control and the monitoring of the DAQ system are
implemented over the NSM framework.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/nsm.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Architecture of Network Shared Memory, NSM.}
\label{daq-nsm}
\end{center}
\vspace{8mm}
\end{figure}


\subsection{Performance}

Figure~\ref{daq-deadtime} shows the total deadtime of the DAQ system
as a function of trigger rate measured in real beam runs. The typical
trigger rate is 200 - 250 Hz. The deadtime linearly increases as the
trigger rate increases and reaches up to 4\% at 250 Hz which is
consistent with the designed value. The overall error rate of the data
acquisition flow is measured to be less than 0.05\%.

\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=daq/deadtime.eps,width=8cm,angle=0}}
\vspace{5mm}
\caption{DAQ deadtime as a function of trigger rate.}
\label{daq-deadtime}
\end{center}
\vspace{8mm}
\end{figure}
