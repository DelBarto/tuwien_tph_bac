\section{Offline Computing System}

The computing and software system is of great importance to the Belle
experiment as very complex data analysis techniques using a large
amount of data are required for physics discoveries. A traditional HEP
computing model has been adopted by the Belle collaboration. Namely,
the Belle collaboration has chosen to use tape library systems with
the sequential access method for the input and output of experimental
data as the mass storage system.

\subsection{Computing Power}
The maximum data rate from the Belle detector, discussed in the
previous section, is 15 MB/s, which corresponds to the maximum trigger
rate of 500 Hz and the maximum event size of 30 KB. Raw data are
accumulated at the rate of about 5 MB/s, corresponding to 400 GB/d or
80 TB/y. The traditional-style data summary tapes, DSTs, are produced
from tape to tape by running the reconstruction algorithms on raw
data. DSTs contain only physics events and thus the size of DST is
reduced to 4 GB per pb$^{-1}$. As we have taken data of 6.8 fb$^{-1}$
between October 1999 and July 2000, we have produced 28 TB of DST.

In order to satisfy the above offline computing needs, a large
computing system has been set up at the computing research center of
KEK. The system consists of a computing server system, a RAID disk
system and tape library system for mass storage, work group servers,
and high speed network systems.  The network systems connect all the
sub-systems.  The main purpose of the offline computing system is to
process data taken by the Belle detector and to store them so that
users can analyze them.  Fig. \ref{fig_kekb-comp} shows a schematic
diagram of the Belle computing system.

The computing server system consists of seven Ultra Enterprise
servers. Each server has 28 167-MHz Ultra SPARC CPUs and is rated to
be more than 7000 SpecInt92s. Each server has 7 GB of physical memory
so that the parallel reconstruction processes can be run without
swapping memories.

\begin{figure}[hbt]
  \vspace*{5mm}
  \begin{center}
    \centerline{\psfig{file=cmp/kekb-comp.eps,width=12cm,angle=0}}
    \vspace*{3mm}
    \caption{Block diagram of the Belle computing system.}
    \vspace*{5mm}
    \label{fig_kekb-comp}
  \end{center}
\end{figure}

\subsection{Tape Libraries and Servers}
The tape library systems consist of (1) SONY PetaSite DMS-8800, (2)
SONY PetaSite DMS-8400 and (3) two sets of SONY PetaSite DMS-8400 with
PetaServe hierarchical storage system software. Tape library system
(1) has a capacity of 25 TB and contains two DIR-1000 tape drives with
a writing speed of 32 MB/s and nine DIR-1000M tape drives with a
writing speed of 16 MB/s. The two 32-MB/s DIR-1000 and one 16-MB/s
DIR-1000M drives are connected to the fast data transfer system and
used to write online data. Six 16-MB/s DIR-1000M tape drives are each
connected to six of seven computing servers and used for DST
production. The remaining two drives are connected to a less powerful
compute server (11CPU) and used for the backup of the system. Tape
library system (2) has a capacity of 75 TB and contains 15 DTF tape
drives with a data transfer speed of 12 MB/s. Attached on each
computing server are two DTF drives. Each set of tape library system
(3) can hold 25 TB of data. The PetaServe hierarchical storage system
software functions as if all files were on a hard disk.

The RAID system consists of six Gen5 (Strategy S-Series XL) storage
servers. Five of the six Gen5 servers have 640 GB each and the 6$th$
one 480GB. Each storage server has up to 8 independent SCSI host
connections and therefore the disks are connected to 31 data servers
and 11 work group servers.

The data servers and the compute servers are integrated into Fujitsu
Parallel Server AP3000 with a proprietary network, called AP-Net by
Fujitsu Ltd. The network has a two-dimensional torus topology with the
maximum point to point data transfer speed of 200 MB/s.

For each sub-detector group and each core software group, a work group
server has been allocated. The number of CPUs varies from 2 to 6
depending on the size of the group. Eight of twelve work group servers
have 2 to 6 250-MHz ultra SPARC II CPUs and four sets of 6 167-MHz
ultra SPARC CPUs. Each group server has a 40 or 80 GB RAID system
described above.  Users, who usually log in to these work group
servers, develop and debug programs, and analyze data.

In the RAID system the storage capacity of 1.36 TB, which is mounted
on the data servers, serves as NFS disks and holds mini-DST data. The
capacity of 1.12 TB is also mounted on the data servers and serves as
NFS disks with migration using 50 TB capacity of tape library (3).

All servers run Solaris 2.5.1 operating system. The group and
computing servers run DCE/DFS (Distributed Computing
Environment/Distributed File System) from IBM and LSF (Load Sharing
Facility, www.platform.com) from Platform computing. The KEKB
computing system described above was set up in January 1997 and has
been running since. The system is on a lease contract that ends in
January 2001. A new system will be installed to replace the current
system.

\subsection{PC Farms}
Two PC farms have been added to incorporate ever growing demands for
generic Monte Carlo generation. The first farm, added in 1999,
consists of 16 4CPU DELL Power Edge servers.  CPUs are 550 MHz Pentium
III Xeon processors. The second farm, which was added one year later,
consists of 20 4CPU DELL Power Edge servers with 550 MHz Pentium III
processors and 2.4 TB RAID systems attached to four 2 CPU servers. All
compute nodes are connected via 100 BaseT and two farms are connected
using GbE hub. These nodes are connected to two data servers of the
KEKB computing system. A little less than one million Monte Carlo
events can be generated in one day if the entire farms are used. The
third PC farm which consists of 40 sets of 2 800-MHz Pentium III
processors has been set up for DST production.  Two tape servers have
been set up to distribute data read from raw and/or DST tapes. The
tape servers are connected to PC farm disk servers via GbE (Giga bit
Ethernet).

\subsection{Offline Software}
All software except for a few HEP specific and non HEP specific free
software packages has been developed by the members of the Belle
collaboration. In particular, the mechanisms to handle event structure
and input and output formatting and to process events in parallel on a
large SMP (Symmetric Multiple Processor) compute server have been
developed locally using C and C++ programming languages. GEANT3 has
been used in simulations~\cite{GEANT3}.

The event processing framework, called BASF (Belle AnalysiS
Framwork)~\cite{daq1-fast-tracking}, takes users' reconstruction and
analysis codes as modules which are linked dynamically at the run
time. A module is written as an object of a class of C++. The class,
inherited from the module class of BASF which has virtual functions
for events, begins and ends run processing and other utility functions
such as initialization, termination and histogram definitions. Modules
written in Fortran and C can also be linked using wrapper functions.

The data transfer between modules is managed by PANTHER, an event and
I/O management package developed by the Belle collaboration. PANTHER
describes the logical structure and inter-relationships of the data
using an entity relationship model. In order to store data (structure)
in the event structure one writes a description file as an ASCII text
file. A PANTHER utility converts the description file into C and C++
header files and source code. The user will include the header files
in his/her code and the source code is compiled and linked into the
user module to have access to the data structure in the module.

The standard reconstruction modules for sub-detectors and global
reconstruction of four momenta, production vertices and likelihoods
for being specific species such as electrons, muons, pions, kaons,
protons and gammas of charged and neutral particles have been prepared
and used to produce physics results as well as detector performance
results described in this paper.

\subsection{Level 4 Offline Trigger Software}

The purpose of Level~4 software filtering is to reject backgrounds
just before full event reconstruction in the very beginning of the DST
production chain.  At the same time, high efficiencies for signal
events are also required. The signal events include not only
$B\bar{B}$ events but also other physics related events such as
$\gamma \gamma$ or $\tau$ events, and events necessary for detector
calibration etc. such as di-muon events.

Most of backgrounds are related to beam activities by both electrons
and positrons.  Fig. \ref{fig_dr_dz} shows $dr$ (top) and $dz$
(bottom) distributions obtained by a fast tracker, $fzisan$
\cite{cmp-fzisan}, which has been developed for the use in Level~4.
The open histograms are track-by-track basis distributions, and the
hatched distributions are for tracks closest to the coordinate origin
in each event.  It can be clearly seen that many tracks are created at
the beam pipe and that a large fraction of tracks are generated far
away from the origin in the $z$ direction caused by beam interactions
with residual gases in the beam pipe.  Therefore, the basic strategy
to reject backgrounds is to select events with tracks originating from
the interaction point, IP.


\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
\centerline{\psfig{file=cmp/cmp_dr_dz.eps,width=10cm,angle=0}}
\vspace{0mm}
	\caption{
			$dr$ (top) and $dz$ (bottom) distributions.
			The open histograms are for track-by-track basis and
			the hatched histograms	show $dr$ or $dz$ distributions 
closest to the origin
			in each event.
			}
\vspace{5mm}
	\label{fig_dr_dz}
  \end{center}
\end{figure}

There are low multiplicity (in terms of charged particles) events even
in $B\bar{B}$ decays such as $B^{0} \rightarrow K_{s} \pi^{0}$.  In
order to save these events, we keep events with large energy deposits
in the electromagnetic calorimeter, ECL, redundantly.

The Level~4 algorithm consists of 4 stages of event selections as follows:
\begin{itemize}
\item Selection by Level~1 trigger information:\\
	  Because of demands from the sub-detector groups, events with some
	  specific trigger bits are saved without any further
	  processing.\\
\item Energy measured by ECL:\\
	  We require the ECL energy reconstructed by  $fzisan$
	  to be greater than 4~GeV.
	  Concurrently a veto logic using Level~1 information is
	  applied, which is formed as a coincidence of ECL and KLM hits,
	  to suppress background events induced by cosmic rays.\\ 
\item Selection of events with a track coming from	IP:\\
	  By reconstructing charged tracks by $fzisan$, events with at least
	  one ``good track'' are selected, where a ``good track'' is defined
	  as a track with $p_{t}$ greater than 300~MeV/c, $\mid dr \mid$
	  less than 1.0~cm, and $\mid dz \mid$ less than 4.0~cm.
	  This corresponds to the application of cuts on the hatched distributions
	  in Fig.~\ref{fig_dr_dz}.\\
\item Level 4 monitoring:\\
For monitoring of the performance of Level~4, 1\% of
	  events which fail our selection criteria are saved and
	  passed to further full reconstruction.
\end{itemize}
In order to reduce CPU usage, events satisfying our requirement in
each stage are selected immediately and submitted to the full
reconstruction chain without further processing.

Table~\ref{table-eff} summarizes the Level~4 efficiency measured for
various real data samples.

\begin{table}[htbp]
  \begin{center}
	\vspace*{5mm}
	\caption{
			Fraction of events which satisfy the requirements of
			Level~4 in	various samples of real data.
			}
\vspace{5mm}
	\begin{tabular}{l|c}
\hline \hline
	  Event type	&Fraction passing Level~4\\
	  \hline
	  All triggered events	&26.7\%	\\
	  Hadronic events (loose selection)		&98.1\% \\
	  Hadronic events (tight selection)		&99.8\% \\
	  Muon pair events		&98.7\% \\
	  Tau pair events		&97.6\% \\
	  Two photon candidates	&92.9\%	\\
\hline \hline
	\end{tabular}
	\label{table-eff}
  \end{center}
\vspace{8mm}
\end{table}

It can be seen that the data are significantly reduced while keeping
high efficiencies for the signals.  The efficiency for the high purity
hadronic sample is very close to 100\%.  The efficiency for two
specific decay modes are also tested using MC data.  First is a
so-called gold plated mode, $B \rightarrow J/\psi K^0_s$ where the
$J/\psi$ and $K^0_s$ decay generically and the decay of the other $B$
is also generic.  Second is $B \rightarrow K_s \pi^0$ where again the
daughter particles and the other $B$ decay generically.  Out of 1000
generated events each, the efficiency of Level~4 is 100\% for both
decay modes.

For the remaining events, the purity is 76.5\% where the purity is
defined as the number of events used in either physics analysis or
calibration jobs divided by the number of events passing Level~4.
This implies that only 6\% more events can be suppressed even if we
achieve 100\% of purity ($26.7\% \times (1 - 0.765) = 6.3\% $).
Therefore, the reduction rate by Level~4 is close to the optimal
value.  The further drastic suppression requires changes in the
definition of signals, i.e. tighter cuts in the event classification.

Figure~\ref{fig:rundep} shows the run dependence of the fraction of
events which pass the Level~4 requirements.

\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
\centerline{\psfig{file=cmp/cmp_exp7.eps,width=10cm,angle=0}}
\vspace{3mm}
	\caption{
			Fraction of events which pass the Level~4 requirements as
			a function of run number (the full range is about 6
			months). 
			The macro structure is due to changes in the accelerator
			parameters.
			}
	\label{fig:rundep}
  \end{center}
\end{figure}

There are some steps and bunch structures.  Such structures are caused
by the changes in the accelerator parameters.  Except for the macro
structures, the Level~4 performance has been reasonably stable.

%\bibitem{ref:fzisan}
%K.~Hanagaki, M.~Hazumi, and H.~Kakuno,
%``The Level~4 Software Trigger at Belle'', Belle Note \#299.

\subsection{DST Production and Skimming}

The events accepted by the Level 4 software filter are reconstructed
and the information is stored as DST.  In this stage, raw data, the
contents of which are direct logs from data acquisition devices, are
converted into physics objects of 4-vectors of $x_{\mu}$ and
$p_{\mu}$.

The most downstream of the reconstruction flow is the event
classification and skimming. Here, all events are examined in such a
way that certain selection criteria are applied to select events of
our interest from a large number of background events. Once events are
satisfied with our cuts, they are recorded into not only DST but also
specific data files on the disk as skimmed data.  In this way, physics
events such as hadronic events and Bhabha candidates are stored. Based
upon those data, detector calibrations are carried out in detail and
the offline luminosity is computed.

After all information is reconstructed, monitoring modules are called
to keep our data quality reasonably high. Basic observables like the
number of charged tracks reconstructed are booked and those histograms
are checked by experts.

A typical processing speed of DST production, including skimming part,
is around 40 Hz, depending on beam conditions.  In our computing
machines, we can process about 80 $pb^{-1}$ of data a day when three
CPU servers out of seven are allocated.  Fig. \ref{cmp-process} shows
a history of our event processing rate together with the data
acquisition rate in the recent runs. Our offline processing has been
well tracked to the online trigger rate with normally one day delay.
The data size of DST is about 100 KB and 60 KB for hadronic and Bhabha
events, respectively.

The calibration constants, which are used to correct detector
responses, are kept in $postgresql$ (www.postgresql) database in our
system. During the period of an experiment, detector raw responses can
be shifted due to changes in environmental conditions. The energy loss
measurements by CDC, for example, suffer from atmospheric pressure
changes. After checking skimmed data created by DST production job,
the detector constants are improved on the weekly basis to correct for
this sort of effects. By getting updated constants, the re-processing
has been made for hadronic events.

\begin{figure}[hbt]
\vspace{0mm}
\begin{center}
\centerline{\psfig{file=cmp/cmp_process.eps,width=10cm,angle=0}}
\vspace{0mm}
	\caption{History of the event processing rate together with the data 
acquisition rate.
				   }
\vspace{5mm}
	\label{cmp-process}
  \end{center}
\end{figure}


In physics analyses, one does not need complete information available
in DST. Instead, easy access to experimental data is highly demanded
because we must play with a large amount of beam data at the
B-factory. For this purpose, minimal sets of DST (``mini-DST''), which
is compact but sufficient to study physics channels, are created for
all runs. The hadronic event size at this level is about 40 KB.
