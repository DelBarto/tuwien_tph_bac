\contentsline {section}{\numberline {1}Introduction}{8}
\contentsline {section}{\numberline {2}Interaction Region}{11}
\contentsline {subsection}{\numberline {2.1}Beam Crossing Angle}{11}
\contentsline {subsection}{\numberline {2.2}Beam-line Magnets near the Interaction Point}{12}
\contentsline {subsection}{\numberline {2.3}Beam Pipe}{12}
\contentsline {subsection}{\numberline {2.4}Beam Background}{14}
\contentsline {section}{\numberline {3}Extreme Forward Calorimeter, EFC}{16}
\contentsline {subsection}{\numberline {3.1}Design and Construction}{16}
\contentsline {subsubsection}{\numberline {3.1.1}BGO and radiation hardness}{16}
\contentsline {subsubsection}{\numberline {3.1.2}Mechanical structure}{18}
\contentsline {subsubsection}{\numberline {3.1.3}Light collection}{18}
\contentsline {subsection}{\numberline {3.2}Electronics}{19}
\contentsline {subsubsection}{\numberline {3.2.1}Front-end amplifier}{19}
\contentsline {subsubsection}{\numberline {3.2.2}Rear-end receiver and digitizer}{20}
\contentsline {subsection}{\numberline {3.3}Performance}{20}
\contentsline {section}{\numberline {4}Silicon Vertex Detector, SVD}{29}
\contentsline {subsection}{\numberline {4.1}General Characteristics}{29}
\contentsline {subsection}{\numberline {4.2}Double-sided Silicon Detector, DSSD}{30}
\contentsline {subsection}{\numberline {4.3}Mechanical Structure}{31}
\contentsline {subsubsection}{\numberline {4.3.1}Ladder}{31}
\contentsline {subsubsection}{\numberline {4.3.2}Support structure}{32}
\contentsline {subsection}{\numberline {4.4}Front-end Electronics}{33}
\contentsline {subsubsection}{\numberline {4.4.1}VA1 integrated circuit}{34}
\contentsline {subsubsection}{\numberline {4.4.2}Hybrid}{36}
\contentsline {subsubsection}{\numberline {4.4.3}Repeater system, CORE}{37}
\contentsline {subsection}{\numberline {4.5}Back-end Electronics}{38}
\contentsline {subsubsection}{\numberline {4.5.1}FADC system}{39}
\contentsline {subsubsection}{\numberline {4.5.2}Trigger Timing Modules}{40}
\contentsline {subsection}{\numberline {4.6}SVD DAQ System}{40}
\contentsline {subsection}{\numberline {4.7}Monitor System}{41}
\contentsline {subsection}{\numberline {4.8}Performance}{42}
\contentsline {section}{\numberline {5}Central Tracking Chamber, CDC}{45}
\contentsline {subsection}{\numberline {5.1}Design and Construction of the Central Tracking Chamber}{45}
\contentsline {subsubsection}{\numberline {5.1.1}Structure and wire configuration}{45}
\contentsline {subsubsection}{\numberline {5.1.2}Gas}{48}
\contentsline {subsubsection}{\numberline {5.1.3}Electronics}{49}
\contentsline {subsection}{\numberline {5.2}Beam Test by a Real-size Prototype Drift Chamber}{53}
\contentsline {subsection}{\numberline {5.3}Calibration with Cosmic Rays}{55}
\contentsline {subsubsection}{\numberline {5.3.1}CDC}{55}
\contentsline {subsubsection}{\numberline {5.3.2}Cathode strip detector}{57}
\contentsline {subsection}{\numberline {5.4}Performance}{61}
\contentsline {section}{\numberline {6}Aerogel \v {C}erenkov Counter System, ACC}{65}
\contentsline {subsection}{\numberline {6.1}Detector Design}{65}
\contentsline {subsubsection}{\numberline {6.1.1}Production of hydrophobic silica aerogels}{65}
\contentsline {subsubsection}{\numberline {6.1.2}Quality of the aerogels}{66}
\contentsline {subsubsection}{\numberline {6.1.3}Fine-mesh photomultiplier tubes}{68}
\contentsline {subsection}{\numberline {6.2}ACC Readout Electronics}{71}
\contentsline {subsubsection}{\numberline {6.2.1}Preamplifier Base Assemblies}{71}
\contentsline {subsubsection}{\numberline {6.2.2}Charge-to-Time Converter Modules}{72}
\contentsline {subsubsection}{\numberline {6.2.3}Monitoring system}{73}
\contentsline {subsubsection}{\numberline {6.2.4}Monte Carlo simulation}{74}
\contentsline {subsection}{\numberline {6.3}Performance of ACC}{74}
\contentsline {subsection}{\numberline {6.4}Particle Identification of Electrons and Charged Kaons, EID and KID}{77}
\contentsline {subsubsection}{\numberline {6.4.1}EID and fake rate}{77}
\contentsline {subsubsection}{\numberline {6.4.2}KID and pion fake rate}{77}
\contentsline {section}{\numberline {7}Time-of-Flight Counters, TOF}{81}
\contentsline {subsection}{\numberline {7.1}Design and Construction of TOF}{81}
\contentsline {subsubsection}{\numberline {7.1.1}FM-PMTs}{82}
\contentsline {subsubsection}{\numberline {7.1.2}Scintillators}{83}
\contentsline {subsubsection}{\numberline {7.1.3}Readout electronics}{84}
\contentsline {subsection}{\numberline {7.2}Beam Test}{88}
\contentsline {subsection}{\numberline {7.3}Performance}{91}
\contentsline {subsubsection}{\numberline {7.3.1}PMT gain adjustment}{91}
\contentsline {subsubsection}{\numberline {7.3.2}Calibration and time resolution using $\mu $-pair events}{91}
\contentsline {subsubsection}{\numberline {7.3.3}$\pi ^{\pm }/K^{\pm }$ separation}{93}
\contentsline {section}{\numberline {8}Electromagnetic Calorimetry, ECL}{97}
\contentsline {subsection}{\numberline {8.1}Design and Construction of ECL}{97}
\contentsline {subsubsection}{\numberline {8.1.1}CsI($Tl$) crystal}{98}
\contentsline {subsubsection}{\numberline {8.1.2}Mechanical assembly}{100}
\contentsline {subsubsection}{\numberline {8.1.3}Readout electronics}{101}
\contentsline {subsubsection}{\numberline {8.1.4}Calibration by cosmic rays}{103}
\contentsline {subsubsection}{\numberline {8.1.5}Radiation hardness}{104}
\contentsline {subsection}{\numberline {8.2}Beam Tests}{108}
\contentsline {subsubsection}{\numberline {8.2.1}$\pi $2 beam}{108}
\contentsline {subsubsection}{\numberline {8.2.2}Photon beams at the ROKK-1M facility}{110}
\contentsline {subsection}{\numberline {8.3}Performance}{122}
\contentsline {section}{\numberline {9}$K_L$ and Muon Detection System, KLM}{129}
\contentsline {subsection}{\numberline {9.1}Design and Construction}{129}
\contentsline {subsubsection}{\numberline {9.1.1}Glass Resistive Plate Counters}{129}
\contentsline {subsubsection}{\numberline {9.1.2}Barrel modules}{130}
\contentsline {subsubsection}{\numberline {9.1.3}End-cap modules}{133}
\contentsline {subsubsection}{\numberline {9.1.4}High voltage system}{133}
\contentsline {subsubsection}{\numberline {9.1.5}Gas mixing and distribution}{134}
\contentsline {subsection}{\numberline {9.2}Readout Electronics}{137}
\contentsline {subsection}{\numberline {9.3}Efficiency and resolution}{138}
\contentsline {subsection}{\numberline {9.4}Performance}{140}
\contentsline {subsubsection}{\numberline {9.4.1}$K_L$ detection}{141}
\contentsline {subsubsection}{\numberline {9.4.2}Muon detection}{141}
\contentsline {section}{\numberline {10}Detector Solenoid and Iron Structure}{144}
\contentsline {subsection}{\numberline {10.1}Solenoid Magnet}{144}
\contentsline {subsection}{\numberline {10.2}Iron Yoke}{144}
\contentsline {subsection}{\numberline {10.3}Magnetic Field Mapping of the Belle Solenoid}{144}
\contentsline {section}{\numberline {11}Trigger}{150}
\contentsline {subsection}{\numberline {11.1}CDC Trigger System}{152}
\contentsline {subsubsection}{\numberline {11.1.1}$r$-$\phi $ trigger}{152}
\contentsline {subsubsection}{\numberline {11.1.2}z trigger}{154}
\contentsline {subsection}{\numberline {11.2}TOF Trigger System}{159}
\contentsline {subsubsection}{\numberline {11.2.1}Timing logic}{160}
\contentsline {subsubsection}{\numberline {11.2.2}Delay and decision logic}{161}
\contentsline {subsection}{\numberline {11.3}ECL Trigger System}{163}
\contentsline {subsubsection}{\numberline {11.3.1}Trigger Cell}{163}
\contentsline {subsubsection}{\numberline {11.3.2}Total Energy Trigger}{163}
\contentsline {subsubsection}{\numberline {11.3.3}Cluster Counting Trigger}{165}
\contentsline {subsection}{\numberline {11.4}KLM Trigger}{167}
\contentsline {subsubsection}{\numberline {11.4.1}KLM signal out}{167}
\contentsline {subsubsection}{\numberline {11.4.2}KLM trigger scheme}{168}
\contentsline {subsubsection}{\numberline {11.4.3}Trigger efficiency}{170}
\contentsline {subsection}{\numberline {11.5}EFC Trigger System}{171}
\contentsline {subsection}{\numberline {11.6}GDL}{173}
\contentsline {subsection}{\numberline {11.7} Trigger Operation for Beam Run }{173}
\contentsline {section}{\numberline {12}Data Acquisition}{177}
\contentsline {subsection}{\numberline {12.1}Readout}{177}
\contentsline {subsubsection}{\numberline {12.1.1}FASTBUS TDC readout systems}{177}
\contentsline {subsubsection}{\numberline {12.1.2}SVD readout system}{179}
\contentsline {subsubsection}{\numberline {12.1.3}Readout sequence control}{180}
\contentsline {subsection}{\numberline {12.2}Event Builder}{181}
\contentsline {subsection}{\numberline {12.3}Online Computer Farm}{182}
\contentsline {subsection}{\numberline {12.4}Data Monitoring System}{183}
\contentsline {subsection}{\numberline {12.5}Mass Storage System}{184}
\contentsline {subsection}{\numberline {12.6}Run Control}{185}
\contentsline {subsection}{\numberline {12.7}Performance}{185}
\contentsline {section}{\numberline {13}Offline Computing System}{187}
\contentsline {subsection}{\numberline {13.1}Computing Power}{187}
\contentsline {subsection}{\numberline {13.2}Tape Libraries and Servers}{187}
\contentsline {subsection}{\numberline {13.3}PC Farms}{189}
\contentsline {subsection}{\numberline {13.4}Offline Software}{190}
\contentsline {subsection}{\numberline {13.5}Level 4 Offline Trigger Software}{191}
\contentsline {subsection}{\numberline {13.6}DST Production and Skimming}{193}
\vspace {6pt}
\contentsline {section}{Acknowledgements}{196}
\contentsline {section}{References}{196}
