\section{Central Tracking Chamber, CDC}

The efficient reconstruction of charged particle tracks and precise
determination of their momenta is an essential ingredient to virtually
all of the measurements planned for the Belle experiment.
Specifically, the physics goals of the experiment require a momentum
resolution of $\sigma_{p_t} /p_t \sim 0.5 \% \sqrt{1 + p^2_t}$ ($p_t$
in GeV/c) for all charged particles with $p_t \ge $ 100 MeV/c in the
polar angle region of $17^o \le \theta \le 150^o$.  In addition, the
charged particle tracking system is expected to provide important
information for the trigger system and particle identification
information in the form of precise $dE/dx$ measurements for charged
particles~\cite{TDR95}.

\subsection{Design and Construction of the Central Tracking Chamber}
The Belle central drift chamber, CDC, was designed and constructed to
meet the requirements for the central tracking
system~\cite{cdc-UnoA379,cdc-Hirano2000}.  Since the majority of the
decay particles of a $B$ meson have momenta lower than 1 GeV/c, the
minimization of multiple scattering is important for improving the
momentum resolution.  Therefore, the use of a low-Z gas is desirable,
while a good $dE/dx$ resolution must be retained.

\subsubsection{Structure and wire configuration}

The structure of CDC is shown in	
%%%%%%%%%%%%%(fig. 1 Hirano2000)
Fig. \ref{eps-cdc-overview}.  It is asymmetric in the $z$ direction in
order to provide an angular coverage of $17^o \le \theta \le 150^o$.
The longest wires are 2400 mm long.  The inner radius is extended down
to 103.5 mm without any walls in order to obtain good tracking
efficiency for low-$p_t$ tracks by minimizing the material
thickness. The outer radius is 874 mm.  The forward and backward
small-$r$ regions have conical shapes in order to clear the
accelerator components while maximizing the acceptance.\\

The chamber has 50 cylindrical layers, each containing between three
and six either axial or small-angle-stereo layers, and three cathode
strip layers.
%%%%%%%%%%%%%(Table 1 cdc-Hirano2000)
Table \ref{CDC-1} gives the detailed parameters of the wire
configuration. CDC has a total of 8400 drift cells. We chose three
layers each for the two innermost stereo super-layers and four layers
each for the three outer stereo super-layers in order to provide a
highly-efficient fast $z$-trigger combined with the cathode strips. We
determined the stereo angles in each stereo super-layer by maximizing
the $z$-measurement capability while keeping the gain variations along
the wire below 10 \%.  \\

%%%%%%%%%%%%% Figure 1 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_01.eps,width=8cm,angle=-90}}
\vspace{5mm}
\caption{Overview of the CDC structure.  The lengths in the figure are
in units of mm.}
\label{eps-cdc-overview}
\end{center}
\vspace{5mm}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Table 1 %%%%%%%%%%%%%%%%%
\begin{table}
\caption{ Configurations of the CDC sense wires and cathode strips.}
\begin{center}
\vspace{6mm}
\begin{tabular}{|c|c|c|c|c|}
\hline \hline
Superlayer &No. of &Signal channels&Radius&Stereo angle (mrad)\\
type and No.&layers&per layer&(mm)&and strip pitch (mm)\\
\hline 
Cathode&1&64 (z) $\times$ 8 ($\phi$)&83.0&(8.2)\\
Axial 1 & 2 &64& 88.0 $\sim$ 98.0& 0.\\
Cathode & 1 &80 (z) $\times$ 8 ($\phi$)&103.0 & (8.2)\\
Cathode & 1 &80 (z) $\times$ 8 ($\phi$)&103.5 & (8.2)\\
Axial 1 & 4 &64 &108.5 $\sim$ 159.5 & 0.\\
Stereo 2 & 3&80 &178.5 $\sim$ 209.5 & 71.46 $\sim$ 73.75\\
Axial 3 & 6 &96 &224.5 $\sim$ 304.0 & 0.\\
Stereo 4 & 3&128&322.5 $\sim$ 353.5 & -42.28 $\sim$ -45.80\\
Axial 5 & 5 &144&368.5 $\sim$ 431.5 & 0.\\
Stereo 6 & 4&160&450.5 $\sim$ 497.5 & 45.11 $\sim$ 49.36\\
Axial 7 & 5 &192&512.5 $\sim$ 575.5 & 0.\\
Stereo 8 & 4&208&594.5 $\sim$ 641.5 & -52.68 $\sim$ -57.01\\
Axial 9 & 5 &240&656.5 $\sim$ 719.5 & 0.\\
Stereo 10&4 &256&738.5 $\sim$ 785.5 & 62.10 $\sim$ 67.09\\
Axial 11 &5 &288&800.5 $\sim$ 863.0 & 0.\\
\hline \hline
\end{tabular}
\end{center}
\label{CDC-1}
\vspace{10mm}
\end{table}

The individual drift cells are nearly square and, except for the inner
three layers, have a maximum drift distance between 8 and 10 mm and a
radial thickness that ranges from 15.5 to 17 mm.  The drift cells in
the inner layers are smaller than the others and their signals are
read out by cathode strips on the cylinder walls. These cell
dimensions were optimized based on the results of beam test
measurements~\cite{cdc-UnoA330}.
%%%%%%%%%%%%%%(Fig. 2 cdc-Hirano2000)
Fig. \ref{eps-cdc-cell} shows the cell arrangement in which the
neighboring radial layers in a superlayer are staggered in $\phi$ by a
half cell to resolve left-right ambiguities.  The sense wires are gold
plated tungsten wires of 30 $\mu$m in diameter to maximize the drift
electric field. The field wires of unplated aluminum of 126 $\mu$m in
diameter are arranged to produce high electric fields up to the edge
of the cell and also to simplify the drift time-to-distance relation.
The aluminum field wires are used to reduce the material of the
chamber.  The electric field strength at the surface of the aluminum
field wires is always less than 20 kV/cm, a necessary condition for
avoiding radiation damage~\cite{cdc-KadykA300}.\\

%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_02.eps,width=8cm,angle=0}}
\vspace{5mm}
\caption{Cell structure of CDC.}
\label{eps-cdc-cell}
\end{center}
\vspace{5mm}
\end{figure}


Three $z$-coordinate measurements at the inner-most radii are provided
by cathode strips as shown in
%%%%%%%%%%%%%%%(Fig. 4 cdc-Akatsu2000) 
Fig. \ref{eps-cdc-cathode}~\cite{cdc-Akatsu2000}.  They were glued on
the inner cylinder surface of the chamber and on both sides of a 400
$\mu$m thick CFRP cylinder located between the second and third anode
layers.  In order to maintain the mechanical strength and accuracy,
aluminum guard rings were attached on the both ends with electrically
conducting glue.  The deviation from a perfect cylindrical shape is
less than 100 $\mu$m in radius.  The cathode strips are divided into
eight segments in the $\phi$ direction and have an 8.2 mm pitch in the
$z$ direction. The strip width is 7.4 mm.  The total number of cathode
channels is 1,792.  The deterioration of the momentum resolution due
to multiple scattering in the cathode materials is minimized.  \\

%%%%%%%%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_03.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{Cell structure and the cathode sector configuration.}
\label{eps-cdc-cathode}
\end{center}
\vspace{5mm}
\end{figure}


The total wire tension of 3.5 tons is supported by the aluminum
end-plates and the CFRP cylinder structures that extend between the
end-plates.  Each end plate consists of three parts; the cathode,
inner and main parts as indicated in
%%%%%%%%%%%%%%
Fig. \ref{eps-cdc-overview}. The cathode part of a 16 mm thick flat
plate corresponds to the three innermost anode layers and three
cathode layers. The conically-shaped inner part covers 11 anode layers
in the radial range from 103.5 to 294 mm.  The end-plate thickness in
the $z$ direction is 11 $\sim$ 18 mm for the backward side and 25
$\sim$ 31 mm for the forward side. The end-plate of the main part is
10 mm in thickness and has a curved profile to minimize distortions
caused by the wire tension. The three end-plate parts are connected to
each other by stainless steel bolts and gas sealed with silicone glue.
The 754.5 mm radial space between the 3rd and 50th anode layers
contains only gases and wires.\\

The feed-through holes were drilled by a custom designed drilling
machine at the KEK machine shop. The hole position accuracy is better
than 30 $\mu$m.

\subsubsection{Gas}

The use of a low-Z gas is important for minimizing multiple Coulomb
scattering contributions to the momentum resolution.  Since low-Z
gases have a smaller photo-electric cross section than argon-based
gases, they have the additional advantage of reduced background from
synchrotron radiation.  We have selected a 50\% helium-50\% ethane gas
mixture.  This mixture has a long radiation length (640 m), and a
drift velocity that saturates at 4 cm/$\mu$s at a relatively low
electric field~\cite{cdc-UnoA330,cdc-Nitoh1994}.  This is important
for operating square-cell drift chambers because of large field
non-uniformities inherent to their geometry.  The use of a saturated
gas makes calibrations simpler and helps to ensure reliable and stable
performance. Even though the gas mixture has a low Z, a good $dE/dx$
resolution is provided by the large ethane
component~\cite{cdc-EmiJPSJ1996}.\\

Several test chambers were constructed and beam tests were carried
out.
%%%%%%%%%%%%%%%(Fig. 1 UnoA330)
Figs. \ref{eps-cdc-gasgain}(a) and (b) show measured gas gain and
drift velocity data for a 50\% helium-50\% ethane gas
mixture~\cite{cdc-UnoA330}.

%%%%%%%%%%%%% Figure 4 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_04.eps,width=14cm,angle=0}}
\vspace{3mm}
\caption{(a) The measured gas gain and (b) drift velocity for a 50 \%
He and 50 \% C$_2$H$_6$ gas mixture.}
\label{eps-cdc-gasgain}
\end{center}
\vspace{5mm}
\end{figure}


\subsubsection{Electronics}

A schematic diagram of the readout electronics of CDC is shown in
%%%%%%%%%%%%%%(Fig. 1 cdc-FujitaA495)
Fig. \ref{eps-cdc-electronics}~\cite{cdc-FujitaA405}. Signals are
amplified by Radeka-type pre-amplifiers~\cite{cdc-Radeka}, and sent to
Shaper/Discriminator/QTC modules in the electronics hut via $\sim$ 30
m long twisted pair cables. This module receives, shapes, and
discriminates signals and performs a charge(Q)-to-time(T) conversion
(QTC).  The module internally generates a logic-level output, where
the leading edge $T_{LE}$ corresponds to the drift time ($t_d$) and
the width $T_W$ is proportional to the input pulse height ($T_W =
a\times Q + b$).  This technique is a rather simple extension of the
ordinary TDC/ADC readout scheme, but allows us to use only TDCs to
measure both the timing and charge of the signals. Since multi-hit
TDCs work in the common stop mode, one does not need a long delay that
analog signals usually require in an ADC readout with a gate produced
by a trigger signal.\\

%%%%%%%%%%%%% Figure 5 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/fig_5_qt_scheme.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{CDC readout electronics scheme. T$_{L.E.}$ and T$_W$ are the
leading edge and width of the pulse, respectively.}
\label{eps-cdc-electronics}
\end{center}
\vspace{3mm}
\end{figure}


A prototype Shaper/QTC board with 32 channels (VME9U) was fabricated
and tested using a test beam.
%%%%%%%%%%%%%%%%(Fig. 2 cdc-FujitaA405)
Fig. \ref{eps-cdc-shaperqtc} shows the schematic circuit of a single
channel.  The signals from a pre-amplifier are split into two paths of
circuit; one is an "analog circuit" and the other is a "digital
circuit".  The digital circuit provides self-gated signals to the QTC
chip, and trigger output signals.  It consists of an amplifier,
comparators, and a gate signal generation circuit.  In order to have
good timing resolution without increasing the hit rates for the gate
and trigger signals due to noise signals, we use a "double-threshold
method". The signal from a low-threshold comparator is delayed by 20
ns before forming a coincidence with a signal from a high-threshold
comparator so that the timing of the coincidence signal is determined
by the low-threshold comparator. Threshold voltages are supplied
externally from precisely regulated adjustable voltage power
supplies. We used a fast video-amplifier chip TL592B (Texas
Instruments, USA) for the amplifier and a MAX9687 (Maxim, USA) for the
comparator.  An MC10198 (Motorola, USA) mono-stable multi-vibrator
chip was used to produce an ECL-level gate pulse.  The gate width was
adjusted by the value of the resistors soldered on the board. The
width was chosen to be $\sim$ 850 ns in the beam test. We observed
that the jitter of the gate width was 0.5 ns (rms) and the width was
stable within $\pm$ 1 ns during the beam test.\\

%%%%%%%%%%%%% Figure 6 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_06.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{Schematic circuit of the Shaper/QTC board.}
\label{eps-cdc-shaperqtc}
\end{center}
\vspace{5mm}
\end{figure}


The analog circuit provides input signals to the QTC chip. It consists
of an amplifier, a shaping circuit, and a delay.  The shaping circuit
has two stages of pole-zero cancellation with time constants of 120
ns/30 ns and 56 ns/20 ns.  A passive LC delay line of 100 ns is used
to delay the analog signal to arrive after the leading edge of the
gate.  We used an LT1192 (Linear Technology, USA) chip for an
amplifier of the analog circuit because it provides a wider linear
region and better bandwidth than a TL592B.\\

We set the gains to be 10 and 30 for the amplifiers in the analog and
digital circuits, respectively.  These are adjusted by the resistor
values soldered on the board.  Since a QTC chip is operated in a
self-gate mode, the charge data are automatically pedestal suppressed
in contrast to the ordinary ADC readout.  In order to take pedestal
data, the board takes "pedestal pulses" from the front panel connector
which forces a trigger of the gate circuits of all the channels
without input signals.\\

For Q-to-T conversion, we used chips recently developed by LeCroy,
MQT300~\cite{cdc-LeCroy}. The MQT300 chip performs Q-to-T conversion
in three ranges.  The conversion gain for the low range is 0.01 pC/ns,
with a factor of 8 and 64 larger conversion gains for the middle and
high ranges.  The full range in the specification is $\sim$ 4 $\mu$s
above the pedestals, but the chip provided a good linearity within 1
\% deviation up to $\sim$ 7 $\mu$s above the pedestals. Each range can
be enabled or disabled by the jumper pins on the Shaper/QTC board.  In
the beam test, we enabled and recorded all three ranges for a detailed
check of the Shaper/QTC board performance.  The MQT300 chip produces
the output pulses as an exclusive OR of the output pulses of the
enabled ranges.  When the three ranges are enabled, two pulses are
produced.  Since the dynamic range provided by a single range of
MQT300 chips is large enough for the CDC readout application, we plan
to use them only with a single range (middle range) in the actual
Belle experiment.\\

A beam test was carried out at the $\pi$2 beam line of the KEK 12-GeV
PS. Data were taken both with the QTC/multi-hit TDC and the
conventional TDC/ADC readout scheme in order to compare the
performance of two readout schemes directly with the same conditions.
In the QTC/multi-hit TDC scheme case, the output signals from the
Shaper/QTC board are recorded by LeCroy 3377 CAMAC TDC modules.  In
the TDC/ADC scheme case the previous beam test arrangement was
used~\cite{cdc-EmiJPSJ1996}.  Spatial resolutions as a function of the
drift distance were measured with various beam conditions.  No
significant difference is seen between the two readout schemes and
they are consistent with the previous result. $dE/dx$ distributions
were also measured with the two readout schemes. Again no significant
difference is seen.  These results confirm that the new readout scheme
worked successfully and is applicable to the CDC readout.

\subsection{Beam Test by a Real-size Prototype Drift Chamber}

A real-size prototype of Belle CDC was constructed and used in a beam
test to study the gas-gain saturation effect in $dE/dx$ measurements.
The chamber is 235 cm long, and the inner and outer radii are 25 cm to
90 cm, respectively.  It has 41 layers of sense wires, 9 layers fewer
than the final design~\cite{cdc-EmiJPSJ1996}.  A beam test was carried
out at the $\pi$2 beam line. The chamber was placed in a null magnetic
field.  The drift time was measured by a LeCroy FASTBUS TDC 1876 and
the total charge was integrated by a LeCroy FASTBUS 1885F.

The measured spatial resolution was 120 $\sim$ 150 $\mu$m depending on
the layer and incident angles in the direction perpendicular to the
wires.\\

For the $dE/dx$ measurement we took the truncated mean in order to
minimize the contribution of the Landau tail in the $dE/dx$
distribution.  In the analysis of this section 80 \% truncated means
are used to measure $dE/dx$. The $dE/dx$ resolution was obtained to be
5.2 \% for 3.5 GeV/c pions at an incident angle of $45^o$.
%%%%%%%%%%%%%(Fig. 7 cdc-EmiA379)
Fig. \ref{eps-cdc-dedx} shows measured $dE/dx$ distributions as a
function of $\beta\gamma$.  The solid curve is a fit to the data at an
angle of $45^o$, based on the most-probable energy loss
formula~\cite{cdc-EmiJPSJ1996}.  The density correction term $\delta$
was parameterized to fit the electron data. The dashed curve
corresponds to the case of $\delta$ = 0.\\

%%%%%%%%%%%%% Figure 7 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{3mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=cdc/fig_7_dedx_curve.eps,width=11cm,angle=0}}
\vspace{3mm}
\caption{(a) Measured $dE/dx$ vs. $\beta \gamma$ and (b) the same as
(a),but normalized with the $dE/dx$ measured by 3.5 GeV/c protons
($\beta \gamma$ = 3.73).  The solid curve is a fit to the data at an
incident angle of 45$^o$, and the dashed curve is that with $\delta$ =
0.}
\label{eps-cdc-dedx}
\end{center}
\vspace{-3mm}
\end{figure}


The incident angle dependence of $dE/dx$ was measured. The normalized
$dE/dx$ distributions to $dE/dx$ values calculated from the fit
obtained for the incident angle of $45^o$ decrease with increasing
incident angles. This indicates a clear space charge effect.  The
gas-gain saturation effect is mainly determined by the projected ion
pair density produced by the incident particles.
%%%%%%%%%%%%%(Fig. 11 cdc-EmiA379)
Fig. \ref{eps-cdc-dedxnorm} shows distributions of
$(dE/dx)_{meas.}/(dE/dx)_{expect}$ vs. $(dE/dx)_{meas.}/\cos \theta$
for various beam conditions.  The $x$-axis corresponds to the
projected ion pair density and the magnitude of the $y$-axis is
essentially independent of incident particles.\\

%%%%%%%%%%%%% Figure 8 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{1mm}
\begin{center}
%\epsfysize=10cm
\centerline{\psfig{file=cdc/fig_7_dedx_saturation.eps,width=10cm,angle=0}}
\vspace{3mm}
\caption{(a) $(dE/dx)_{meas}/(dE/dx)_{expect}$
vs. $(dE/dx)_{meas}/cos\theta$ (avalanche density on the sense wire)
for 0.8 GeV/c protons, 0.8 GeV/c pions, 0.6 GeV/c electrons and 3.5
GeV/c pions, and (b) $(dE/dx)_{meas}/(dE/dx)_{expect}$
vs. $(dE/dx)_{meas}/cos\theta$ for 0.6, 0.7 and 1.0 GeV/c protons and
2.0 GeV/c electrons. The solid curves are the fit results.  The 90$^o$
data are treated separately.}
\label{eps-cdc-dedxnorm}
\end{center}
\vspace{3mm}
\end{figure}

It is important to correct data for the space charge effect to
maintain the particle identification performance expected from the
obtained resolution and $\beta\gamma$ dependence of $dE/dx$.\\

\subsection{Calibration with Cosmic Rays}

\subsubsection{CDC}

Cosmic ray data were taken with the solenoid operating at its nominal
1.5 T field. A positive high voltage was applied to the sense wires,
and the field wires were connected to the end-plates and kept at
ground.  The sense wire high voltages, typically 2.35 kV, were
adjusted layer-by-layer to keep the same gas gain (several $\times
10^4$) for different cell sizes.  The chamber was operated at a
constant pressure slightly above one atmosphere.\\

The standard trigger and data acquisition system developed for
operation in the Belle experiment was used in the cosmic ray run.
Back-to-back cosmic ray events were triggered by the Belle TOF counter
system~\cite{Belle-TOF}.
% This trigger system is cogged with a 16 ns cycle phased with
% the KEKB beam crossing frequency of 509 MHz.
The event timing was determined with a 0.7 ns resolution using TOF
information.  The acceptance for cosmic ray tracks was determined by
TOF which has a smaller polar angle acceptance than CDC.\\

We accumulated a sample of $5 \times 10^7$ cosmic ray tracks. After
requiring tracks to go through the interaction region ($-2.0$ cm $\le
dz \le 3.0$ cm and $| dr | \le 2$ cm), 56,743 events remain, where
$dr$ is the closest distance in the $r$-$\phi$ plane and $dz$ is the
closest distance in the $z$ coordinate.\\

Extensive works on the optimization of the drift-time versus distance
function were carried out by minimizing residual distributions for the
function iteratively. Separate functions were used for each layer and
each incident angle of the track with respect to the radial direction.
A correction for the signal propagation time along the wire was
applied after the first 3-D track reconstruction. Corrections were
also made for the effects of wire sag and distortions of the
end-plates caused by the wire tension by using the measured data.  The
relative alignment of the cathode and inner end-plates with respect to
the main end-plates was determined using cosmic ray data.  The
rotations and translations thus acquired were found to be less than
200 $\mu$m, consistent with the estimated construction accuracy.\\

%%%%%%%%%%%%(Fig. 6 cdc-Hirano2000)
Figure \ref{eps-cdc-field} shows the $z$ dependence of $B_z$, the
axial component of the magnetic field, measured before the
installation of CDC.  The non-uniformity of the magnetic field is as
large as 4 \% along the central axis. The Kalman filtering
method~\cite{Kalman} was used to correct the effects due to the
non-uniformity of the measured magnetic field.
%%%%%%%%%%%%(Fig. 8 cdc-Hirano2000)	
Fig. \ref{eps-cdc-resolution} shows the spatial resolution as a
function of the drift distance.  Near the sense wire and near the cell
boundary the spatial resolution is significantly poorer.  The spatial
resolution for tracks passing near the middle of the drift space is
approximately 100 $\mu$m.\\

%%%%%%%%%%%%% Figure 9 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_09.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Measured axial component of the magnetic field produced by
the Belle solenoid.  The nominal value of the magnetic field inside
CDC is 1.5 T.  The difference between the minimum and maximum values
along the central axis is about 4 \%.}
\label{eps-cdc-field}
\end{center}
\vspace{5mm}
\end{figure}


%%%%%%%%%%%%% Figure 10 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_10.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Spatial resolution as a function of the drift distance.}
\label{eps-cdc-resolution}
\end{center}
\vspace{5mm}
\end{figure}


The $p_t$ resolution as a function of $p_t$ is shown in 
%%%%%%%%%%%%%(Fig. 10 cdc-Hirano2000)
Fig. \ref{eps-cdc-ptresolution}.  The solid curve indicates the result
fitted to the data points, i.e. $(0.201 \pm 0.003) ~p_t \oplus (0.290
\pm 0.006)/\beta)$ in \% ($p_t$ in GeV/c). The dashed curve shows the
ideal expectation for $\beta = 1$ particles, i.e. $(0.188 ~p_t \oplus
0.195)$ in \%. No apparent systematic effects due to the particle
charge were observed.

%%%%%%%%%%%%% Figure 11 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_11.eps,width=12cm,angle=0}}
\caption{$p_t$ dependence of $p_t$ resolution for cosmic rays. The
solid curve shows the fitted result (0.201 \% $p_t \oplus$ 0.290
\%/$\beta$) and the dotted curve (0.118 \% $p_t \oplus $ 0.195 \%)
shows the ideal expectation for $\beta$ = 1 particles .}
\label{eps-cdc-ptresolution}
\end{center}
\vspace{5mm}
\end{figure}

\subsubsection{Cathode strip detector}

The performance test was carried out using cosmic rays before the CDC
was installed into the Belle detector.  Data were taken with a simple
trigger system with scintillation counters located in the inner
cylinder to select cosmic rays that passed through the beam pipe area
inside CDC.  Without any special optimization, the anode wire data
provide an extrapolation resolution of 160 $\mu$m in the $r$-$\phi$
plane and 3.6 mm in the $z$ direction to the CDC cathode part.
Anode-wire tracks were used to estimate the detection efficiency of
the cathode readout.\\

The charge distribution on the cathode strips depends on the
anode-cathode gap and the spread of the avalanche at the anode.  When
an incoming particle is inclined with respect to the normal incidence,
the electron avalanche is spread along the anode wire, making the
induced charge distribution broader.  This effect was made minimal
owing to the gate expansion and the limitation of the charge
integration time to 150 ns.\\

Consecutive cathode hits are recognized as a "cluster".  At least
three strips are required to form a cluster. If two local charge
maxima are separated more than three strips, they are regarded as
separate clusters.  The matching of a track to a cathode-hit cluster
is done by requiring that the extrapolated $\phi$ is within the
$\phi_{min}$ and $\phi_{max}$ region covered by the cathode strip hits
and that the extrapolated $z$ is within the $z$ region covered by the
strip hits.
%%%%%%%%%%%%%%(Fig. 13 cdc-Akatsu2000)	
Fig. \ref{eps-cdc-cathodeeff} shows the cathode efficiency as a
function of incident angle for $V_{th}$ = 50 mV and an anode high
voltage at 2.3 kV. The cathode strip efficiency is defined as the
ratio of the number of matched tracks to the number of sampled tracks.
As the Belle tracking trigger requires at least two hits in the three
cathode layers for each track, the trigger efficiency by the cathode
strips is better than 99\%, even for the worst case of the incident
angle of $90^o$ where the cathode charge is smallest due to gas gain
saturation.\\

The spatial resolution of the cathode readout was obtained with the
self-tracking method in the $z$ direction using cathode data alone.
%%%%%%%%%%%%%%(Fig. 16 cdc-Akatsu2000)
Fig. \ref{eps-cdc-cathoderesol} shows measured results of the
intrinsic cathode resolution as a function of incident angle.  The
data for the three layers are consistent. The data measured in a beam
test using a prototype detector is also shown~\cite{TDR95}.

%%%%%%%%%%%%% Figure 12 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_12.eps,width=10cm,angle=0}}
\caption{Angular dependence of the cathode detection efficiency.}
\label{eps-cdc-cathodeeff}
\end{center}
\end{figure}
%
%%%%%%%%%%%%% Figure 13 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_13.eps,width=10cm,angle=0}}
\caption{Angular dependence of the cathode resolution.}
\label{eps-cdc-cathoderesol}
\end{center}
\end{figure}
\clearpage

To evaluate the improvement in the three dimensional tracking provided
by the cathode data, each cosmic ray track that passed through the
entire CDC was treated as two separate tracks ({\it up} and {\it
down}) going outwards from the beam pipe area.  The tracking
performance was evaluated by checking the mismatch of these two track
segments, $\Delta z = z_{up} - z_{down}$, at the closest approach to
the beam axis.
%%%%%%%%%%%%%%(Fig. 17 cdc-Akatsu2000) 
Figs. \ref{eps-cdc-deltaz}~(a) and (b) show $\Delta z$ distributions
respectively (a) without and (b) with including the cathode cluster
information in three dimensional tracking with the axial and stereo
anode wire information.  The $\Delta z$ resolution was improved from
about 3 mm to 0.64 mm with the cathode data. We note that $\Delta$z
obtained in the present analysis is equivalent to $\sqrt{2}$ times of
the track extrapolation resolution of CDC to the interaction point.

%%%%%%%%%%%%% Figure 14 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace*{5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_14.eps,width=14cm,angle=0}}
\caption{The effect of the cathode readout information on $\Delta z =
z_{up} - z_{down}$ for cosmic ray tracks: (a) without and (b) with the
use of the cathode information in tracking, respectively.}
\label{eps-cdc-deltaz}
\end{center}
\end{figure}

\subsection{Performance}

The Belle experiment has been taking data at the $\Upsilon$(4S)
resonance energy since June 1999.  In order to operate CDC in the high
beam-background environment, we readjusted high voltages and
electronics parameters such as the bias voltage of the preamplifiers.
These changes reduce cross-talk noise hits caused by the very large
signals due to background particles spiraling around sense wires.
These changes also reduced the preamplifier gains and slowed their
response times.  The calibration constants were recalculated using the
beam data. In addition, corrections for the additional magnetic field
non-uniformity caused by the accelerator magnet components near the
interaction region were introduced into the Kalman filter using a
magnetic field map data measured with these magnets in place. We again
obtained 130 $\mu$m overall spatial resolution, although the residual
distribution has considerable non-Gaussian tails.

We analysed $e^+e^- \rightarrow \mu^+\mu^-$ events, detected as two
charged tracks by CDC and identified as muons by the outer detectors,
to extract the $p_t$ resolution of CDC.  We can calculate the $p_t$
resolution using the fact that each muon track has the same momentum
in the cm system.  The $p_t$ resolution measured is 1.64 $\pm$ 0.04 \%
in the $p_t$ range from 4 to 5.2 GeV/c, which is imposed by kinematics
and the acceptance of the TOF system used for triggering.  This result
is somewhat worse than the resolution of 1.38 \% expected from Monte
Carlo simulations.

The $\phi$ dependence of the $\Delta p_t$ resolution has been checked
with $e^+e^- \rightarrow \mu^+\mu^-$ events. No significant
$\phi$-dependent systematic effects were observed.

The $K_S^0$ mass was reconstructed from $K_S^0 \rightarrow \pi^+\pi^-$
decays in hadronic events in order to check the $p_t$ resolution at
low momenta. Most of the decay pions have momenta below 1 GeV/c as
shown in
%%%%%%%%%%%%%%%(Fig. 14 cdc-Hirano2000)
Fig. \ref{eps-cdc-ptdist}.
%%%%%%%%%%%%%%%(Fig. 15 cdc-Hirano2000)
Fig. \ref{eps-cdc-kmass} shows a $\pi^+\pi^-$ invariant mass
distribution.  The FWHM of the distribution is 7.7 MeV/$c^2$, which is
slightly worse than the idealized Monte Carlo prediction of 6.9
MeV/$c^2$.

%%%%%%%%%%%%% Figure 15 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_15.eps,width=10cm,angle=0}}
\caption{Transverse momentum distributions for pions from $K^0_S$
decays.  The solid and dashed histograms correspond to $\pi^-$ and
$\pi^+$ tracks, respectively.}
\label{eps-cdc-ptdist}
\end{center}
\end{figure}

%%%%%%%%%%%%% Figure 16 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_16.eps,width=10cm,angle=0}}
\caption{Inclusive $K^0_S \rightarrow \pi^+\pi^-$ mass distribution
for multi hadronic events.}
\label{eps-cdc-kmass}
\end{center}
\end{figure}

The truncated-mean method was employed to estimate the most probable
energy loss.  The largest 20 \% of measured $dE/dx$ values for each
track were discarded and the remaining data were averaged in order to
minimize occasional large fluctuations in the Landau tail of the
$dE/dx$ distribution.  As described in Section of the beam
test~\cite{cdc-EmiJPSJ1996}, the $<dE/dx>$ thus obtained is expected
to give a resolution of 5 \%.  A scatter plot of measured $<dE/dx>$
and particle momentum is shown in
%%%%%%%%%%%% Fig. 2 cdcBN321 %%%%%%%%%%%%%%
Fig. \ref{cdc-dE/dxscatter}, together with the expected mean energy
losses for different particle species. Populations of pions, kaons,
protons, and electrons can be clearly seen.  The normalized $<dE/dx>$
distribution for minimum ionizing pions from $K^0_S$ decays is shown
in
%%%%%%%%%%%% Fig. 3 cdcBN321 %%%%%%%%%%%%%%
Fig. \ref{cdc-normalizeddEdx}. The $<dE/dx>$ resolution was measured to be
7.8 \% in the momentum range from 0.4 to 0.6 GeV/c, while the
resolution for Bhabha and $\mu$-pair events was about 6 \%. These
results are somewhat worse than the Monte Carlo results.

At present the resolutions obtained from the beam data seem to be
slightly worse than those expected from the cosmic ray data and Monte
Carlo simulations. Although the present performance level of CDC
provides the momentum resolution sufficient for the goals of the Belle
experiment, efforts to further improve CDC performance are being made.

%%%%%%%%%%%%% Figure 17 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/cdcfig_dEdx.eps,width=10cm,angle=0}}
\caption{Truncated mean of $dE/dx$ versus momentum observed in
collision data.}
\label{cdc-dE/dxscatter}
\end{center}
\end{figure}

%%%%%%%%%%%%% Figure 18 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace*{-5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=cdc/fig_dedx_pi_ks.eps,width=10cm,angle=0}}
\vspace*{-5mm}
\caption{Distribution of $<dE/dx>/<dE/dx>_{exp}$ for pions from
$K^0_S$ decays.}
\label{cdc-normalizeddEdx}
\end{center}
\end{figure}
