\section{$K_L$ and Muon Detection System, KLM}

The KLM detection system was designed to identify $K_L$'s and muons
with high efficiency over a broad momentum range greater than 600
MeV/c.  The barrel-shaped region around the interaction point covers
an angular range from $45^o$ to $125^o$ in the polar angle and the
end-caps in the forward and backward directions extend this range to
$20^o$ and $155^o$~\cite{TDR95,klm-NIM}.\\

\subsection{Design and Construction}

%%%%%%%%%%%%%%%(NIM Fig. 1) %%%%%%%%%%
Figure \ref{int-sideview} shows the side view of the Belle detector with
the indicated location of the KLM detection system.
KLM consists of alternating layers of charged particle detectors and 4.7
cm-thick iron plates.  There are 15 detector layers and 14 iron layers
in the octagonal barrel region and 14 detector layers in each of the
forward and backward end-caps.  The iron plates provide a total of 3.9
interaction lengths of material for a particle traveling normal to
the detector planes. In addition, the electromagnetic calorimeter,
ECL, provides another 0.8 interaction length of material to convert
$K_L$'s. $K_L$ that interacts in the iron or ECL produces a shower of
ionizing particles.  The location of this shower determines the
direction of $K_L$, but fluctuations in the size of the shower do not
allow a useful measurement of the $K_L$ energy. The multiple layers of
charged particle detectors and iron allow the discrimination between
muons and charged hadrons ($\pi^{\pm}$ or $K^{\pm}$) based upon their
range and transverse scattering.  Muons travel much farther with
smaller deflections on average than strongly interacting hadrons.\\

\subsubsection{Glass Resistive Plate Counters}

The detection of charged particles is provided by glass-electrode
resistive plate counters (RPCs)
\cite{klm-Cardarelli,klm-Pestov,klm-Santonico,klm-Romano,klm-Gorini}. 
Glass RPCs have a long history dating back to the early
1970's~\cite{klm-Parkhomchuck}, but this is the first experiment in
which large area glass detectors operated at atmospheric pressure have
been used.  Resistive plate counters have two parallel plate
electrodes with high bulk resistivity ($\ge$ $10^{10}$$\Omega$ cm)
separated by a gas-filled gap.  In the streamer mode, an ionizing
particle traversing the gap initiates a streamer in the gas that
results in a local discharge of the plates.  This discharge is limited
by the high resistivity of the plates and the quenching
characteristics of the gas. The discharge induces a signal on external
pickup strips, which can be used to record the location and the time
of the ionization.

\subsubsection{Barrel modules}
There are minor differences between the barrel and the end-cap modules.
The barrel resistive plate counters which were constructed in the
United States consist of two parallel sheets of 2.4 mm-thick
commercially available float glass, the content of which is 73 \%
silicon dioxide, 14 \% sodium oxide, 9 \% calcium oxide, and 4 \%
trace elements.  The bulk resistivity of the glass is $10^{12}$ -
$10^{13}$ $\Omega$~cm at room temperature.  The plates are separated
by 1.9 mm thick extruded noryl spacers epoxied to both plates using 3M
2216 epoxi.
%%%%%%%%%%%%%%(NIM Fig. 2)
Fig. \ref{eps-klm-spacer} shows a barrel RPC with the spacers placed
every 10 cm so that they channel the gas flow through the RPC to
provide uniform gas composition throughout the active volume. A
T-shaped noryl spacer was epoxied around the perimeter forming a gas
tight unit. The spacers have the cross sections shown in
%%%%%%%%%%%%%%(NIM Fig. 3)
Fig. \ref{eps-klm-spaceredge}.  They were designed with concave
regions for the epoxy joints and were extruded to an accuracy of $\pm$
0.05 mm.  Tilting table tops were used to lift the RCPs into the
vertical orientation to avoid flexing the epoxy joints.  After
assembly, the RCPs were always moved in the vertical orientation or
supported by a rigid flat surface.  The barrel RPCs are rectangular in
shape and vary in size 2.2 x 1.5 m$^2$ to 2.2 x 2.7 m$^2$.\\

%%%%%%%%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-02.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Schematic diagram of the internal spacer arrangement for barrel RPC.}
\label{eps-klm-spacer}
\end{center}
\vspace{8mm}
\end{figure}

%%%%%%%%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-03.eps,width=10cm,angle=0}}
\vspace{5mm}
\caption{Cross section of the internal spacer and edge in KLM.}
\label{eps-klm-spaceredge}
\end{center}
\vspace{8mm}
\end{figure}

To distribute the high voltage on the glass, the outer surface was
coated with Koh-i-noor 3080F india ink. The ink was mixed 30 \% black
and 70 \% white by weight to achieve a surface resistivity of $10^6$ -
$10^7$$\Omega$/square.  This resistivity is chosen so that this
surface does not shield the discharge signal from the external pickup
pads but is small compared to the resistivity of the glass to provide
a uniform potential across the entire surface.\\
 
%%%%%%%%%%%%%%%(NIM Fig. 4)
Figure \ref{eps-klm-superlayer} shows the cross section of a
super-layer, in which two RPCs are sandwiched between the orthogonal
$\theta$ and $\phi$ pickup-strips with the ground planes for signal
reference and proper impedance.  This unit structure of two RPCs and
two readout-planes is enclosed in an aluminum box and is less than 3.7
cm thick.  Each RPC is electrically insulated with a double layer of
0.125 mm thick mylar.  Signals from both RPCs are picked up by copper
strips above and below the pair of RPCs, providing a three-dimensional
space point for particle tracking. Multiple scattering of particles as
they travel through the iron is typically a few centimeters.  This
sets the scale for the desired spatial resolution of KLM.  The pickup
strips in the barrel vary in width from layer to layer but are
approximately 50 mm wide with lengths from 1.5 to 2.7 m.  The geometry
of the pickup strips was chosen so that the pickup strip behaves as a
transmission line with a characteristic impedance of $\sim$ 50
$\Omega$ to minimize signal reflections at the junction with the
twisted-pair readout cable.  The barrel modules have a 100 $\Omega$
resistor connecting the pickup strip to ground at the cable end of the
pickup strip to create an effective impedance of 50 $\Omega$ at that
point.  This reduces the size of the signal which reaches the readout
boards for the barrel modules by a factor of two.\\

%%%%%%%%%%%%% Figure 4 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-04.eps,width=8cm,angle=0}}
\vspace{5mm}
\caption{Cross section of a KLM super-layer.}
\label{eps-klm-superlayer}
\end{center}
\vspace{8mm}
\end{figure}

The double-gap design provides redundancy and results in high
super-layer efficiency of $\ge$ 98 \%, despite the relatively low
single-layer RPC efficiency of 90 \% to 95 \%.  In particular, the
effects of dead regions near the spacers are minimized by offsetting
their locations for the two RPCs that comprise a super-layer. To
provide overall operational redundancy, care is taken to supply gas
and HV independently for each RPC layer so that the super-layer can
continue to operate even if a problem develops with one RPC.\\

Each barrel module has two rectangular RPCs with 48 $z$ pickup strips
perpendicular to the beam direction. The smaller 7 super-layers closest
to the interaction point have 36 $\phi$ strips and the outer 8
super-layers have 48 $\phi$ strips orthogonal to the $z$ strips.  The
backward region of the upper octant has modules that are 63 cm shorter
than the modules in the other octants in order to accommodate plumbing
for the cooling of the superconducting solenoid.  This chimney region
can be seen in Fig.
%%%%%%%%%%%%(NIM Fig. 1)
\ref{int-sideview}. 
%\ref{eps-klm-belle}.
This amounts to less than 2 \% of the solid angle of the barrel
coverage and has a minimal effect on the acceptance since it is in the
backward hemisphere.\\

The glass RPCs are relatively robust except for overpressure
situations which can push the two sheets of glass apart, breaking the
glass-spacer epoxy joint.  To avoid this hazard, the gas volume was
not sealed during shipping.  Relief bubblers protect the RPCs during
operation.  The RPCs were checked for gas leaks prior to installation.
The sensitivity of our measurement was about 0.05 cc/min and this was
the leak rate limit we set for all installed RPCs.

\subsubsection{End-cap modules} 

The glass used in the end-cap RPCs is 2.0 mm thick and has a chemical
content of $SiO_2$ 70-74 \%, $CaO$ 6-12 \%, $Na_2O$ 12-16 \%,
$Al_2O_3$ 0-2 \%, and $MgO$ 0-4 \%. The epoxy used to attach the
spacers and to seal the gas volume was 3M DP460.  The high voltage
distribution on the glass was accomplished by applying a conducting
carbon tape SHINTRON STR-9140 with a surface resistivity of $10^7$ -
$10^8$ $\Omega$/square to the outer surface of the glass.\\

Each super-layer module contains 10 $\pi$-shaped RPCs as shown in 
%%%%%%%%%%%%%%%(NIM Fig. 5)
Fig. \ref{eps-klm-endspacer} .  The $\theta$ strips are 36 mm wide and
vary in length from 2 m to 5 m.  The $\phi$ strips are 1.83 m long and
vary in width from 19 mm to 47 mm.
%%%%%%%%%%%%%%%(NIM Fig. 6)
Fig. \ref{eps-klm-endcut} shows an end-cap super-layer module cutaway
view with the 96 $\phi$ and 46 $\theta$ pickup-strips in each module.

%%%%%%%%%%%%% Figure 5 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-05.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{Schematic diagram of the internal spacer arrangement of the end-cap RPC.}
\label{eps-klm-endspacer}
\end{center}
\vspace{8mm}
\end{figure}

%%%%%%%%%%%%% Figure 6 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-06.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{Cut-away view of an end-cap super-layer module of KLM.}
\label{eps-klm-endcut}
\end{center}
\vspace{0mm}
\end{figure}

\subsubsection{High voltage system}

During data taking, the modules are operated typically with a total
gap voltage of 8 kV.  We chose to separately apply positive voltage to
the anodes and negative voltage to the cathodes.  This approach
minimizes external discharges through and around insulators.
Moreover, it helps reduce the overall HV system cost.\\

The LeCroy VISyN high voltage system is used which consists of Model
1458 mainframes and plug-in modules, Model 1468P for the anodes and
Model 1469N for the cathodes.  The cathodes are set at - 3.5 kV, and
the anodes are set at + 4.7 kV for the barrel RPCs and + 4.5 kV for
the end-cap RPCs.  To reduce the system cost, the anode planes are
ganged together and controlled by one positive high voltage channel.
In the barrel, eight anode planes are ganged together, while in the
end-caps, five anode planes are ganged together. The total current
drawn by the RPCs during operation is approximately 5 mA or $\sim$ 1
$\mu$A/m$^2$ of the RPC area. For properly operating chambers, most of
this current flows through the noryl spacers.

\subsubsection{Gas mixing and distribution}

We have investigated gas mixtures in search of an environmentally
friendly and non-combustible mixture that provides high detection
efficiency and stable RPC operation~\cite{klm-Yamaguchi}.  We compared
16 different mixtures with butane concentrations of 4, 8, 12, and 25
\% and argon concentrations of 20, 25, 30, and 35 \% with the balance
of the gas being HFC-134a. The RPC performance in terms of efficiency,
dark current, singles rate, and timing resolution was compared at an
operating point 200 V/mm above the knee of the efficiency plateau
curve.  The butane reduces the presence of after-pulses by absorbing
photons from the initial discharge. The non-flammable limit for the
butane is about 12 \% at the mixing ratio of 1:1 for the argon and
HFC-134a.  We found very little difference between flammable and
non-flammable mixtures and have chosen a non-combustible mixture of 62
\% HFC-134a, 30 \% argon, and 8 \% butane-silver.
%%%%%%%%%%%%%%%%(NIM Table 1)
Table \ref{klm-gas} lists some basic physical parameters of these
gases.  Butane-silver is a mixture of approximately 70 \% n-butane and
30 \% iso-butane. The cost of butane-silver is one tenth of the cost
of 99.5 \% pure iso-butane.\\
%%%%%%%%%%%%%%% Table 1 %%%%%%%%%%%%%%
\begin{table}
\caption{Physical parameters of the gases used in KLM.}
\vspace{3mm}
\begin{center}
\vspace{3mm}
\begin{tabular}{c c c c}
\hline \hline
Gas & Symbol & Mol. weight & Density (g/$l$) \\
\hline
Argon & Ar & 39.95 & 1.784 (0 $^o$C, 1atm)\\
Butane-silver & C$_4$H$_{10}$ & 58.12 & 2.6 (0 $^o$C, 1atm)\\
HFC-134a & CH$_2$FCF$_3$ & 102.0 & 4.5\\
\hline \hline
\end{tabular}
\end{center}
\label{klm-gas}
\vspace{10mm}
\end{table}

Two separate banks of bottles are arranged for each type of gas.  When
one side becomes empty, the supply line automatically switches to the
other.  Tank quantities are measured by weight for butane and HFC-134a
and by pressure for argon.  A diagram of the mixing system is shown in
%%%%%%%%%%%%%%(NIM Fig. 10)
Fig. \ref{eps-klm-gasmixing} . The three gases are sent to MKS model
1179A mass flow controllers for mixing in the appropriate ratios.
Four gas mixing systems are used separately for the inner RPCs in the
barrel super-layers, the barrel outer RPCs, the end-cap inner RPCs, and
the end-cap outer RPCs.  The flow rates from the mass flow controllers
are monitored via a network connection and the high voltage is
automatically lowered if a deviation from the desired flow rate is
detected. During normal operation, we flow a total of 4.5 $l$/min,
which corresponds to approximately one volume change per day.\\

%%%%%%%%%%%%% Figure 7 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-07.eps,width=12cm,angle=0}}
\vspace{5mm}
\caption{KLM gas mixing system.}
\label{eps-klm-gasmixing}
\end{center}
\vspace{0mm}
\end{figure}

The gas distribution system is designed to provide an independent gas
supply to each RPC in a super-layer.  Therefore, if one supply line
fails for any reason, the other RPC in the same super-layer will still
be operational.  To ensure uniform distribution of the flow without
the need for tedious adjustments, a "flow resistor" was inserted in
series upstream of each RPC.  These devices are 10 cm-long
stainless-steel tubes with an inner diameter of 254 $\mu$m. The flow
impedance of the tubes is about ten times larger than that of an RPC
layer.  Thus the flow rate is determined by the flow resistor (uniform
to about 15 \%) and almost independent of variations in the flow
resistance of individual RPCs.\\

The exhaust system has an active control of the exhaust pressure and
relief bubblers at various points in the system to prevent any
overpressure situations.  Tests indicated that epoxy joints between
the glass plates and the internal spacers begin to detach when a
barrel RPC is pressurized above 50 mmAq. For safety reasons, the
exhaust gas is dumped outside the experimental hall through a 20 m
vertical exhaust line.  Due to the large density of the mixed gas a
suction pump system is used.

\subsection{Readout Electronics}

Readout of 38 k pickup strips is accomplished with the use of
custom-made VME-based discriminator/time-multiplexing boards developed
by Princeton University and Osaka City University groups. The system
consists of the signal discrimination and multiplexing boards, crate
controller boards (one per crate) for crate-wide control and data
processing, string controller boards for downloading and controlling
multiple readout crates, and FASTBUS time-to-digital converters.\\

The discriminator boards are 6U-size VME boards with 96 input channels
per board.  A comparator (MAX908CPD) is used to generate a logic
signal if the voltage on the input channel exceeds the threshold
voltage.  This threshold can be selected via a programmable
digital-to-analog converter to be any value from - 250 mV to + 250 mV.
A time multiplexer scheme combines hit information from 12 RPC
channels into a single high-speed serial data stream that is passed to
a LeCroy 1877 pipelined TDC.  The multiplexing is accomplished with a
Xilinx XC4005E FGPA. A schematic diagram of the readout electronics is
shown in
%%%%%%%%%%%%%%%%(NIM Fig. 13)
Fig. \ref{eps-klm-readout}. In addition, the logical OR of the hits
for each 12-channel group is generated and is available for use as a
fast trigger signal.\\

%%%%%%%%%%%%% Figure 8 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-08.eps,width=14cm,angle=0}}
\vspace{5mm}
\caption{Schematic diagram of the KLM readout electronics.}
\label{eps-klm-readout}
\end{center}
\vspace{8mm}
\end{figure}

Each VME crate has a crate-controller board which transmits control
data from the string controller to the discriminator boards via the
dedicated VME backplane.  A 10 MHz clock signal from the
crate-controller board is distributed throughout the crate for the
discriminator boards in time sequencing the RPC hits.  The string
controller is a multi-function VME-compatible board using a Xilinx 4013
programmable gate array to allow downloading and control of a string
of up to 8 RPC readout crates.  Once the discriminator board is
programmed, the time sequenced hit information travels directly from
each 96 channel board to 8 TDC channels residing in a FASTBUS
crate. In this manner, 38 k RPC channels are reduced to 3200 FASTBUS
TDC channels, resulting in significant cost savings. It is also
possible to read RPC strip-hit data directly through the string
controllers, as was done during system commissioning when the
production TDC system was not yet available.

\subsection{Efficiency and resolution}

The relatively high resistance of the glass~\cite{klm-Morgan}, $\sim$
5 x $10^{12}$ $\Omega$ cm, limits the rate capability of these
counters to $\sim$ 0.2 Hz/cm$^2$. However, in the present application
in which the particle flux is little more than the cosmic ray flux,
the detectors function with high efficiency. Signals typically have a
100 mV peak into a 50 $\Omega$ termination and have a full width at
half maximum of less than 50 ns.  A typical RPC has a singles rate of
less than 0.03 Hz/cm$^2$ with few spurious discharges or
after-pulses. We operate the barrel (end-cap) modules at 4.3 (4.2)
kV/mm with a signal threshold of 40 (70) mV.  The choice of different
operating points is due to the differences in the characteristics of
the pickup strips for the barrel and end-cap modules.\\

%%%%%%%%%%%%%%(NIM Fig. 7-c)
Figure \ref{eps-klm-plateau} shows efficiency versus voltage curves
for individual RPC layers alone and in combination in a typical
super-layer in the end-cap. These data were obtained using cosmic rays.
The efficiency was obtained by tracing a particle using other
super-layers. The predicted location of the track was searched within
$\pm$ 1 strip. Although the area near the internal spacers is
inactive, care was taken to ensure that the internal spacers do not
overlap. Under the normal operating condition, the super-layer acts as
a logical "OR" for hits in either RPC layer and has an average
efficiency typically over 98 \%. \\

%%%%%%%%%%%%% Figure 9 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-09.eps,width=10cm,angle=0}}
\caption{Efficiency plateau curves for RPCs alone and in combination
in a typical super-layer of KLM.}
\label{eps-klm-plateau}
\end{center}
\vspace{-5mm}
\end{figure}

Cosmic rays were used to map the efficiency and to determine the
relative positions of all the super-layer modules. In addition, the
response of the modules to penetrating muons was measured and the
results were used as input to the simulation programs.  For example,
for a given set of operating voltage and discriminator threshold, a
penetrating muon generates the average hits of 1.4 strips and 1.9
strips per layer in the barrel and end-cap modules, respectively.\\

The spatial resolution of the modules is shown in	
%%%%%%%%%%%%%%(NIM Fig. 9)
Fig. \ref{eps-klm-spresol}. This residual distribution is the
difference between the measured and predicted hit locations using a
track that has been fitted using hits in the adjacent layers. The
multiplicity referred to is the number of strips in the super-layer
that have signals over threshold.  When two or more adjacent strips
have signals over threshold, the hit location used for particle
tracking is calculated by averaging the strips together.  For hits
with 1, 2, 3, and 4 strips, the standard deviations are 1.1, 1.1, 1.7,
and 2.9 cm, respectively.  The multiplicity weighted standard
deviation for this residual distribution is 1.2 cm and gives angular
resolution from the interaction point of better than 10 mrad.  TDCs
provide time information for the hits that can be used to eliminate
hits which are out of time with respect to the $e^+e^-$ collision.
The time resolution of KLM is a few ns.

%%%%%%%%%%%%% Figure 10 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{8mm}
\begin{center}
\vspace{5mm}
\centerline{\epsfysize=10cm\epsfbox{klm/klmfig-10a.eps}}
\caption{Spatial resolution of a super-layer of KLM}
\label{eps-klm-spresol}
\end{center}
\vspace{3mm}
\end{figure}


\subsection{Performance}

The system has been operating over one year.  When we first installed
the modules, we used $\frac{1}{4}$ inch diameter flexible polyolefin
tubing from the gas distribution manifolds to the RPCs. After several
weeks of operation, we noticed an increase in the dark current drawn
by some of the RPCs and the corresponding decrease in efficiency.
This was found to be due to water vapor in the air migrating through
the tubing and entering the RPC active volume.  Some of these tubes
were as long as 12 m and we measured concentrations of $H_2$O as high
as 2000 ppm in some RPC exhaust lines.  Approximately 50 \% of the
barrel modules were affected.  The efficiency of some barrel RPCs
dropped below 50 \% before corrective measures were taken. We replaced
the plastic tubing with copper tubing to prevent additional water
vapor entering the RPCs. The contaminated RPCs eventually dried out
and have recovered most of their lost efficiency.  Other than the
initial water vapor problem, which has been solved, the RPCs have been
operated reliably and with an average efficiency of better than 97 \%.

\subsubsection{$K_L$ detection}

In order to identify $K_L$, a cluster must be observed in KLM. Then,
tracks of charged particles measured in CDC are extrapolated into KLM.
Clusters within 15 degrees of an extrapolated charged particle track
are excluded from $K_L$ cluster candidates. For an isolated cluster,
the center of gravity of the hits is calculated and used to determine
the direction of the cluster from the interaction point.
%%%%%%%%%%%%%%(NIM Fig. 14)
Fig. \ref{eps-klm-neutral} shows a histogram of the difference between
the direction of the $K_L$ cluster candidate and the missing momentum
direction.  The data was obtained during the summer 1999 commissioning
run of the KEK B-factory.  The missing momentum vector is calculated
using all the other measured particles in the event.  The histogram
shows a clear peak where the direction of the neutral cluster measured
in KLM is consistent with the missing momentum in the event.  A large
deviation of the missing momentum direction from the neutral cluster
direction is mainly due to undetected neutrinos and particles escaping
the detector acceptance.
%%%%%%%%%%%(NIM Fig. 15)
Fig. \ref{eps-klm-noneutral} shows the number of neutral clusters per
event and a Monte Carlo simulation of the predicted number of $K_L$
clusters per event.  The average number of $K_L$ clusters per event is
0.5.  The agreement with the prediction gives us the confidence that
the detector and our reconstruction software are performing correctly.

\subsubsection{Muon detection}

Cosmic rays were used to measure the detection efficiency and
resolution of the super-layers. The momenta of cosmic muons were
measured by CDC with the solenoid field of 1.5 T. Below 500 MeV/c, the
muon does not reach KLM.  A comparison of the measured range of a
particle with the predicted range for a muon allows us to assign a
likelihood of being a muon.  In
%%%%%%%%%%%%%%%(NIM Fig. 16)
Fig. \ref{eps-klm-muoneff} the muon detection efficiency versus
momentum is shown for a likelihood cut of 0.66.  Some fraction of
charged pions and kaons will be misidentified as muons. A sample of
$K_S$ $ \rightarrow$ $ \pi ^+$$\pi ^-$ events in the $e^+e^-$
collision data was used to determine this fake rate.  The fraction of
pions which are misidentified as muons is shown in
%%%%%%%%%%%%%%%(NIM Fig. 17)
Fig. \ref{eps-klm-fake}, again with a muon likelihood cut of 0.66.
The solid points with error bars are the measured data and the
histogram is the result of a Monte Carlo simulation. Above 1.5 GeV/c
we have a muon identification efficiency of better than 90 \% with a
fake rate of less than 5 \%.

%%%%%%%%%%%%% Figure 11 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-11.eps,width=10cm,angle=0}}
\vspace{-5mm}
\caption{Difference between the neutral cluster and the direction of missing momentum in KLM.}
\label{eps-klm-neutral}
\end{center}
\end{figure}

%%%%%%%%%%%%% Figure 12 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/klmfig-12.eps,width=10cm,angle=0}}
\vspace{-5mm}
\caption{Number of neutral clusters per event in KLM.}
\label{eps-klm-noneutral}
\end{center}
\end{figure}

%%%%%%%%%%%%% Figure 13 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/nim_eff_905.eps,width=10cm,angle=0}}
\vspace{-5mm}
\caption{Muon detection efficiency versus momentum in KLM.}
\label{eps-klm-muoneff}
\end{center}
\end{figure}

%%%%%%%%%%%%% Figure 14 %%%%%%%%%%%%%%
\begin{figure}[hbt]
\vspace{-5mm}
\begin{center}
\epsfysize=10cm
\centerline{\psfig{file=klm/nim_fake_905.eps,width=10cm,angle=0}}
\vspace{0mm}
\caption{Fake rate versus momentum in KLM.}
\label{eps-klm-fake}
\end{center}
\vspace{-5mm}
\end{figure}

