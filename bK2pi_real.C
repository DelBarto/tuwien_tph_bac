#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TFile.h"
#include "TTree.h"
#include "RooArgList.h"

#include <iostream>
#include <fstream>

using namespace RooFit ;

void bK2pi_real()
{
  // int testfile = 2;

  std::string inFile1Str = "./bdata/evt_exp55_s0s1.root";
  std::string inFile2Str = "./bdata/on_res_exp55.root";
  std::string outFileStr = "./plots/res/data.txt";

  // >>>>>>>>>>>>>>>>generate plotfilenames>>>>>>>>>>>>>>>>
  std::string outSigStr = "./plots/res/MCsig.eps";
  std::string outBgStr = "./plots/res/MCbg.eps";
  std::string outBgSigStr = "./plots/res/RealBgSig.eps";
  std::string outAllStr = "./plots/res/pAll.eps";
  // <<<<<<<<<<<<<<<<generate plotfilenames<<<<<<<<<<<<<<<<


  //read in file
  TFile* inFile1 = TFile::Open(inFile1Str.c_str());
  TTree* tree1 = (TTree*) inFile1->Get("h1");
  TFile* inFile2 = TFile::Open(inFile2Str.c_str());
  TTree* tree2 = (TTree*) inFile2->Get("h1");

  //!!!!!!!!!!!!!!!!declaring variables!!!!!!!!!!!!!!!!
  Double_t mbcMin = 5.26;
  Double_t mbcMax = 5.29;

  RooRealVar m_bc("m_bc","m_bc",mbcMin,mbcMax,"GeV/c^2") ;
  RooRealVar delta_e("delta_e","delta_e",-0.5,0.5) ;
  RooRealVar fl_bd02p("fl_bd02p","fl_bd02p",0.,1.) ;



  // create 3 frames for signal, backround and signal+Background
  RooPlot *frame1_sig = m_bc.frame() ;
  RooPlot *frame2_bg = m_bc.frame() ;
  RooPlot *frame3 = m_bc.frame() ;
  frame1_sig->SetTitle("Signal");
  frame2_bg->SetTitle("Background");
  frame3->SetTitle("Signal + Background");


  //!!!!!!!!!!!!!!!!create datasets and cutting them!!!!!!!!!!!!!!!!


  RooDataSet* MCdata = new RooDataSet("MC data", "MC dataset", tree1, RooArgSet(m_bc,delta_e,fl_bd02p));

  RooPlot *frame_m_bc = m_bc.frame() ;
  frame_m_bc->SetTitle("");
  MCdata->plotOn(frame_m_bc);
  TCanvas can2;
  frame_m_bc->Draw();
  can2.SaveAs("delta_m_bc.eps");
  RooPlot *frame_deltaE = delta_e.frame() ;
  MCdata->plotOn(frame_deltaE);
    frame_deltaE->SetTitle("");
  TCanvas can1;
  frame_deltaE->Draw();
  can1.SaveAs("delta_E.eps");

  // RooDataSet* nData = (RooDataSet*) MCdata->reduce("(abs(delta_e) < 0.1)");
  RooDataSet* MCsig = (RooDataSet*) MCdata->reduce("(m_bc > 5.25) && (abs(delta_e) < 0.1) && (fl_bd02p == 1)");
  MCsig->plotOn(frame1_sig);
  RooDataSet* MCbg = (RooDataSet*) MCdata->reduce("(m_bc > 5.25) && (abs(delta_e) < 0.1) && (fl_bd02p == 0)");
  MCbg->plotOn(frame2_bg);

  RooDataSet Tdata("Test data", "Test dataset", tree2, RooArgSet(m_bc,delta_e,fl_bd02p));
  RooDataSet* Tall = (RooDataSet*) Tdata.reduce("(m_bc > 5.25) && abs(delta_e)<0.015");
  Tall->plotOn(frame3);

  Int_t nbr_Tdata = Tdata.numEntries();

  ////////////////////////////////////////////////////////////////////////////
  //################# Signal #################
  m_bc.setMin(5.26) ;

  // --- Build Gaussian PDF ---
  RooRealVar meanSig("meanSig","B^{#pm} mass",5.279,mbcMin,mbcMax) ;
  RooRealVar sigmSig1("sigmSig1","B^{#pm} width",0.0027,0.001,0.2) ;
  RooRealVar sigmSig2("sigmSig2","B^{#pm} width",0.01,0.001,0.1) ;
  RooRealVar weiSig("weiSig","weight of the doubleGauss",0.5,0.,1.) ;

  RooGaussian sig1("sig1","gaussian signal1",m_bc,meanSig,sigmSig1);
  RooGaussian sig2("sig2","gaussian signal2",m_bc,meanSig,sigmSig2);

  RooAddModel modSig("modSig","Model for Signal",RooArgList(sig1,sig2), weiSig) ;

  modSig.fitTo(*MCsig);
  modSig.plotOn(frame1_sig);

  // fix Signal parameters
  meanSig.setConstant(kTRUE);
  sigmSig1.setConstant(kTRUE);
  sigmSig2.setConstant(kTRUE);
  weiSig.setConstant(kTRUE);

  //################# Background #################
  m_bc.setMin(mbcMin) ;

  // --- Build Gaussian PDF ---
  RooRealVar weiBg("weiBg","weight parameter arg gaus",0.5,0.,1.);
  RooRealVar sigmBg("sigmBg","B^{#pm} width",0.0027,0.001,0.1) ;
  // RooRealVar nBg1("nBg1","#background1 events",500,0.,10000) ;
  RooGaussian bg_gauss("bg1","gaussian background",m_bc,meanSig,sigmBg);

  // --- Build Argus background PDF ---
  RooRealVar argpar("argpar","argus shape parameter",-50.0,-100.,-1.) ;
  // RooRealVar nBg2("nBg2","#background2 events",700,0.,10000) ;

  RooArgusBG bg_argus("bg2","Argus background",m_bc,RooConst(mbcMax),argpar) ;

  // RooAddModel modBg("modBg","Model for Background",RooArgList(bg1,bg2),RooArgList(nBg1,nBg2)) ;
  RooAddModel modBg("modBg","Model for Background",RooArgList(bg_gauss,bg_argus),weiBg) ;

  modBg.fitTo(*MCbg);
  modBg.plotOn(frame2_bg);

  sigmBg.setConstant(kTRUE);
  argpar.setConstant(kTRUE);
  weiBg.setConstant(kTRUE);
  ////////////////////////////////////////////////////////////////////////////

  //################# Signal + Background #################

  RooRealVar nS("nSig","#signal events",3000.,0.,nbr_Tdata) ;
  RooRealVar nB("nBg","#baclground events",6000.,0.,nbr_Tdata) ;

  RooAddPdf modAll("modAll","Model for Data",RooArgList(modBg,modSig),RooArgList(nB,nS)) ;

  modAll.fitTo(*Tall);

  m_bc.setMin(mbcMin);
  modAll.plotOn(frame3);
  modAll.plotOn(frame3, Components(modBg), LineStyle(kDashed));




  //################# Dataoutput #################
  ofstream myfile;
  myfile.open (outFileStr);
  myfile << "#### Signal ####\n";
  myfile << "meanSig: " << meanSig.getVal() << "\n";
  myfile << "sigmSig1: " << sigmSig1.getVal() << "\n";
  myfile << "sigmSig2: " << sigmSig2.getVal() << "\n";
  myfile << "weiSig: " << weiSig.getVal() << "\n";

  myfile << "#### Background ####\n";
  // myfile << "meanBg: " << meanBg.getVal() << "\n";
  myfile << "sigmBg: " << sigmBg.getVal() << "\n";
  myfile << "argpar: " << argpar.getVal() << "\n";
  myfile << "weiBg: " << weiBg.getVal() << "\n";

  myfile << "#### Signal + Background #### \n";
  myfile << "nS: " << nS.getVal() << " " << nS.getError() << "\n";
  myfile << "nB: " << nB.getVal() << " " << nB.getError() << "\n";

  myfile << "####################\n";

  myfile.close();


  cout << endl;
  cout << "############ nS & nB ############" << endl;
  nS.Print();
  nB.Print();
  cout << endl;


  //################# Draw #################

  TCanvas canSig("Signal","Signal",500,500);
  frame1_sig->Draw();
  canSig.SaveAs(outSigStr.c_str());
  TCanvas canBg("Background","Background",500,500);
  frame1_sig->Draw();
  canBg.SaveAs(outBgStr.c_str());
  TCanvas canSigBg("Signal + Background","Signal + Background",500,500);
  frame1_sig->Draw();
  canSigBg.SaveAs(outBgSigStr.c_str());

  // create canvas
  TCanvas* c = new TCanvas("rf101_basics","rf101_basics",1500,500) ;
  c->Divide(3) ;
  c->cd(1) ; gPad->SetLeftMargin(0.15) ; frame1_sig->GetYaxis()->SetTitleOffset(1.6) ; frame1_sig->Draw() ;
  c->cd(2) ; gPad->SetLeftMargin(0.15) ; frame2_bg->GetYaxis()->SetTitleOffset(1.6) ; frame2_bg->Draw() ;
  c->cd(3) ; gPad->SetLeftMargin(0.15) ; frame3->GetYaxis()->SetTitleOffset(1.6) ; frame3->Draw() ;
  // c->cd(1)->SaveAs(outSigStr.c_str());
  // c->cd(2)->SaveAs(outBgStr.c_str());
  // c->cd(3)->SaveAs(outBgSigStr.c_str());
  c->SaveAs(outAllStr.c_str());

}
