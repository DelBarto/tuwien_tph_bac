#$$B^+ \rightarrow \bar{D}^0 \pi^+ \rightarrow \left[ K^+ \pi^- \right]_{_{\bar D^0}} \pi^+$$

##Q & A

-   better_charged



```cpp
Gen_hepevt_Manager& gen_m = Gen_hepevt_Manager::get_manager();
```
Managet eventliste

-   rec_b2d0pi

```cpp
// right sign combinations only
//if ( i->charge() * j->charge() > 0. ) continue;
//instead of looking for the right sign just looking for pi- because we accept only K+
//looking for piom- -> -1e [C]
if ( j->charge() > 0. ) continue;
```
nur den genauen Zerfall durchmachen

```cpp
atc_pid selKpi(3,1,5,3,2);
```

```cpp
i->charge()
```
charge of particle

```cpp
selKpi.prob ( *i );
```

```cpp
better_charged( *j, 2 )
```

```cpp
if (b.p().mag() - d0.p().mag() > .25 ) continue;
```


##Access & Commands

-   [Belle](http://belle.kek.jp) ```bellewww B->mu+nu```
-   ```ssh -Y user@bellle(.local.hephy.at)```
-   außen zusätzlich: ```ssh -Y user@lxbelle.hephy.oeaw.ac.at```
-   compile .c-file: ```make```
-   execute compiled file: ```sh ./#filename#.csh```
-   convert .h-file to .root file: ```h2root '#filename#.hbook'```
-   ```root```; for GUI: ```new TBrowser();```
-   copy file ```scp benutzerx@server1:datei1 datei2 benutzery@server2: ```
-   zombies in background ```ipcs``` --> ```fpda_kill #fpda_pid.1502#```
-   hephy wifi ""


##Teilchen
<img src="/home/bdan/MegaSync/TU_Wien/Technische Physik/BacArb/notes/pics/stdmoep.svg" style="display: block; margin-left: auto; margin-right: auto;width: 400px;"/>

-   [Hadron](https://de.wikipedia.org/wiki/Hadron) --> zusammengehalten von starken Wechselwirkung
    -   [Mesonen](https://de.wikipedia.org/wiki/Meson) --> Quark-Antiquark-Paar; ganzzahliger Spin; Bosonen (k. gl. Zustand haben am selben Ort)
    -   [Baryonen](https://de.wikipedia.org/wiki/Baryon) --> 3-Quarks; halbzahliger Spin; Fermionen (k. nicht gl. Zustand haben am selben Ort)

##Todo
local file:

  - [x] init
  - [x] hist_def
  - [x] event
  - [x] begin_run
  - [x] count_b2d0pi
  - [?] rec_b2d0pi
  - [x] lab2cm
  - [x] better_charged
  - [x] flag_b2d0pi

##Mo, 2017-07-24

#. [Particlecode](http://home.fnal.gov/~mrenna/lutp0613man2/node44.html) --> [alternative](http://research.npl.illinois.edu/exp/eic/bootcamp/gmc1/lundcodes.html#meson1)
    + [B-Meson](https://de.wikipedia.org/wiki/B-Meson) - $B^+ \rightarrow u {\bar b} : 521$
    + [D-Meson](https://de.wikipedia.org/wiki/D-Meson) - $\bar D^0 \rightarrow {\bar c} u : -421$
    + [Pion](https://de.wikipedia.org/wiki/Pion) - $\pi^+ \rightarrow d{\bar u} : 211$
    + [Kaon](https://de.wikipedia.org/wiki/Kaon) - $K^+ \rightarrow {\bar s} u : 321$
    + [Pion](https://de.wikipedia.org/wiki/Pion) - $\pi ^{-} \rightarrow {\bar u} d : -211$

---

nicht ganz correct --> weiter unten richtig gestellt

$$m_B = \sqrt{ ({E_B^\ast})^2 + ({\vec{p}_B^\ast})^2} ~~~~~~, mit ~~~~~~ E_{B}^\ast = 5.28 [\text{GeV}]$$

[]:  (Lorentztranfo in SPS $\rightarrow$)

$$n_{bc} = \sqrt{ ({E_{beam}^\ast})^2 + ({\vec{p}_B^\ast})^2} ~~~~~~, mit ~~~~~~ E_{beam}^\ast$$

$$\Rightarrow \Delta E = E^{ * }_{B} - E^{ * }_{beam}$$

---

##Fr, 2017-07-28

+ [B-Meson](https://de.wikipedia.org/wiki/B-Meson): $B^+ : m_{B^\pm} =\sim 5.279 ~ [\text{GeV}]~, ~~ q_{B^\pm} = 1e [\text{C}]$

+ [D-Meson](https://de.wikipedia.org/wiki/D-Meson): $\bar D^0 : m_{\bar D^0} =\sim  ~ 1.865 [\text{GeV}]~, ~~ q_{\bar D^0} = 0e [\text{C}]$

+ [Pion](https://de.wikipedia.org/wiki/Pion): $\pi^\pm : m_{\pi^\pm} =\sim 0.14 ~ [\text{GeV}]~, ~~ q_{\pi^\pm} = \pm 1e [\text{C}]$
+ [Kaon](https://de.wikipedia.org/wiki/Kaon): $K^+ : m_{K^+} =\sim 0.493 ~ [\text{GeV}]~, ~~ q_{K^+} = 1e [\text{C}]$

charge for antiparticle times $(-1)$ but mass is the same therefore is $$\pi^+ \rightarrow u{\bar d} : m =\sim 0.14 ~ [\text{GeV}]~, ~~ q = 1e [\text{C}]$$ $$\downarrow$$ $$\pi^-  \rightarrow d{\bar u} : m =\sim 0.14 ~ [\text{GeV}]~, ~~ q = -1e [\text{C}]$$

##Sa, 2017-07-29
![](./pics/bacarb_pic_1.jpg )

$$\mathtt{cos}(\alpha) = \dfrac{\vec{a} \cdot \vec{b}}{{|\vec{a}|}{|\vec{b}|}}$$

##So, 2017-07-30
![](./pics/bacarb_pic_2.jpg )

zugriff auf 3er-Impuls ```d0.p3()```  
zugriff auf 4er-Impuls ```d0.p()```


##Mo, 2017-07-31

```cpp
selKpi();
double id_k = selKpi.prob ( *i );
```

##Di, 2017-08-01
![](pics/bacarb_pic_3.jpg )

##Do, 2017-08-03

$$m_B = \sqrt{ ({E_B^\ast})^2 - ({\vec{p}_B^\ast})^2} = \sqrt{ ({E_{K^+}^\ast} + {E_{\pi-}^\ast} + {E_{\pi+}^\ast})^2 - ({\vec{p}_{K^+}^\ast} + {\vec{p}_{\pi^-}^\ast} + {\vec{p}_{\pi^+}^\ast})^2} $$
$$, mit ~~~~~~ E_{B}^\ast = 5.29 [\text{GeV}]$$



$$m_{bc} = \sqrt{ ({E_{beam}^\ast})^2 - ({\vec{p}_B^\ast})^2} ~~~~~~, mit ~~~~~~ E_{beam}^\ast$$

$$\Rightarrow \Delta E = E^{ * }_{beam} - E^{ * }_{B}$$

##Fr, 2017-08-04

$\pi^+$: fast Pion (slow pi) -> pip(lus)
$\pi^-$: faster Pion (fast pi) -> pim(inus)


neu schreiben  

- [x] init
- [x] hist_def
- [x] event
- [x] begin_run
- [x] count_b2d0pi
- [x] rec_b2d0pi
- [x] lab2cm
- [x] better_charged
- [x] flag_b2d0pi

Es müssen auch alle konjugiert komplexen Teilchen berücksichtigt werden beim den Funktionen.

## Do, 2017-08-17

[roofit-20-minutes](https://root.cern.ch/roofit-20-minutes): ```roofit```

![](pics/argus_gaus-plot.png)

$$N_{sig} = N_{B \bar{D}} ~ 2 ~ \frac{1}{2} ~ \mathtt{Br}(B^{+} \rightarrow \bar{D}^{0} \pi^{+}) ~ \mathtt{Br}(\bar{D}^{0} \rightarrow K^{+} \pi^{-}) $$

$$\mathtt{Br}(B^{+} \rightarrow \bar{D}^{0} \pi^{+}) = \frac{N_{sig}}{N_{B \bar{D}} ~ \mathtt{Br}(\bar{D}^{0} \rightarrow K^{+} \pi^{-}) ~ \varepsilon}$$

$\varepsilon =$ ... efficiency

-----

$$B^+ \rightarrow \bar{D}^0 \pi^+$$
![](pics/BDpi.png)

----

$$\bar{D^{0}} \rightarrow K^+ \pi^-$$
![](pics/D0Kpi.png)

----

## Di, 2017-08-22

>Ihre Jobs ( insgesamt 28 ) sind am Wochenende erfolgreich in Japan gelaufen. Der output befindet sich in diesem Verzeichnis auf belle:
>
>**/home/schwanda/public/bdan**
>
>Gelaufen ist Ihr Programm über die Realdaten des Experimentes 55 ( on_res_exp55_*.hbook ) sowie die zughörige charged und mixed Monte Carlo ( evt_charged_exp55_*.hbook und evt_mixed_exp55_*.hbook ), genauer über die streams 0, 1 und 2. ( Ein Stream entspricht der Luminosität des korrespondierenden Belle-Datensatzes. )
>
>Experiment 55 enthält 80,2472 +1,2462 / -1,2439 Millionen Y(4S) -> [BB events  ](http://belle.kek.jp/secured/nbb/nbb.html) (http://belle.kek.jp/secured/nbb/nbb.html).
>
>Die Dateien 91132*.out sind der Textoutput der Jobs, den ich bereits auf mögliche Fehlermeldungen überprüft habe.
>
>Die Aufspaltung auf viele Jobs ist notwendig, um das CPU Limit der batch queue nicht zu überschreiten. Zur interaktiven Analyse ist es aber praktisch, den output wieder zu vereinigen. In diesem Zusammenhang möchte ich Sie auf den **hadd**-Befehl hinweisen, der es gestattet mehrere root-Files in ein neues zu vereinigen.


## Do, 2017-08-24

![](pics/ex_mbc.png)

```
.x #filename#
```

## Do, 2017-08-31

- [x] hadd mixed with charged
- [x] rewrite flag function
- [ ] make background and signal fit with one MC-Stream but flaged
    - [x] background
    - [x] signal (overlay of 2 Gaussian signals with same mean)
- [x] make background + signal fit with other MC-Stream

$$N_{sig} = N_{B \bar{B}} ~ 2 ~ \frac{1}{2} ~ \mathtt{Br}(B^{+} \rightarrow \bar{D}^{0} \pi^{+}) ~ \mathtt{Br}(\bar{D}^{0} \rightarrow K^{+} \pi^{-}) ~ \varepsilon$$

$$\mathtt{Br}(B^{+} \rightarrow \bar{D}^{0} \pi^{+}) = \frac{N_{sig}}{N_{B \bar{B}} ~ \mathtt{Br}(\bar{D}^{0} \rightarrow K^{+} \pi^{-}) ~ \varepsilon}$$


$$\varepsilon = \dfrac{N_{sig_{true}}}{N_{B\bar{B}}}$$

Verzweigungsverhältnis
-----
(in engl. "branching fraction" oder "branching ratio")
Wahrscheinlichkeit ein Teilchenzustand in einen bestimmten Endzustand zerfällt

-----

$$B^+ \rightarrow \bar{D}^0 \pi^+$$
$$\mathtt{Br}(B^{+} \rightarrow \bar{D}^{0}) = (4.81 \pm 0.15) * 10^{-3} $$
![](pics/BDpi.png)

----

FALSCH!!!
$$\bar{D^{0}} \rightarrow K^+ \pi^-$$
$$\mathtt{Br}(\bar{D}^{0} \rightarrow K^{+} \pi^{-}) = (1.6 ) * 10^{-5} $$
![](pics/D0Kpi.png)

>in aktueller Version ist Wert 1.6


## Do, 2017-09-14
$$\bar{D^{0}} \rightarrow K^+ \pi^-$$
$$\mathtt{Br}(\bar{D}^{0} \rightarrow K^{+} \pi^{-}) = 0.039 $$
![](pics/D0-Kpi.png)

----

## Do, 2017-09-21
- [x] Deckblatt (en,de)
- [ ] root
    - [x] draw order
    - [x] fit -> draw (difference)
    - [x] normalize data
    - [x] enable parameters
- [x] efficiency


##Do, 2017-10-05

- [ ] intersection angle?
- [ ] picture source?
    - [ ] Wiki picture source?
- [ ] B decay vertex position
- [ ] Luminosity
- [ ] ultimate beam crossing rate
- [ ] pull test


>Nein, der source code muss nicht in der Arbeit stehen. Eine schriftliche Beschreibung der Analyse ( dessen was das Programm macht ) mit Angabe der cuts ist ausreichend. Dadurch sehe ich auch gleich, ob Sie den Ablauf der Analyse gut verstanden haben. Für den Roofit-Teil sollten Sie das Signal- und Untergrund-Modell beschreiben, wie die Modell-Parameter bestimmt wurden ( Werte in Tabelle ) und wie das Modell auf MC-Daten getestet wurde ( Pull-Verteilung ).
