//
//   B0 -> D*- pi+, D*+ -> D0 pi+ and D0 -> K- pi+ reconstruction
//   for slow pion efficiency measurement
//
//   Christoph Schwanda, H20/11/5
//

#include <math.h>
#include <iostream.h>
#include <vector.h>
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "tuple/BelleTupleManager.h"

#include "event/BelleEvent.h"
#include "basf/module.h"
#include "basf/module_descr.h"
#include "panther/panther.h"
#include BELLETDF_H
#include LEVEL4_H
#include EVTCLS_H
#include MDST_H
#include HEPEVT_H
#include "mdst/mdst.h"

#include "particle/Particle.h"
#include "kid/atc_pid.h"
#include "ip/IpProfile.h"

// particle codes
#define ID_PI 211
#define ID_K 321

#define ID_D0 421
#define ID_DSTAR 413
#define ID_B0 511

// beam parameters ( as used in the MC )
#define E_HER 7.998213
#define E_LER 3.499218
#define THETA 0.022

// module class **************************************************************

class b0dspi : public Module {

public:

b0dspi( void ) {
};
~b0dspi( void ) {
};
void init( int* );
void term( void ) {
};
void disp_stat( const char* ) {
};
void hist_def( void );
void event( BelleEvent*, int* );
void begin_run( BelleEvent*, int* );
void end_run( BelleEvent*, int* ) {
};
void other( int*, BelleEvent*, int* ) {
};

private:

void count_b0k3pi( void );
void rec_b0k3pi( void );

HepLorentzVector lab2cm( HepLorentzVector );
int better_charged( const Mdst_charged&, int );
int flag_b0k2pi( const Mdst_charged&, const Mdst_charged&,
                 const Mdst_charged& );
int flag_b0k3pi( const Mdst_charged&, const Mdst_charged&,
                 const Mdst_charged&, const Mdst_charged& );

// "global variables"

// event()
int exp_no, run_no, crate_no, evt_no, fl_mc;

// histogram and ntuple
BelleTuple *t_1, *t_2;
BelleHistogram *h_1, *h_2, *h_3;

};

extern "C" Module_descr *mdcl_b0dspi() {

        b0dspi *module = new b0dspi;
        Module_descr *dscr = new Module_descr ( "b0dspi", module );
        return dscr;

}

// initialization ************************************************************

void b0dspi::init( int *status ) {

        // dummy construction for Ptype initialization
        Ptype ptype_dummy("VPHO");

        cout << endl;
        cout << "b0dspi: module b0dspi initialized" << endl;
        cout << endl;

}


// histogram and ntuple definition *******************************************

void b0dspi::hist_def( void ) {

        extern BelleTupleManager *BASF_Histogram;

        t_1 = BASF_Histogram->ntuple( "b0k2pi",
                                      "exp_no run_no crate_no evt_no "
                                      "p_k id_k p_pif id_pif m_kpi "
                                      "p_pi id_pi" );

        t_2 = BASF_Histogram->ntuple( "b0k3pi",
                                      "exp_no run_no crate_no evt_no "
                                      "p_k id_k p_pif id_pif m_kpi "
                                      "p_pis id_pis delta_m "
                                      "p_pi id_pi m_bc delta_E "
                                      "fl_b0k3pi" );

        // event()
        h_1 = BASF_Histogram->histogram( "no. hadronb", 1, 0., 1.);
        h_2 = BASF_Histogram->histogram( "fix_mdst", 3, -1., 2.);

        // count_b0k3pi()
        h_3 = BASF_Histogram->histogram( "no. b0k3pi", 1, 0., 1.);

}

// event function ************************************************************

void b0dspi::event( BelleEvent *evptr, int *status ) {

        // event information
        Belle_event_Manager &event_m = Belle_event_Manager::get_manager();
        Belle_event &event = event_m( ( Panther_ID ) 1 );
        if ( !event ) return;

        exp_no = event.ExpNo();
        run_no = event.RunNo();

        // event number is stored in Belle_event table as combination
        // of farm number and event number, event number is the 28 LSBs
        // and farm number is to the "left" of this
        int raw_evt_no = event.EvtNo();
        crate_no = raw_evt_no >> 28;
        evt_no = raw_evt_no & ~(~0 << 28);

        fl_mc = ( event.ExpMC() == 2 );

        // for MC: check trigger level 4 condition

        if ( fl_mc ) {
                L4_summary_Manager& l4_m = L4_summary_Manager::get_manager();
                L4_summary &l4 = l4_m( ( Panther_ID ) 1 );
                if ( !l4 ) return;

                if( l4.type() == 0 ) return;
        }

        // check HadronB condition
        Evtcls_hadronic_flag_Manager& evtcls_m =
                Evtcls_hadronic_flag_Manager::get_manager();
        Evtcls_hadronic_flag &evtcls = evtcls_m( ( Panther_ID ) 1 );
        if ( !evtcls ) return;

        if ( evtcls.hadronic_flag(2) != 10 && evtcls.hadronic_flag(2) != 20 )
                return;

        h_1->accumulate( 0.5, 1. );
        h_2->accumulate( *status+0.5, 1. );

        // count generated B0 -> D*-(-> D0bar[-> K+ pi-] pi-) pi+
        count_b0k3pi();

        // reconstruct B0 -> D*-(-> D0bar[-> K+ pi-] pi-) pi+
        rec_b0k3pi();

}


// begin of run **************************************************************
void b0dspi::begin_run( BelleEvent *evptr, int *status ) {

        // get IP profile data
        IpProfile::begin_run();
        //  IpProfile::dump();

}

// count generated B0 -> D*-(-> D0bar[-> K+ pi-] pi-) pi+ ********************
void b0dspi::count_b0k3pi( void ) {

        if ( !fl_mc ) return;

        Gen_hepevt_Manager& gen_m = Gen_hepevt_Manager::get_manager();

        for ( std::vector<Gen_hepevt>::iterator i = gen_m.begin();
              i != gen_m.end(); i++ ) {

                // look for B0 meson
                Gen_hepevt& hepevt_b0 = *i;
                if ( abs( hepevt_b0.idhep() ) != ID_B0 ) continue;

                // scan B0 daughters
                Gen_hepevt_Index gen_i = gen_m.index( "mother" );
                gen_i.update();

                std::vector<Gen_hepevt> b0_daughters =
                        point_from( hepevt_b0.get_ID(), gen_i );
                if ( b0_daughters.size() != 2 ) continue;

                Gen_hepevt *pt_dstar, *pt_pi;
                pt_dstar = pt_pi = 0;

                for ( std::vector<Gen_hepevt>::iterator j = b0_daughters.begin();
                      j != b0_daughters.end(); j++ ) {

                        int id = abs( j->idhep() );

                        // look for D*
                        if ( id == ID_DSTAR ) pt_dstar = &( *j );

                        // look for pion
                        if ( id == ID_PI ) pt_pi = &( *j );
                }

                if ( pt_dstar == 0 || pt_pi == 0 ) continue;

                // scan D* daughters
                Gen_hepevt& hepevt_dstar = *pt_dstar;

                std::vector<Gen_hepevt> dstar_daughters =
                        point_from( hepevt_dstar.get_ID(), gen_i );
                if ( dstar_daughters.size() != 2 ) continue;

                Gen_hepevt *pt_d0, *pt_pis;
                pt_d0 = pt_pis = 0;

                for ( std::vector<Gen_hepevt>::iterator j = dstar_daughters.begin();
                      j != dstar_daughters.end(); j++ ) {

                        int id = abs( j->idhep() );

                        // look for D0
                        if ( id == ID_D0 ) pt_d0 = &( *j );

                        // look for pion
                        if ( id == ID_PI ) pt_pis = &( *j );
                }

                if ( pt_d0 == 0 || pt_pis == 0 ) continue;

                // scan D0 daughters
                Gen_hepevt& hepevt_d0 = *pt_d0;

                std::vector<Gen_hepevt> d0_daughters =
                        point_from( hepevt_d0.get_ID(), gen_i );
                if ( d0_daughters.size() != 2 ) continue;

                Gen_hepevt *pt_k, *pt_pif;
                pt_k = pt_pif = 0;

                for ( std::vector<Gen_hepevt>::iterator j = d0_daughters.begin();
                      j != d0_daughters.end(); j++ ) {

                        int id = abs( j->idhep() );

                        // look for kaon
                        if ( id == ID_K ) pt_k = &( *j );

                        // look for pion
                        if ( id == ID_PI ) pt_pif = &( *j );
                }

                if ( pt_k == 0 || pt_pif == 0 ) continue;

                // great! we have a b0k3pi candidate!
                h_3->accumulate( 0.5, 1. );
        }
}

// B0 -> D*-(-> D0bar[-> K+ pi-] pi-) pi+ reconstruction *********************
void b0dspi::rec_b0k3pi( void ) {

        Mdst_charged_Manager &charged_m = Mdst_charged_Manager::get_manager();

//************

        // loop over kaons
        for( std::vector<Mdst_charged>::iterator i = charged_m.begin();
             i != charged_m.end(); i++ )
        {
                // track selection
                if ( !better_charged( *i, 3 ) ) continue;

                // kaon identification

                // accq =  3 Probability is calculated based on measured Npe and PDF
                // tofq =  1 Used with checking Z hit position w.r.t the track extrapol.
                // cdcq =  5 Correct dE/dx run-dependent shift in 2001 summer reprocesing
                atc_pid selKpi(3,1,5,3,2);

                double id_k = selKpi.prob ( *i );
                if ( id_k < 0.6 ) continue;

                Particle kaon;
                if ( i->charge() > 0. )
                {
                        kaon = Particle( *i, ( std::string )"K+" );
                }

                else
                {
                        kaon = Particle( *i, ( std::string ) "K-" );
                }


//****************

                // loop over fast d0pions
                for( std::vector<Mdst_charged>::iterator j = charged_m.begin();
                     j != charged_m.end(); j++ )
                {

                        if ( j == i ) continue;

                        // right sign combinations only
                        if ( i->charge() * j->charge() > 0. ) continue;

                        // track selection
                        if ( !better_charged( *j, 2 ) ) continue;

                        // kaon rejection
                        double id_d0pif = selKpi.prob ( *j );
                        if ( id_d0pif > 0.9 ) continue;

                        Particle d0pion_fast;
                        if ( j->charge() > 0. )
                        {
                                d0pion_fast = Particle( *j, ( std::string ) "PI+" );
                        }
                        else
                        {
                                d0pion_fast = Particle( *j, ( std::string ) "PI-" );
                        }

                        //=>

                        // D0 reconstruction, 1.76 < m( K- pi+ ) < 1.96 GeV/c2 <- woher der cut?!?
                        HepLorentzVector p_lv_d0 = kaon.p() + d0pion_fast.p();

                        Ptype ptype_d0 = Ptype( "D0" );
                        Ptype ptype_d0_bar = Ptype( "D0B" );

                        Particle d0;
                        if ( kaon.charge() < 0. )
                        {
                                d0 = Particle( p_lv_d0, ptype_d0 );
                        }
                        else
                        {
                                d0 = Particle( p_lv_d0, ptype_d0_bar );
                        }

                        if ( d0.p().mag() < 1.76 || d0.p().mag() > 1.96 ) continue;


                        // fit charged tracks to a common vertex
                        kvertexfitter vtx;
                        addTrack2fit( vtx, kaon );
                        addTrack2fit( vtx, d0pion_fast );
                        unsigned err = vtx.fit();
                        if ( err != 0 ) continue;
                        double prb_vtx = Chisq::Prob( vtx.chisq(), vtx.dgf() );

                        // compute D0 decay angle
                        HepLorentzVector p_lv = kaon.p();
                        p_lv.boost( -( d0.p().boostVector() ) );
                        double cos_d =
                                ( p_lv.vect()*d0.p3() )/( p_lv.vect().mag()*d0.p3().mag() );

//*****************

                        // loop over slow dstarpions
                        for( std::vector<Mdst_charged>::iterator k = charged_m.begin();
                             k != charged_m.end(); k++ )
                        {

                                if ( k == i || k == j ) continue;

                                // right sign combinations only
                                if ( i->charge() * k->charge() > 0. ) continue;

                                // track selection
                                // if ( !better_charged( *k, 2 ) ) continue;

                                // kaon rejection
                                double id_dstarpis = selKpi.prob( *k );
                                if ( id_dstarpis > 0.9 ) continue;

                                Particle dstarpion_slow;
                                if ( k->charge() > 0. )
                                {
                                        dstarpion_slow = Particle( *k, ( std::string ) "PI+" );
                                }
                                else
                                {
                                        dstarpion_slow = Particle( *k, ( std::string ) "PI-" );
                                }


                                //=>

                                // D* reconstruction, Delta m to D0 < 250 MeV <- woher cut?? slow pi's??
                                HepLorentzVector p_lv_dstar = d0.p() + dstarpion_slow.p();

                                Ptype ptype_dstar_plus = Ptype( "D*+" );
                                Ptype ptype_dstar_minus = Ptype( "D*-" );

                                Particle dstar;
                                if ( dstarpion_slow.charge() > 0. )
                                {
                                        dstar = Particle( p_lv_dstar, ptype_dstar_plus );
                                }
                                else
                                {
                                        dstar = Particle( p_lv_dstar, ptype_dstar_plus );
                                }


                                if ( dstar.p().mag() - d0.p().mag() > .25 ) continue;

//******************

                                // loop over fast b0pions
                                for( std::vector<Mdst_charged>::iterator l = charged_m.begin();
                                     l != charged_m.end(); l++ )
                                {

                                        if ( l == i || l == j || l == k ) continue;

                                        // right sign combinations only
                                        if ( k->charge() * l->charge() > 0. ) continue;

                                        // track selection
                                        if ( !better_charged( *l, 2 ) ) continue;

                                        // kaon rejection
                                        double id_b0pif = selKpi.prob ( *l );
                                        if ( id_b0pif > 0.9 ) continue;

                                        Particle b0pion_fast;
                                        if ( l->charge() > 0. )
                                        {
                                                b0pion_fast = Particle( *l, ( std::string ) "PI+" );
                                        }
                                        else
                                        {
                                                b0pion_fast = Particle( *l, ( std::string ) "PI-" );
                                        }

                                        //=>

                                        // B0 reconstruction,
                                        HepLorentzVector p_lv_b0 = b0pion_fast.p() + dstar.p();

                                        Ptype ptype_b0 = Ptype( "B0" );
                                        Ptype ptype_b0_bar = Ptype( "B0B" );

                                        Particle b0;
                                        if ( b0pion_fast.charge() < 0. )
                                        {
                                                b0 = Particle( p_lv_b0, ptype_b0 );
                                        }
                                        else
                                        {
                                                b0 = Particle( p_lv_b0, ptype_b0_bar );
                                        }


                                        //=>>>

                                        // m_bc cut > 5.20 GeV
                                        if ( E_HER*E_LER - (lab2cm( b0.p() ).px()*lab2cm( b0.p() ).px() + lab2cm( b0.p() ).py()*lab2cm( b0.p() ).py() + lab2cm( b0.p() ).pz()*lab2cm( b0.p() ).pz() ) < 0. ) continue;
                                        if ( sqrt( E_HER*E_LER - (lab2cm( b0.p() ).px()*lab2cm( b0.p() ).px() + lab2cm( b0.p() ).py()*lab2cm( b0.p() ).py() + lab2cm( b0.p() ).pz()*lab2cm( b0.p() ).pz() ) ) < 5.20 ) continue;

                                        // delta_E cut abs < 300 MeV
                                        if ( abs( lab2cm( b0.p() ).e() - sqrt(E_HER*E_LER) ) > 0.3 ) continue;

                                        //=>>

                                        // fill ntuple
                                        t_1->column ( "exp_no", exp_no );
                                        t_1->column ( "run_no", run_no );
                                        t_1->column ( "crate_no", crate_no );
                                        t_1->column ( "evt_no", evt_no );

                                        // rho() equivalent to vect().mag()
                                        t_1->column ( "ps_k", lab2cm( kaon.p() ).rho() );
                                        t_1->column ( "id_k", id_k );
                                        t_1->column ( "ps_d0pif", lab2cm( d0pion_fast.p() ).rho() );
                                        t_1->column ( "id_d0pif", id_d0pif );
                                        t_1->column ( "m_kpi", d0.p().mag() );
                                        t_1->column ( "prb_vtx", prb_vtx );
                                        t_1->column ( "cos_d", cos_d );

                                        t_1->column ( "ps_dstarpis", lab2cm( dstarpion_slow.p() ).rho() );
                                        t_1->column ( "id_dstarpis", id_dstarpis );
                                        t_1->column ( "delta_m_dstar", dstar.p().mag() - d0.p().mag() );

                                        t_1->column ( "ps_b0pif", lab2cm( b0pion_fast.p() ).rho() );
                                        t_1->column ( "id_b0pif", id_b0pif );
                                        t_1->column ( "delta_m_b0", b0.p().mag() - dstar.p().mag() );

                                        t_1->column ( "delta_E", lab2cm( b0.p() ).e() - sqrt( E_HER*E_LER ) );
                                        t_1->column ( "m_bc", sqrt( E_HER*E_LER - (lab2cm( b0.p() ).px()*lab2cm( b0.p() ).px() + lab2cm( b0.p() ).py()*lab2cm( b0.p() ).py() + lab2cm( b0.p() ).pz()*lab2cm( b0.p() ).pz() ) ) );

                                        t_1->column ( "fl_b0k3pi", flag_b0k3pi( *i, *j, *k, *l ) );

                                        //??
                                        t_1->dumpData();


                                }

                        }

                }

        }

}


// Lorentz transform lab -> cm system ****************************************

HepLorentzVector b0dspi::lab2cm( HepLorentzVector p_lv )
{

        HepLorentzVector
        p_lv_tot( E_HER*sin( THETA ), 0., E_HER*cos( THETA )-E_LER, E_HER+E_LER );

        p_lv.boost( -( p_lv_tot.boostVector() ) );

        return p_lv;

}


// Track selection based on IP position **************************************

int b0dspi::better_charged( const Mdst_charged  &mdst, int imass ) {

        Mdst_trk &trk = mdst.trk();
        Mdst_trk_fit &trk_fit = trk.mhyp( imass );

        // require at least one associated rphi SVD hit
        if ( trk_fit.nhits( 3 ) < 1 ) return 0;

        // compute dr and dz with respect to IP
        HepPoint3D pivot( trk_fit.pivot_x(), trk_fit.pivot_y(), trk_fit.pivot_z() );

        HepVector a( 5, 0 );
        a[0] = trk_fit.helix( 0 );
        a[1] = trk_fit.helix( 1 );
        a[2] = trk_fit.helix( 2 );
        a[3] = trk_fit.helix( 3 );
        a[4] = trk_fit.helix( 4 );


        HepSymMatrix Ea( 5, 0 );
        Ea[0][0] = trk_fit.error( 0 );
        Ea[1][0] = trk_fit.error( 1 );
        Ea[1][1] = trk_fit.error( 2 );
        Ea[2][0] = trk_fit.error( 3 );
        Ea[2][1] = trk_fit.error( 4 );
        Ea[2][2] = trk_fit.error( 5 );
        Ea[3][0] = trk_fit.error( 6 );
        Ea[3][1] = trk_fit.error( 7 );
        Ea[3][2] = trk_fit.error( 8 );
        Ea[3][3] = trk_fit.error( 9 );
        Ea[4][0] = trk_fit.error( 10 );
        Ea[4][1] = trk_fit.error( 11 );
        Ea[4][2] = trk_fit.error( 12 );
        Ea[4][3] = trk_fit.error( 13 );
        Ea[4][4] = trk_fit.error( 14 );

        Helix helix( pivot, a, Ea );
        helix.pivot( IpProfile::position() );

        double dr  = helix.dr();
        double dz  = helix.dz();

        // |dr| < 2.0 cm, |dz| < 4.0 cm
        if ( abs( dr ) > 2. ) return 0;
        if ( abs( dz ) > 4. ) return 0;

        return 1;

}


// MC flags for b0k3pi candidates ********************************************

int b0dspi::flag_b0k3pi( const Mdst_charged &mdst_k,
                         const Mdst_charged &mdst_d0pif,
                         const Mdst_charged &mdst_dstarpis,
                         const Mdst_charged &mdst_b0pif )
{
        if ( !fl_mc ) return 0;

        // correct particles?
        Gen_hepevt &hepevt_k = get_hepevt( mdst_k );
        if ( !hepevt_k ) return 0;
        if ( abs( hepevt_k.idhep() ) != ID_K ) return 0;

        Gen_hepevt &hepevt_d0pif = get_hepevt( mdst_d0pif );
        if ( !hepevt_d0pif ) return 0;
        if ( abs( hepevt_d0pif.idhep() ) != ID_PI ) return 0;

        Gen_hepevt &hepevt_dstarpis = get_hepevt( mdst_dstarpis );
        if ( !hepevt_dstarpis ) return 0;
        if ( abs( hepevt_dstarpis.idhep() ) != ID_PI ) return 0;

        Gen_hepevt &hepevt_b0pif = get_hepevt( mdst_b0pif );
        if ( !hepevt_b0pif ) return 0;
        if ( abs( hepevt_b0pif.idhep() ) != ID_PI ) return 0;


        // kaon from D0?
        Gen_hepevt &hepevt_d0 = hepevt_k.mother();
        if ( !hepevt_d0 ) return 0;
        if ( abs( hepevt_d0.idhep() ) != ID_D0 ) return 0;

        // fast d0pion from same D0?
        if ( hepevt_d0pif.mother() != hepevt_d0 ) return 0;

        // D0 decaying into K- pi+
        Gen_hepevt_Manager& gen_m = Gen_hepevt_Manager::get_manager();
        Gen_hepevt_Index gen_i = gen_m.index( "mother" );
        gen_i.update();
        std::vector<Gen_hepevt> d0_daughters =
                point_from( hepevt_d0.get_ID(), gen_i );
        if ( d0_daughters.size() != 2 ) return 0;


        // D0 from D*?
        Gen_hepevt &hepevt_dstar = hepevt_d0.mother();
        if ( !hepevt_dstar ) return 0;
        if ( abs( hepevt_dstar.idhep() ) != ID_DSTAR ) return 0;

// slow dstarpion from same D*?
        if ( hepevt_dstarpis.mother() != hepevt_dstar ) return 0;


        // D* from B0?
        Gen_hepevt &hepevt_b0 = hepevt_dstar.mother();
        if ( !hepevt_b0 ) return 0;
        if ( abs( hepevt_b0.idhep() ) != ID_B0 ) return 0;


// fast b0pion from same B0?
        if ( hepevt_b0pif.mother() != hepevt_b0 ) return 0;



        // great! we have a b0k3pi event!
        return 1;

}
