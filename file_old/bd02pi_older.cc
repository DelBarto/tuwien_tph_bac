// B+ -> D0_Bar pi+ -> ( K+ pi- ) pi+

#include "belle.h"
#include <math.h>
#include <iostream.h>
#include <vector.h>
#include "probutil/Chisq.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "tuple/BelleTupleManager.h"

#include "event/BelleEvent.h"
#include "basf/module.h"
#include "basf/module_descr.h"
#include "panther/panther.h"
#include BELLETDF_H
#include LEVEL4_H
#include EVTCLS_H
#include MDST_H
#include HEPEVT_H
#include "mdst/mdst.h"

#include "particle/Particle.h"  //supplies you interfaces to various particle information, such as momentum, pid, etc.
#include "particle/utility.h"
#include "kid/atc_pid.h"
#include "ip/IpProfile.h"
#if defined(BELLE_NAMESPACE)
namespace Belle {
#endif


// particle codes
#define ID_PI 211
#define ID_K 321

#define ID_D0 421
//#define ID_DSTAR 413

#define ID_B 521

// beam parameters
#define E_HER 7.9965
#define E_LER 3.5
#define THETA 0.022

// module class **************************************************************
class b2d0pi : public Module {

public:

b2d0pi( void ) {
};
~b2d0pi( void ) {
};
void init( int* );
void term( void ) {
};
void disp_stat( const char* ) {
};
void hist_def( void );
void event( BelleEvent*, int* );
void begin_run( BelleEvent*, int* );
void end_run( BelleEvent*, int* ) {
};
void other( int*, BelleEvent*, int* ) {
};

private:

void count_b2d0pi( void );
void rec_b2d0pi( void );

HepLorentzVector lab2cm( HepLorentzVector );
int better_charged( const Mdst_charged&, int );
int flag_b2d0pi( const Mdst_charged&, const Mdst_charged&,
                 const Mdst_charged& );

// "global variables"

// event()
int exp_no, run_no, crate_no, evt_no, fl_mc;

int n_bc;

// histogram and ntuple
BelleTuple *t_1;
BelleHistogram *h_1, *h_2, *h_3;

};

extern "C" Module_descr *mdcl_b2d0pi() {

        b2d0pi *module = new b2d0pi;
        Module_descr *dscr = new Module_descr ( "b2d0pi", module );
        return dscr;

}

// initialization ************************************************************
void b2d0pi::init( int *status ) {

        // dummy construction for Ptype initialization
        Ptype ptype_dummy("VPHO"); // needed for access to particle data

        cout << endl;
        cout << "b2d0pi: module b2d0pi initialized" << endl;
        cout << endl;

}

// histogram and ntuple definition *******************************************
void b2d0pi::hist_def( void ) {

        extern BelleTupleManager *BASF_Histogram;

        t_1 = BASF_Histogram->ntuple( "b2d0pi",
                                      "exp_no run_no crate_no evt_no "
                                      "ps_k id_k ps_pif id_pif m_kpi prb_vtx cos_d "
                                      "ps_pis id_pis delta_m "
                                      "fl_b2d0pi" );

        // event()
        h_1 = BASF_Histogram->histogram( "no. hadronb", 1, 0., 1.);
        h_2 = BASF_Histogram->histogram( "fix_mdst", 3, -1., 2.);

        // count_b2d0pi
        h_3 = BASF_Histogram->histogram( "no. b2d0pi", 1, 0., 1.);

}

// event function ************************************************************
void b2d0pi::event( BelleEvent *evptr, int *status ) {

        // event information
        Belle_event_Manager &event_m = Belle_event_Manager::get_manager();
        Belle_event &event = event_m( ( Panther_ID ) 1 );
        if ( !event ) return;

        exp_no = event.ExpNo();
        run_no = event.RunNo();

        // event number is stored in Belle_event table as combination
        // of farm number and event number, event number is the 28 LSBs
        // and farm number is to the "left" of this
        int raw_evt_no = event.EvtNo();
        crate_no = raw_evt_no >> 28;
        evt_no = raw_evt_no & ~(~0 << 28);

        fl_mc = ( event.ExpMC() == 2 );

        // for MC: check trigger level 4 condition
        if ( fl_mc ) {
                L4_summary_Manager& l4_m = L4_summary_Manager::get_manager();
                L4_summary &l4 = l4_m( ( Panther_ID ) 1 );
                if ( !l4 ) return;

                if( l4.type() == 0 ) return;
        }

        // check HadronB condition
        Evtcls_hadronic_flag_Manager& evtcls_m = Evtcls_hadronic_flag_Manager::get_manager();
        Evtcls_hadronic_flag &evtcls = evtcls_m( ( Panther_ID ) 1 );
        if ( !evtcls ) return;

        if ( evtcls.hadronic_flag(2) != 10 && evtcls.hadronic_flag(2) != 20 )
                return;

        h_1->accumulate( 0.5, 1. );
        h_2->accumulate( *status+0.5, 1. );

        // count generated B+ -> D_0 pi+, D_0 -> K+ pi-
        count_b2d0pi();

        // b2d0pi reconstruction
        rec_b2d0pi();

}

// begin of run **************************************************************
void b2d0pi::begin_run( BelleEvent *evptr, int *status ) {

        // get IP profile data
        IpProfile::begin_run();
        //  IpProfile::dump();

}

// count generated events ****************************************************
void b2d0pi::count_b2d0pi( void ) {

        if ( !fl_mc ) return;

        Gen_hepevt_Manager& gen_m = Gen_hepevt_Manager::get_manager();

        for ( std::vector<Gen_hepevt>::iterator i = gen_m.begin(); i != gen_m.end(); ++i ) {

                //# look for B+ meson
                if ( i->idhep() != ID_B ) continue;
                Gen_hepevt& hepevt_b = *i;
                //# if ( hepevt_b.idhep() != ID_B ) continue;

                //# scan B+ daughters
                Gen_hepevt_Index gen_i = gen_m.index("mother");
                gen_i.update();
                std::vector<Gen_hepevt> b_daughters = point_from( hepevt_b.get_ID(), gen_i );
                if ( b_daughters.size() != 2 ) continue;

                Gen_hepevt *pt_d0, *pt_pip;
                pt_d0 = pt_pip = 0;

                for ( std::vector<Gen_hepevt>::iterator j = b_daughters.begin(); j != b_daughters.end(); j++ ) {

                        int id = j->idhep();

                        // look for D0_Bar
                        if ( id == (-ID_D0) ) pt_d0 = &( *j );

                        // look for pion+
                        if ( id == ID_PI ) pt_pip = &( *j );
                }

                if ( pt_d0 == 0 || pt_pip == 0 ) continue;
                Gen_hepevt& hepevt_d0 = *pt_d0;
                Gen_hepevt& hepevt_pip = *pt_pip;

                // scan D0_Bar daughters
                std::vector<Gen_hepevt> d0_daughters = point_from( hepevt_d0.get_ID(), gen_i );
                if ( d0_daughters.size() != 2 ) continue;

                Gen_hepevt *pt_k, *pt_pim;
                pt_k = pt_pim = 0;

                for ( std::vector<Gen_hepevt>::iterator j = d0_daughters.begin(); j != d0_daughters.end(); j++ ) {
                        int id = j->idhep();

                        // look for Kaon+
                        if ( id == ID_K ) pt_k = &( *j );

                        // look for Pion-
                        if ( id == (-ID_PI) ) pt_pim = &( *j );
                }

                if ( pt_k == 0 || pt_pim == 0 ) continue;

                // great! we have a b2d0pi event!
                h_3->accumulate( 0.5, 1. );
        }
}

// b2d0pi reconstruction and ntuple filling **********************************
void b2d0pi::rec_b2d0pi( void ) {

        Mdst_charged_Manager &charged_m = Mdst_charged_Manager::get_manager();


        // loop over kaons #(loop over all particle)
        for( std::vector<Mdst_charged>::iterator i = charged_m.begin(); i != charged_m.end(); i++ ) {

                // track selection
                if ( !better_charged( *i, 3 ) ) continue;

                // kaon identification

                // accq =  3 Probability is calculated based on measured Npe and PDF
                // tofq =  1 Used with checking Z hit position w.r.t the track extrapol.
                // cdcq =  5 Correct dE/dx run-dependent shift in 2001 summer reprocesing
                atc_pid selKpi(3,1,5,3,2);

                //# mass Kaon 0.493 GeV
                //# mass Pion 0.14 GeV
                double id_k = selKpi.prob ( *i );
                if ( id_k < 0.6 ) continue;

                if ( i->charge() < 0. ) continue;
                Particle kaon;
                kaon = Particle( *i, ( std::string )"K+" );

                // loop over fast pions
                for( std::vector<Mdst_charged>::iterator j = charged_m.begin(); j != charged_m.end(); j++ ) {

                        //fast exit if other (i) and inner (j) particle are the same
                        if ( j == i ) continue;

                        // right sign combinations only
                        //if ( i->charge() * j->charge() > 0. ) continue;
                        //instead of looking for the right sign just looking for pi- because we accept only K+
                        //looking for piom- -> -1e [C]
                        if ( j->charge() > 0. ) continue;

                        // track selection
                        if ( !better_charged( *j, 2 ) ) continue;

                        // kaon rejection
                        //# mass Kaon 0.493 GeV
                        //# mass Pion 0.14 GeV
                        double id_pif = selKpi.prob ( *j );
                        if ( id_pif > 0.9 ) continue;

                        // cout << endl;
                        // cout << "charge of j " << j->charge() << endl;
                        // cout << endl;

                        Particle pion_fast;
                        if ( j->charge() > 0. ) {
                                //pion_fast = Particle( *j, ( std::string ) "PI+" );
                                continue;
                        } else {
                                pion_fast = Particle( *j, ( std::string ) "PI-" );
                        }

                        // D0 reconstruction, 1.76 < m( K- pi+ ) 1.96 GeV/c2
                        HepLorentzVector p_lv_d0 = kaon.p() + pion_fast.p();

                        //Ptype ptype_d0 = Ptype( "D0" );
                        Ptype ptype_d0_bar = Ptype( "D0B" );
                        Particle d0;
                        if ( kaon.charge() < 0. ) {
                                continue;
                                //d0 = Particle( p_lv_d0, ptype_d0 );
                        } else {
                                d0 = Particle( p_lv_d0, ptype_d0_bar );
                        }


                        if ( d0.p().mag() < 1.76 || d0.p().mag() > 1.96 ) continue;

                        // fit charged tracks to a common vertex
                        kvertexfitter vtx;
                        addTrack2fit( vtx, kaon );
                        addTrack2fit( vtx, pion_fast );
                        unsigned err = vtx.fit();
                        if ( err != 0 ) continue;
                        double prb_vtx = Chisq::Prob( vtx.chisq(), vtx.dgf() );

                        // compute D0 decay angle
                        HepLorentzVector p_lv = kaon.p();
                        //# boost kaon into CMS of d0 (D0_Bar)
                        p_lv.boost( -( d0.p().boostVector() ) );
                        double cos_d = ( p_lv.vect()*d0.p3() )/( p_lv.vect().mag()*d0.p3().mag() );

                        // loop over slow pions
                        for( std::vector<Mdst_charged>::iterator k = charged_m.begin(); k != charged_m.end(); k++ ) {

                                if ( k == i || k == j ) continue;

                                // right sign combinations only
                                //if ( i->charge() * k->charge() > 0. ) continue;
                                //# pi+ has a positiv charge
                                if ( k->charge() < 0. ) continue;

                                // track selection
                                // if ( !better_charged( *k, 2 ) ) continue;

                                // kaon rejection
                                double id_pis = selKpi.prob( *k );
                                if ( id_pis > 0.9 ) continue;

                                Particle pion_slow;
                                if ( k->charge() > 0. ) {
                                        pion_slow = Particle( *k, ( std::string ) "PI+" );
                                } else {
                                        continue;
                                        //# pion_slow = Particle( *k, ( std::string ) "PI-" );
                                }

                                // D* reconstruction, Delta m < 250 MeV
                                //HepLorentzVector p_lv_dstar = d0.p() + pion_slow.p();

                                //# B+ reconstruction, Delta m <
                                HepLorentzVector p_lv_b = d0.p() + pion_slow.p();
                                Ptype ptype_b_plus = Ptype( "B+" );
                                Particle b;


                                if (pion_slow.charge() > 0.) {
                                        b = Particle( p_lv_b, ptype_b_plus );
                                }else continue;

                                // Ptype ptype_dstar_plus = Ptype( "D*+" );
                                // Ptype ptype_dstar_minus = Ptype( "D*-" );
                                // Particle dstar;
                                // if ( pion_slow.charge() > 0. ) {
                                //         dstar = Particle( p_lv_dstar, ptype_dstar_plus );
                                // } else {
                                //         dstar = Particle( p_lv_dstar, ptype_dstar_minus );
                                // }
                                //if ( dstar.p().mag() - d0.p().mag() > .25 ) continue;

                                //if (b.p().mag() - d0.p().mag() > ??? ) continue;

                                //if (b.p().mag()**2 + b.p3().mag()**2 - ) continue;
                                if (b.p().mag() - d0.p().mag() > .25) continue;

                                // cout << endl;
                                // cout << "got an event" << endl;
                                // cout << endl;

                                // fill ntuple
                                t_1->column ( "exp_no", exp_no );
                                t_1->column ( "run_no", run_no );
                                t_1->column ( "crate_no", crate_no );
                                t_1->column ( "evt_no", evt_no );

                                // rho() equivalent to vect().mag()
                                t_1->column ( "ps_k", lab2cm( kaon.p() ).rho() );
                                t_1->column ( "id_k", id_k );
                                t_1->column ( "ps_pif", lab2cm( pion_fast.p() ).rho() );
                                t_1->column ( "id_pif", id_pif );
                                t_1->column ( "m_kpi", d0.p().mag() );
                                t_1->column ( "prb_vtx", prb_vtx );
                                t_1->column ( "cos_d", cos_d );

                                t_1->column ( "ps_pis", lab2cm( pion_slow.p() ).rho() );
                                t_1->column ( "id_pis", id_pis );
                                //# t_1->column ( "delta_m", dstar.p().mag() - d0.p().mag() );
                                //# t_1->column ( "delta_m", b.p().mag() - d0.p().mag() );
                                t_1->column ( "delta_m", 5.29 - b.p().mag() );

                                t_1->column ( "fl_b2d0pi", flag_b2d0pi( *i, *j, *k ) );

                                t_1->dumpData();
                        }
                }
        }

}

// Lorentz transform lab -> cm system ****************************************
HepLorentzVector b2d0pi::lab2cm( HepLorentzVector p_lv ) {

        HepLorentzVector
        p_lv_tot( E_HER*sin( THETA ), 0., E_HER*cos( THETA )-E_LER, E_HER+E_LER );

        p_lv.boost( -( p_lv_tot.boostVector() ) );

        return p_lv;

}

// Track selection based on IP position **************************************
int b2d0pi::better_charged( const Mdst_charged  &mdst, int imass ) {

        Mdst_trk &trk = mdst.trk();
        Mdst_trk_fit &trk_fit = trk.mhyp( imass );

        // require at least one associated rphi SVD hit
        if ( trk_fit.nhits( 3 ) < 1 ) return 0;

        // compute dr and dz with respect to IP
        HepPoint3D pivot( trk_fit.pivot_x(), trk_fit.pivot_y(), trk_fit.pivot_z() );

        HepVector a( 5, 0 );
        a[0] = trk_fit.helix( 0 );
        a[1] = trk_fit.helix( 1 );
        a[2] = trk_fit.helix( 2 );
        a[3] = trk_fit.helix( 3 );
        a[4] = trk_fit.helix( 4 );

        HepSymMatrix Ea( 5, 0 );
        Ea[0][0] = trk_fit.error( 0 );
        Ea[1][0] = trk_fit.error( 1 );
        Ea[1][1] = trk_fit.error( 2 );
        Ea[2][0] = trk_fit.error( 3 );
        Ea[2][1] = trk_fit.error( 4 );
        Ea[2][2] = trk_fit.error( 5 );
        Ea[3][0] = trk_fit.error( 6 );
        Ea[3][1] = trk_fit.error( 7 );
        Ea[3][2] = trk_fit.error( 8 );
        Ea[3][3] = trk_fit.error( 9 );
        Ea[4][0] = trk_fit.error( 10 );
        Ea[4][1] = trk_fit.error( 11 );
        Ea[4][2] = trk_fit.error( 12 );
        Ea[4][3] = trk_fit.error( 13 );
        Ea[4][4] = trk_fit.error( 14 );

        Helix helix( pivot, a, Ea );
        helix.pivot( IpProfile::position() );

        double dr  = helix.dr();
        double dz  = helix.dz();

        // |dr| < 2.0 cm, |dz| < 4.0 cm
        if ( abs( dr ) > 2. ) return 0;
        if ( abs( dz ) > 4. ) return 0;

        return 1;

}

// MC flags for b2d0pi candidates ********************************************
int b2d0pi::flag_b2d0pi( const Mdst_charged &mdst_k,
                         const Mdst_charged &mdst_pim,
                         const Mdst_charged &mdst_pip ) {

        if ( !fl_mc ) return 0;

        // correct particles?
        const Gen_hepevt &hepevt_k = get_hepevt( mdst_k );
        if ( !hepevt_k ) return 0;
        if ( hepevt_k.idhep() != ID_K ) return 0;

        const Gen_hepevt &hepevt_pim = get_hepevt( mdst_pim );
        if ( !hepevt_pim ) return 0;
        if ( hepevt_pim.idhep() != -ID_PI ) return 0;

        const Gen_hepevt &hepevt_pip = get_hepevt( mdst_pip );
        if ( !hepevt_pip ) return 0;
        if ( hepevt_pip.idhep() != ID_PI ) return 0;

        // kaon from D0?
        Gen_hepevt &hepevt_d0 = hepevt_k.mother();
        if ( !hepevt_d0 ) return 0;
        if ( hepevt_d0.idhep() != ID_D0 ) return 0;

        // fast pion from same D0?
        if ( hepevt_pim.mother() != hepevt_d0 ) return 0;

        // D0 decaying into K- pi+
        Gen_hepevt_Manager& gen_m = Gen_hepevt_Manager::get_manager();
        Gen_hepevt_Index gen_i = gen_m.index( "mother" );
        gen_i.update();
        std::vector<Gen_hepevt> d0_daughters = point_from( hepevt_d0.get_ID(), gen_i );
        if ( d0_daughters.size() != 2 ) return 0;

        // D0 from D*?
        //# Gen_hepevt &hepevt_dstar = hepevt_d0.mother();

        //# D0 from B+?
        Gen_hepevt &hepevt_b = hepevt_d0.mother();
        if ( !hepevt_b ) return 0;
        if ( hepevt_b.idhep() != ID_B ) return 0;

        // slow pion from D*?
        //# if ( hepevt_pip.mother() != hepevt_dstar ) return 0;

        //# slow pion from B+?
        if ( hepevt_pip.mother() != hepevt_b ) return 0;

        // great! we have a b2d0pi event!
        return 1;

}

#if defined(BELLE_NAMESPACE)
} // namespace Belle
#endif
