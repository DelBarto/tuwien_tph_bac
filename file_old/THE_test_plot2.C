#include <iostream>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "TFile.h"
#include "TTree.h"

using namespace RooFit;

void THE_test_plot2()
{
  // Import data
  TFile *f1 = new TFile("./bdata/evt_exp55_s1s2.root");
  TTree *tr1 = (TTree*) f1->Get("h1");

  TFile *f2 = new TFile("./bdata/evt_exp55_s0.root");
  TTree *tr2 = (TTree*) f2->Get("h1");

  //Declare Variables
  RooRealVar m_bc("m_bc","m_{bc} (GeV/c^2)",5.25,5.29);
  RooRealVar delta_e("delta_e","delta_e",-0.5,0.5) ;
  RooRealVar fl_bd02p("fl_bd02p","fl_bd02p",0.,1.);

  //Create  Data - Sets
  RooDataSet mc("mc","mc",tr1,RooArgList(m_bc,delta_e,fl_bd02p));
  RooDataSet* mc_sig = (RooDataSet*) mc.reduce("(m_bc > 5.25) && abs(delta_e)<0.15 && (fl_bd02p == 1)");
  RooDataSet* mc_bkg = (RooDataSet*) mc.reduce("(m_bc > 5.25) && abs(delta_e)<0.15 && (fl_bd02p == 0)");
  Int_t mc_entries = mc.numEntries();

  RooDataSet data("data","data",tr2,RooArgList(m_bc,delta_e,fl_bd02p));
  RooDataSet* data_sig = (RooDataSet*) data.reduce("(m_bc > 5.25) && abs(delta_e)<0.15 && (fl_bd02p == 1)");
  RooDataSet* data_all = (RooDataSet*) data.reduce("(m_bc > 5.25) && abs(delta_e)<0.15");
  Int_t data_entries = data.numEntries();

  // Declare Parameters
  RooRealVar sig_gauss_mean("sig_gauss_mean","sig_gauss_mean",5.28,5.27,5.29);
  RooRealVar sig_gauss_width("sig_gauss_width","sig_gauss_width",0.02,0.001,1.);
  RooRealVar bkg_arg_par("bkg_arg_par","bkg_arg_par",-20.,-100.,-1.);
  RooRealVar bkg_gauss_width("bkg_gauss_width","bkg_gauss_width",0.02,0.001,1.);

  RooRealVar nevt1("nevt1","#events",500,0.,mc_entries);
  RooRealVar nevt2("nevt2","#events",500,0.,mc_entries);
  RooRealVar par1("par1","weight parameter bkg peak",0.5,0.,1.);


  RooRealVar nsig("nsig","#signal events",1100,0.,data_entries);
  RooRealVar nbkg("nbkg","#background events",800,0.,data_entries);

  ///////////////////////////////////////////////////////////////
  // Signal PDF
  RooGaussian sig_gauss("sig_gauss","gaussian PDF",m_bc,sig_gauss_mean,sig_gauss_width);
  RooAddPdf signal("signal","signal model",sig_gauss,nevt1);

  signal.fitTo(*mc_sig);

  RooPlot *mframe1 = m_bc.frame();
  mframe1->SetTitle("Signal pdf fit");
  mc_sig->plotOn(mframe1);
  signal.plotOn(mframe1,LineColor(kBlue));
  TCanvas can1;
  mframe1->Draw();
  can1.SaveAs("signal.eps");

  //Fix Parameters
  sig_gauss_mean.setConstant(kTRUE);
  sig_gauss_width.setConstant(kTRUE);

  // Background PDF
  RooArgusBG bkg_argus("bkg_argus","argus PDF",m_bc,RooConst(5.29),bkg_arg_par) ;
  RooGaussian bkg_gauss("bkg_gauss","bkg gaussian PDF",m_bc,sig_gauss_mean,bkg_gauss_width);
  //RooAddPdf background("background","background model",bkg_argus,nevt2);
  RooAddModel background("background","background model",RooArgList(bkg_argus,bkg_gauss),par1);

  background.fitTo(*mc_bkg);

  RooPlot *mframe2 = m_bc.frame();
  mframe2->SetTitle("Background pdf fit");
  mc_bkg->plotOn(mframe2);
  background.plotOn(mframe2,LineColor(kBlue));
  TCanvas can2;
  mframe2->Draw();
  can2.SaveAs("background.eps");

  //Fix Parameters
  bkg_arg_par.setConstant(kTRUE);
  bkg_gauss_width.setConstant(kTRUE);
  par1.setConstant(kTRUE);

  ///////////////////////////////////////////////////////
  //Construct & Fit total PDF
  RooAddPdf model("model","data model",RooArgList(sig_gauss,background),RooArgList(nsig,nbkg));

  model.fitTo(*data_all);

  RooPlot *mframe3 = m_bc.frame();
  mframe3->SetTitle("Data fit");
  data_all->plotOn(mframe3);
  model.plotOn(mframe3,LineColor(kBlue));
  model.plotOn(mframe3,Components(background),LineStyle(kDashed));
  TCanvas can3;
  mframe3->Draw();
  can3.SaveAs("model.eps");

  cout << "Fit result" << endl;
  nsig.Print();
  nbkg.Print();
  sig_gauss_mean.Print();
  sig_gauss_width.Print();
  bkg_arg_par.Print();
  bkg_gauss_width.Print();
  par1.Print();
  cout << "N_true = " << data_sig->numEntries() << endl;
}
