#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TFile.h"
#include "TTree.h"
#include "RooArgList.h"

#include <iostream>
#include <fstream>

using namespace RooFit ;

void bd02pi()
{
  //read in file
  TFile* inFile1 = new TFile("bdata/evt_exp55_s1_s2.root");
  TFile* inFile2 = new TFile("bdata/evt_exp55_s0.root");

  RooRealVar m_bc("m_bc","m_bc",5.2,5.29,"GeV") ;
  RooRealVar delta_e("delta_e","delta_e",-0.2,0.2 ,"") ;
  RooRealVar fl_bd02p("fl_bd02p","fl_bd02p",0,1,"") ;

  TTree* tree1 = (TTree*) inFile1->Get("h1");
  TTree* tree2 = (TTree*) inFile1->Get("h1");

  RooDataSet* data1 = new RooDataSet("data", "dataset", tree1, RooArgSet(m_bc,delta_e,fl_bd02p));
  RooDataSet* nData = (RooDataSet*) data1->reduce("(abs(delta_e) < 0.05)");
  RooDataSet* nDataSig = (RooDataSet*) data1->reduce("(abs(delta_e) < 0.05) && m_bc > 5.27 && (fl_bd02p == 1)");
  RooDataSet* nDataBg = (RooDataSet*) data1->reduce("(abs(delta_e) < 0.05) && (fl_bd02p == 0)");

  RooDataSet* data2 = new RooDataSet("data", "dataset", tree1, RooArgSet(m_bc,delta_e,fl_bd02p));
  RooDataSet* nAll = (RooDataSet*) data2->reduce("(abs(delta_e) < 0.05)");

  Int_t nbr_data2 = data2.numEntries();


  //################# Signal #################
  m_bc.setMin(5.27) ;

  // --- Build Gaussian PDF ---
  RooRealVar meanSig("meanSig","B^{#pm} mass",5.279,5.20,5.29) ;
  RooRealVar sigmSig1("sigmSig1","B^{#pm} width",0.0027,0.001,0.2) ;
  RooRealVar sigmSig2("sigmSig2","B^{#pm} width",0.01,0.001,0.1) ;
  RooRealVar nSig1("nSig1","#signal1 events",200,0.,10000) ;
  RooRealVar nSig2("nsig2","#signal2 events",100,0.,10000) ;
  RooRealVar weiSig("weiSig","weight of the doubleGauss",0.5,0.,1.) ;

  RooGaussian sig1("sig1","gaussian signal1",m_bc,meanSig,sigmSig1);
  RooGaussian sig2("sig2","gaussian signal2",m_bc,meanSig,sigmSig2);

  // RooAddPdf modSig("modSig","Model for Signal",RooArgList(sig1,sig2),RooArgList(nSig1,nSig2)) ;
  RooAddModel modSig("modSig","Model for Signal",RooArgList(sig1,sig2), weiSig) ;

  modSig.fitTo(*nDataSig, Extended());

  meanSig.setConstant(kTRUE);
  sigmSig1.setConstant(kTRUE);
  sigmSig2.setConstant(kTRUE);
  weiSig.setConstant(kTRUE);

  //################# Background #################
  m_bc.setMin(5.20) ;

  // --- Build Gaussian PDF ---
  RooRealVar weiBg("weiBg","weight parameter arg gaus",0.5,0.,1.);
  RooRealVar meanBg("meanBg","B^{#pm} mass",5.279,5.20,5.29) ;
  RooRealVar sigmBg("sigmBg","B^{#pm} width",0.0027,0.001,0.1) ;
  RooRealVar nBg1("nBg1","#background1 events",500,0.,10000) ;
  RooGaussian bg1("bg1","gaussian background",m_bc,meanBg,sigmBg);

  // --- Build Argus background PDF ---
  RooRealVar argpar("argpar","argus shape parameter",-50.0,-100.,-1.) ;
  RooRealVar nBg2("nBg2","#background2 events",700,0.,10000) ;

  RooArgusBG bg2("bg2","Argus background",m_bc,RooConst(5.29),argpar) ;

  // RooAddModel modBg("modBg","Model for Background",RooArgList(bg1,bg2),RooArgList(nBg1,nBg2)) ;
  RooAddModel modBg("modBg","Model for Background",RooArgList(bg1,bg2),weiBg) ;

  modBg.fitTo(*nDataBg, Extended());

  sigmBg.setConstant(kTRUE);
  meanBg.setConstant(kTRUE);
  weiBg.setConstant(kTRUE);


  //################# Signal + Background #################
  // RooRealVar meanS("meanSig","B^{#pm} mass",5.279,5.20,5.29) ;
  // RooRealVar sigmS1("sigmSig1","B^{#pm} width",0.0027,0.001,0.2) ;
  // RooRealVar sigmS2("sigmSig2","B^{#pm} width",0.01,0.001,0.1) ;

  RooRealVar nS("nSig1","#signal1 events",200,0.,nbr_data2) ;
  RooRealVar nB("nsig2","#signal2 events",100,0.,nbr_data2) ;

  RooGaussian s1("sig1","gaussian signal1",m_bc,meanSig,sigmSig1);
  RooGaussian s2("sig2","gaussian signal2",m_bc,meanSig,sigmSig2);

  // --- Build Gaussian PDF ---
  // RooRealVar meanB("meanBg","B^{#pm} mass",5.279,5.20,5.29) ;
  // RooRealVar sigmB("sigmBg","B^{#pm} width",0.0027,0.001,0.1) ;
  // meanB.setVal(meanBg.getVal());
  // sigmB.setVal(sigmBg.getVal());

  // --- Build Argus background PDF ---
  // RooRealVar parB("argpar","argus shape parameter",-50.0,-100.,-1.) ;
  // RooRealVar nB2("nBg2","#background2 events",100,0.,10000) ;
  // parB.setVal(argpar.getVal());
  argpar.setConstant(kTRUE);

  RooGaussian b1("bg1","gaussian background",m_bc,meanBg,sigmBg);
  RooArgusBG b2("bg2","Argus background",m_bc,RooConst(5.29),argpar) ;



  RooAddPdf modAll("modAll","Model for Data",RooArgList(s1,s2,b1,b2),RooArgList(nS,nB)) ;

  modAll.fitTo(*nAll, Extended());




  //################# Draw #################
  m_bc.setMin(5.20);
  RooPlot *frame1 = m_bc.frame() ;
  RooPlot *frame2 = m_bc.frame() ;
  RooPlot *frame3 = m_bc.frame() ;
  frame1->SetTitle("Siganl");
  frame2->SetTitle("Background");
  frame3->SetTitle("Signal + Background");

  nDataSig->plotOn(frame1);
  modSig.plotOn(frame1);

  nDataBg->plotOn(frame2);
  modBg.plotOn(frame2);

  nAll->plotOn(frame3);
  modAll.plotOn(frame3);

  // ofstream myfile;
  // myfile.open ("data.txt");
  // myfile << "#### Signal ####\n";
  // myfile << "meanSig: " << meanSig.getVal() << "\n";
  // myfile << "sigmSig1: " << sigmSig1.getVal() << "\n";
  // myfile << "sigmSig2: " << sigmSig2.getVal() << "\n";
  // myfile << "nSig1: " << nSig1.getVal() << "\n";
  // myfile << "nSig2: " << nSig2.getVal() << "\n";
  //
  // myfile << "\n";
  // myfile << "nSig: " << nSig1.getVal() + nSig2.getVal() << "\n";
  // myfile << "\n";
  //
  // myfile << "#### Background ####\n";
  // myfile << "meanBg: " << meanBg.getVal() << "\n";
  // myfile << "sigmBg: " << sigmBg.getVal() << "\n";
  // myfile << "nBg1: " << nBg1.getVal() << "\n";
  // myfile << "argpar: " << argpar.getVal() << "\n";
  // myfile << "nBg2: " << nBg2.getVal() << "\n";
  //
  // myfile << "\n";
  // myfile << "nBg: " << nBg1.getVal() + nBg2.getVal() << "\n";
  // myfile << "\n";
  //
  // myfile << "#### Signal + Background #### \n";
  // myfile << "meanS: " << meanS.getVal() << "\n";
  // myfile << "sigmS1: " << sigmS1.getVal() << "\n";
  // myfile << "sigmS2: " << sigmS2.getVal() << "\n";
  // myfile << "nS1: " << nS1.getVal() << "\n";
  // myfile << "nS2: " << nS2.getVal() << "\n";
  // myfile << "meanB: " << meanB.getVal() << "\n";
  // myfile << "sigmB: " << sigmB.getVal() << "\n";
  // // myfile << "nB1: " << nB1.getVal() << "\n";
  // myfile << "parB: " << parB.getVal() << "\n";
  // // myfile << "nB2: " << nB2.getVal() << "\n";

  // myfile << "\n";
  // myfile << "nS: " << nS1.getVal() + nS2.getVal() << "\n";
  // myfile << "nB: " << nB1.getVal() + nB2.getVal() << "\n";
  // myfile << "n: " << nS1.getVal() + nS2.getVal() + nB1.getVal() + nB2.getVal() << "\n";
  // myfile << "\n";

  // myfile << "####################\n";
  //
  // myfile.close();


  TCanvas* c = new TCanvas("rf101_basics","rf101_basics",1500,500) ;
  c->Divide(3) ;
  c->cd(1) ; gPad->SetLeftMargin(0.15) ; frame1->GetYaxis()->SetTitleOffset(1.6) ; frame1->Draw() ;
  c->cd(2) ; gPad->SetLeftMargin(0.15) ; frame2->GetYaxis()->SetTitleOffset(1.6) ; frame2->Draw() ;
  c->cd(3) ; gPad->SetLeftMargin(0.15) ; frame2->GetYaxis()->SetTitleOffset(1.6) ; frame3->Draw() ;
  c->SaveAs("fit.eps");

}
