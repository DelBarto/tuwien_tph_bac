#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TFile.h"
#include "TTree.h"
#include "RooArgList.h"

#include "RooLandau.h"
#include "RooFFTConvPdf.h"
#include "TH1.h"
#include "TF1.h"
#include "TMath.h"
#include "Math/DistFunc.h"
#include "RooTFnBinding.h"

#include <iostream>
#include <fstream>

using namespace RooFit;



void test_fit()
{

        int xmax = 4;
        int bins = 10;

        RooRealVar x("x","x",-xmax,xmax) ;

        RooPlot* frame1 = x.frame();
        TF1 *f1 = new TF1("gs1","gaus",0.0,1.0);
        f1->SetParameters(1, 0.5, 0.1);
        // f1->Draw();
        // TF1 *f1 = new TF1("f1","theg",-xmax,xmax);
        // f1->SetParameters(1,0,1);
        // f1->Print();
        RooAbsReal* rf1 = bindFunction(f1,x) ;
        rf1->plotOn(frame1,Norm(1));
        // rf1->Draw();

        // TF1 *f1 = new TF1("f1","[0]*1/sqrt(2*pi)*exp(-(x**2)/2)",-xmax,xmax);
        // f1->SetParameter(0,1);
        // f1->Print();
        // f1->Print();
        // RooAbsReal* rf1 = bindFunction(f1,x) ;

        // TF1 *f2 = new TF1("f2","1/sqrt(2*pi)*exp(-(x**2)/2)",-xmax,xmax);
        // RooAbsReal* rf2 = bindFunction(f2,x) ;

        // TH1F *h1=new TH1F("h1","test",50,-xmax,xmax);
        // h1->FillRandom("f2",1000);

        // TH1D* h2 = (TH1D*) (h1->Clone("h2"));
        // Double_t norm = h1->GetEntries();
        // h2->Scale(1/norm);

        // RooDataHist *dathist=new RooDataHist("dathist","data histogram",x,h1);
        // RooDataHist *dath1 = new RooDataHist("dath1","scaled data histogram",x,h2);


        // TF1 *fit = (TF1 *) h1->GetFunction("gaus");
        // fit->Print();
        // Double_t p0 = f1->GetParameter(0);

        // cout << fit->GetParameter(0) << endl;

        // dath1->plotOn(frame1);
        // rf1->plotOn(frame1);
        // rf2->plotOn(frame1);
        // cout << p0 <<endl;

        frame1->Draw();



}
