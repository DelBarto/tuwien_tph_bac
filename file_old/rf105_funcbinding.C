#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "TMath.h"
#include "TF1.h"
#include "Math/DistFunc.h"
#include "RooTFnBinding.h"
using namespace RooFit;
void rf105_funcbinding()
{
   // B i n d   T M a t h : : E r f   C   f u n c t i o n
   // ---------------------------------------------------
   // Bind one-dimensional TMath::Erf function as RooAbsReal function
   // B i n d   R O O T   T F 1   a s   R o o F i t   f u n c t i o n
   // ---------------------------------------------------------------
   // Create a ROOT TF1 function
   TF1 *fa1 = new TF1("fa1","1/sqrt(2*pi)*exp(-(x*x)/2)",0,10);
   // Create an observable
   RooRealVar x("x","x",-4,4) ;
   // Create binding of TF1 object to above observable
   RooAbsReal* rfa1 = bindFunction(fa1,x) ;
   // Print rfa1 definition
   rfa1->Print() ;
   // Make plot frame in observable, plot TF1 binding function
   RooPlot* frame = x.frame(Title("TF1 bound as RooFit function")) ;
   rfa1->plotOn(frame) ;
   TCanvas* c = new TCanvas("rf105_funcbinding","rf105_funcbinding",400,400) ;
   frame->Draw() ;
}
