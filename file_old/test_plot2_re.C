#include <iostream>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "TFile.h"
#include "TTree.h"

using namespace RooFit; 

void fit_real()
{
  // Import data
  TFile *f1 = new TFile("real_data55.root");
  TTree *tr1 = (TTree*) f1->Get("h1");

  //Declare Variables
  RooRealVar m_bc("m_bc","m_{bc} (GeV/c^2)",5.2,5.29);  
  RooRealVar delta_e("delta_e","delta_e",-0.5,0.5) ;
  RooRealVar fl_b_k2m("fl_b_k2m","fl_b_k2m",0.,1.);

  //Create  Data - Sets
  RooDataSet data("data","data",tr1,RooArgList(m_bc,delta_e,fl_b_k2m));
  RooDataSet* data_cut = (RooDataSet*) data.reduce("(m_bc > 5.2) && abs(delta_e)<0.2");
  Int_t data_entries = data.numEntries();

  // Declare Parameters 
  RooRealVar sig_gauss_mean("sig_gauss_mean","sig_gauss_mean",5.28035,5.27,5.29);
  RooRealVar sig_gauss_width("sig_gauss_width","sig_gauss_width",0.00275,0.001,1.);
  RooRealVar bkg_arg_par("bkg_arg_par","bkg_arg_par",-13.36109,-100.,-1.);
  RooRealVar bkg_gauss_width("bkg_gauss_width","bkg_gauss_width",0.004438,0.001,1.);
  RooRealVar par1("par1","weight parameter bkg peak",0.496982,0.,1.);

  //Fix Parameters
  sig_gauss_mean.setConstant(kTRUE);
  sig_gauss_width.setConstant(kTRUE);
  bkg_arg_par.setConstant(kTRUE);
  bkg_gauss_width.setConstant(kTRUE);
  par1.setConstant(kTRUE);


  RooRealVar nevt1("nevt1","#events",500,0.,data_entries); 
  RooRealVar nevt2("nevt2","#events",500,0.,data_entries);

  RooRealVar nsig("nsig","#signal events",1100,0.,data_entries); 
  RooRealVar nbkg("nbkg","#background events",800,0.,data_entries);

  ///////////////////////////////////////////////////////////////
  // Signal PDF
  RooGaussian sig_gauss("sig_gauss","gaussian PDF",m_bc,sig_gauss_mean,sig_gauss_width);
  RooAddPdf signal("signal","signal model",sig_gauss,nevt1);


  // Background PDF
  RooArgusBG bkg_argus("bkg_argus","argus PDF",m_bc,RooConst(5.29),bkg_arg_par) ;
  RooGaussian bkg_gauss("bkg_gauss","bkg gaussian PDF",m_bc,sig_gauss_mean,bkg_gauss_width);
  RooAddModel background("background","background model",RooArgList(bkg_argus,bkg_gauss),par1);

  ///////////////////////////////////////////////////////
  //Construct & Fit total PDF
  RooAddPdf model("model","data model",RooArgList(sig_gauss,background),RooArgList(nsig,nbkg));

  model.fitTo(*data_cut);

  RooPlot *mframe3 = m_bc.frame();
  mframe3->SetTitle("Data fit");
  data_cut->plotOn(mframe3);
  model.plotOn(mframe3,LineColor(kBlue));
  model.plotOn(mframe3,Components(background),LineStyle(kDashed));
  TCanvas can3;
  mframe3->Draw();
  can3.SaveAs("real_12.eps");

  cout << "Fit result" << endl;
  nsig.Print();
  nbkg.Print();
  sig_gauss_mean.Print();
  sig_gauss_width.Print();
  bkg_arg_par.Print();
  bkg_gauss_width.Print();
  par1.Print();
}

