\chapter{The Belle Experiment at KEK}\label{chap:experiment}


KEKB\footnote{The description of the experiment is mainly based on article "Determination of the Cabibbo-Kobayashi-Maskawa Matrix Element $|V_{cb}|$ and $|V_{ub}|$ at the Belle Experiment" \citep{habSchw}} is a national accelerator laboratory for high energy physics in Japan, about 50 km north-east of Tokyo. One of their factories is called KEK B which consists of an asymmetric particle accelerator and is optimized on the collision of $e^{+}e^{-}$ to create B-mesons.\\

The KEKB accelerator is used in the Belle experiment. Also the detector at the single interaction region is called Belle. It collected data from 1999 to 2010 and has to be still analysed. \footnote{Pictures in this chapter are mainly used from the article "The Belle detector" \cite{ABASHIAN2002117} otherwise they are refered to.}


\section{The KEKB accelerator}

KEKB is a two ring energy-asymmetric electron positron collider and has a circumference of about 3km. At the intersection of the 8 GeV electron ring (Low Energy Ring, LER) and the 3.5 GeV positron ring (High Energy Ring, HER) is the Belle detector located (Fig. \ref{fig:KEKB}). The crossing angle of the two beams in the interaction region is 22 mrad and the center-of-mass (c.m.) energy can be calculated as followed,
\begin{equation}
\sqrt{s} = \sqrt{(p_{\text{LER}}+p_{\text{HER}})^{2}}  \approx \sqrt{E_{\text{LER}}E_{\text{HER}}} \approx 10.58 \text{GeV}
\end{equation}
with the four-momenta $p_{\text{LER}}$ and $p_{\text{HER}}$ of the colliding beam particles and their energies $E_{\text{LER}}$ and $E_{\text{HER}}$. 

This energy corresponds to the energy of the $\Upsilon(4S)$ resonance and most likely decays into a pair of $B$-mesons ($B^{+}B^{-}$ or $B^{0}\bar{B}^{0}$)

Due to the asymmetry of the two beam energy the corresponding Lorentz-boost for the $\Upsilon (4S)$-Meson is 

\begin{equation}
\beta \gamma \approx \dfrac{E_{HER}-E_{LER}}{M_{\Upsilon(4S)}} \approx 0.425
\end{equation}


Therefore also the c.m. moves along the $z$-axis, where the $z$-axis is aligned with the LER but in the opposite direction as the $e^+$ momentum (as shown in Fig. \ref{fig:Belle_coord} and described in \ref{sec:SpherCoordSys}). 

\begin{figure}[H]
	\begin{center}
		\includegraphics[width = \textwidth]{figures/belle_coord_sys.png} 
		\caption{Belle coordinate system \cite{belleCob}}
		\label{fig:Belle_coord}
	\end{center}
\end{figure}

An important characteristic of any particle accelerator is the number of events detected $N$. This property can be calculated with the product of the integrated luminosity $\mathcal{L}$ and the cross-section $\sigma$ as
\begin{equation}
N = \mathcal{L} \sigma
\label{eq:nbrBB}
\end{equation}
The integrated luminosity \cite{Wille1996} is given by the integral of the luminosity $L$ over time.
\begin{equation}
\mathcal{L} = \int L dt = \int \dfrac{N_{1} ~ N_{2}}{A} ~ f ~dt 
\end{equation}
where $N_1$ and $N_2$ the number of Particles in a package, $A$ the beam cross section and $f$ the rate of colliding particle packages are.
\begin{equation}
\dfrac{dN}{dt} = L \sigma = \dfrac{N_1 N_2}{A} f \sigma
\end{equation}
As seen in (\ref{eq:nbrBB}) the number of generated $B\bar{B}$ pairs is directly proportional to the integrated luminosity of the KEKB.
The integrated luminosity is one of the most important factors of the KEKB, because it was designed to make precise measurements of the Cabibbo-Kobayashi-Maskawa mechanism (which is not relevant for this thesis).

KEKB was build to produce a luminosity of about $10^{34} ~ \text{cm}^{-2}~ \text{s}^{-1}$ but during operation luminosities of up to $2.11 \cdot 10^{34} ~ \text{cm}^{-2}~ \text{s}^{-1}$ were reached. Because of its low cross section of the $e^+ e^- \rightarrow \Upsilon (4S) \rightarrow B\bar{B}$ process the rate of which $B\bar{B}$ pairs are produced is about $20 \text{Hz}$. At the end KEKB could deliver $1040  \text{fb}^{-1}$ ($\text{b} = \text{barn}$; $\text{b} = 10^{24} \text{cm}^{-2}$ \citep{dem4}). Most of the data was recorded on the $\Upsilon (4S)$ resonance, to be precise about $711 \text{fb}^{-1}$ which correspondents to $772$ million $B\bar{B}$ events. The other data was recorded below the resonance for measuring non-$B\bar{B}$ background and at different energies to further investigate decays e.g. such as from $B^0_S$-mesons.




\begin{figure}[H]
	\begin{center}
		\includegraphics[width = \textwidth]{figures/Bellering4.png} 
		\caption{Schematic layout of the KEKB particle accelerator at KEK in Tsukuba (Japan) \cite{belleCob}}
		\label{fig:KEKB}
	\end{center}
\end{figure}

\newpage


\section{The Belle detector}

The Belle detector is a large-solid-angle magnetic spectrometer which consists of several sub-detectors. Every sub-detector (Fig. \ref{fig:Belle-det}) has its own task i.e. to determine the number of charged and neutral particle, measure the momentum and energy and identifies the type of the particle (pion, kaon, ...) of the recorded product of the $e^{+}e^{-}$ collision. The 1.5 T magnetic field from the superconducting coil, located at $R=170cm$ (coordinate system as in \ref{eq:Sphericalcoordinatesystem}), where most of the sub-detectors are located, is used to force the charged particle on an helical trajectory. This curved track is used to determine the momentum of the particles. 

\begin{figure}[H]
	\begin{center}
		\includegraphics[width = \textwidth]{figures/icondetector.png} 
		\caption{Schematic layout of the Belle detector}
		\label{fig:Belle-det}
	\end{center}
\end{figure}


\subsection{Tracking detectors}

There are two sub-detectors, within the 1.5 T magnetic field, which are used to track particles: the Silicon Vertex Detector (SVD) and the Central Drift Chamber (CDC).

\subsubsection{SVD}

The location of the SVD is around the beam pipe and measures charged particles near the $e^+e^-$ interaction point. This sub-detector is made of semiconductor diodes ($p$-$n$ junction) operated in reverse bias and called double-sided silicon strip detectors (DSSDs). When a charged particle is passing through it creates an electron-hole pair ($e^-h^+$). Due to the electric field these charges drift to either $n$- or $p$-side of the sensor. This electric signal can be detected and the intersection point of the particle track with the detector can be determined within a few tens of a micrometer.The strips on the $p$-side are aligned along the beam axis to measure the azimuthal angle $\vartheta$ and the $n$-side is perpendicular to the the beam axis to measure $z$.

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width = 0.95\textwidth]{figures/svd-config_1117.eps} 
		\caption{First version of the SVD (right: cross section; left: side view)}
		\label{fig:Belle-det}
	\end{center}
\end{figure}




\subsubsection{CDC}

Belle's tracking system is the CDC which consists of 50 cylindrical layers of anode sense wires and three cathode strip layers. These wires are either parallel to the chamber axis or slightly tilted to determine the $z$-position. 
This chamber is a drift chamber and is filled with a gas mixture of 50\% helium - 50\% ethane gas. So if a charged particle goes through it ionizes the counting gas. This drift increases towards the wire because of an increase of the electric field. The ionization of the gas can then be measured and with the position and drift time the intersection point of particle with the wire layer can be determined within $100 \mu m$.


\subsubsection{Performance}

The particle trajectories are searched and measured by the CDC and with the SVD hit points added to the CDC track reconstruction the track extrapolation can be improved towards the interaction region. It also makes possible to determine a much more accurate vertex position of the B decay.

\begin{figure}[htbp]
   \centering
       	\includegraphics[height=\textwidth, angle = -90 ]{figures/cdcfig_01.eps} 
 \caption{Schematic layout of the CDC}
 \label{fig:schematicCDC}
\end{figure}

\begin{figure}[htbp]
   \centering
       \includegraphics[width=.41\textwidth]{figures/cdcfig_02.eps}       	
       \includegraphics[width=.55\textwidth]{figures/cdcfig_03.eps} 
 \caption{Wire configuration of the CDC}
 \label{fig:schematicCDC}
\end{figure}


\subsection{Calorimetry and neutral particles}

The electromagnetic calorimeter (ECL) is used to measure photons via electromagnetic interaction. It is only used for online luminosity monitoring. Belle cannot fully measure long-lived neutrals hadrons, like $K_L$ particles, because there is no such hadronic calorimeter, but there is a the KLM system witch measures the direction of $K_L$s and muons. This is the reason why it is called KLM.

\subsubsection{ECL}

The ECL is 3.0m long and has a inner radius of 1.25m also it is made out of 8736 tower-shaped CsI(Tl) crystals and consists out of 3 parts. The the forward endcap (1152 crystals) and the backward endcap (960 crystals), located at $z=2.0m$ and $z=-1.0m$, and the barrel (6624 crystals). Photons which hit this crystals get completely  stopped and generate a scintillation light proportional to their energy. This light is collected by a photo diode on each crystal.

\begin{figure}[H]
	\begin{center}
		\includegraphics[height = 0.9\textwidth, angle = -90]{figures/ECL.eps} 
		\caption{Layout of the ECL}
		\label{fig:ECL}
	\end{center}
\end{figure}

\subsubsection{KLM}

The iron flux return is equipped with with an detector called KLM detector, it name comes from the particle which it measures. These particles are $K_{L^S}$ and muons since almost every other particle is stopped before reaching this part of the Belle solenoid. This sub detector is built out of iron plate alternating with glass resistive-plate chambers (RPCs) and a gas filled gap in between. A crossing particle can be measured by the interaction with the gas as it creates a shower which can not be used to make assumptions about the energy because it is not fully contained with in the detector. If it is induced by a $K_L$ particle the direction can be can be observed otherwise can the polar and azimuthal angels of $K_{L^S}$ be determined. 

\subsection{Particle identification}

There are just five different types of particles which can be detected by the Belle detector. These are
\begin{itemize}
\setlength\itemsep{0mm}
	\item electrons
	\item muons
	\item pions (approx. 80\% of all seen particles in the Belle detector)
	\item kaons
	\item protons
\end{itemize}
Most of the heavier particles also decay before reaching the first sensitive layer of the detector. All the particles can be identified by assigning a mass hypotheses to a charged particle trajectory.


\subsubsection{Hadron identification}

Pions, kaons and protons are hadrons as in \ref{sec:hadron} described. By combining the energy loss measurement $dE/dx$ by the CDC, the observed Cherenkov light yield at the ACC and the time-of-flight measured by the TOF one of these three hadrons can be assigned to the particle. 

The drift chamber CDC measures the points of the particle trajectories and with the wires in the CDC this energy loss can be determined. With the energy loss per unit length $dE/dx$ a particle can be identified as a pions and kaons if their momentum is within 0.4 to 0.6 GeV/$c$.

On the outside of the CDC to extend the area of the particle identification system to high momenta there is an array of threshold counters. These are called aerogel Cherenkov counter (ACC) and it consists of 960 counter modules. Cherenkov light only occurs when a particle crosses this section with a greater velocity than the speed of light in the medium $c/n$, with $n$ as the refraction index of the medium. This refractions indices is set so that only pions with a momentum of 1 to 4 GeV/$c$ produce such a Cherenkov light.

Right between the ACC and the ECL is the time-of-flight (TOF) system. This system consists of 128 plastic scintillation counters and the signal is measured by a fine-mesh photo-multiplier tubes with an time resolution of approximate 100 ps. With 1.2m away from the interaction point it can tell pions and kaons below 1.2 GeV/$c$ apart. That are about 90\% of all particles produced in an $\Upsilon(4S)$ decay.

For every measurement the likelihood function can be calculated for the particle being a kaon or a pion. Multiplying these three likelyhood function gives you the overall likelihood probability. The efficiency of determining kaons and pions this way is by about 90\% with a momentum range of 0.5 to 4 GeV/$c$. Also the fake pion rate, identifying a pion as a kaon, is within a few percent.


\begin{figure}[H]
	\begin{center}
		\includegraphics[height = 0.8\textwidth]{figures/cdcfig_dEdx.eps} 
		\caption{Particle identification by measuring the energy loss $dE/dx$ in the CDC}
		\label{fig:CDCmeasur}
	\end{center}
\end{figure}



\subsubsection{Electron identification}

For the electron identification there are three other measurements needed additionally to the ones described for hadrons:
\begin{itemize}
\setlength\itemsep{0mm}
	\item the ratio of the energy deposited in the ECL to the charged particle momentum as measured by the CDC
	\item the transverse shower shape in the ECL
	\item the matching between a cluster in ECL and the charged particle position extrapolated to the ECL
\end{itemize}

Electrons are usually completely stopped in the ECL if they just lose just a part of their energy in the calorimeter, the energy deposit will be less than from the measured momentum by the CDC expected.

The efficiency for this identification is also around 90\% for electrons with a momentum greater than 1 GeV/$c$, with a fake rate of about $0.2-0.3 \%$.

\subsubsection{Muon identification}

Muons have the ability to pass through the iron of the magnetic flux return and leave the Belle detector in this way they can be identified as such a particle to be precise it is based on comparing the measured by the KLM with the predicted range. To reach a adequate number of KLM-layers to be identified as a muon, with the KLM being located outside of the superconducting coil, the momentum of muon must be above 800 GeV/$c$. Muons with a lower momentum are following a spiral trajectory within the magnetic field and never reach the KLM.

The identification of muons with a momentum over 1 GeV/$c$ is at approximate 90\% and the fake rate again by a few percent.

\newpage
\section{Trigger and data acquisition}

\subsection{Trigger system}

With electrons and positrons packages colliding at an ultimate beam crossing rate of 509 MHz, which means every 2 ns, are a lot of these events uninteresting, such as Bhabha scattering ($e^+e^- \rightarrow e^+e^-$) and other backgrounds. Therefore a trigger system is used to suppress such events and only start the readout if the cross section is small enough, like $e^+e^- \rightarrow \Upsilon(4S) \rightarrow B\bar{B}$, $e^+e^- \rightarrow e^+e^- \rightarrow q\bar{q}$ or $e^+e^- \rightarrow \tau^+\tau^-$. Such events are only generated at a rate of a few Hz, as well the cross section lies in the nanobarn (nb)\footnote{barn is a unit which expresses the cross section area and is mainly used in particle physics  $1 b = 10^{-28} m^2$}. The measured background can have different causes such as interaction with gas in the beam pipe due to a not ideal vacuum, from synchrotron radiation or cosmic radiation.

The Belle trigger system consists of three levels. The Level-1 hardware trigger is a sub-detector trigger systems, this triggers detect either charged particle tracks or energy deposits, and a central trigger system called Global Decision Logic (GDL). The GDL receives all signals of the sub-detector triggers after $1.85 \mu s$ and has decided $2.2 \mu s$ after the collision whether it should start the readout or not. 

To monitor the trigger efficiency the redundancy of the triggers is used and combined give a efficiency more than 99.5\%.

\begin{figure}[H]
	\begin{center}
		\includegraphics[height = 0.8\textwidth]{figures/trgsystem_1.eps} 
		\caption{Belles level-1 trigger system}
		\label{fig:lvl1TriggerSys}
	\end{center}
\end{figure}

\subsection{Data acquisition}
Fast event reconstruction is done by the Level-1 trigger system. Where 7 parallel subsystems are handling each data form a sub-detector. Afterwards the event builder is combining the data into a single event record and transferred somewhere else for Level-3 filtering.
Almost every detector, like CDC, ACC, TOF, ECL and EFC, is using a technique to read out data called charge-to-time ($Q$-to-$T$). Only the SVD uses a different system.

\begin{figure}[H]
	\begin{center}
		\includegraphics[height = 0.8\textwidth]{figures/daq-overview.eps} 
		\caption{Belles data acquisition model}
		\label{fig:DAqModel}
	\end{center}
\end{figure}


\subsection{DST production}

At last the raw data is processed and reduced by the Level-4 filtering system at the KEK computing center. If an event is accepted it is reconstructed and the information is stored in a DST file. Also the detector signals are converted into physics objects as e.g. 4-vectors of charge and neutral particles. After the reconstruction process the events are classified and added to various streams. The most useful stream due to his high particle multiplicity is called HadronB(J) and contains primarily events such as $e^+e^- \rightarrow \Upsilon(4S) \rightarrow B\bar{B}$, $e^+e^- \rightarrow q\bar{q}$ and $e^+e^- \rightarrow \tau^+ \tau^-$

\newpage