%!PS-Adobe-2.0
%%Title: ./plots/data_s0.txt: rf101_basics
%%Creator: ROOT Version 6.11/01
%%CreationDate: Sat Sep 23 20:37:12 2017
%%Orientation: Landscape
%%DocumentNeededResources: ProcSet (FontSetInit)
%%EndComments
%%BeginProlog
/s {stroke} def /l {lineto} def /m {moveto} def /t {translate} def
/r {rotate} def /rl {roll}  def /R {repeat} def
/d {rlineto} def /rm {rmoveto} def /gr {grestore} def /f {eofill} def
/c {setrgbcolor} def /black {0 setgray} def /sd {setdash} def
/cl {closepath} def /sf {scalefont setfont} def /lw {setlinewidth} def
/box {m dup 0 exch d exch 0 d 0 exch neg d cl} def
/NC{systemdict begin initclip end}def/C{NC box clip newpath}def
/bl {box s} def /bf {gsave box gsave f grestore 1 lw [] 0 sd s grestore} def /Y { 0 exch d} def /X { 0 d} def 
/K {{pop pop 0 moveto} exch kshow} bind def
/ita {/ang 15 def gsave [1 0 ang dup sin exch cos div 1 0 0] concat} def 
/mp {newpath /y exch def /x exch def} def
/side {[w .77 mul w .23 mul] .385 w mul sd w 0 l currentpoint t -144 r} def
/mr {mp x y w2 0 360 arc} def /m24 {mr s} def /m20 {mr f} def
/mb {mp x y w2 add m w2 neg 0 d 0 w neg d w 0 d 0 w d cl} def
/mt {mp x y w2 add m w2 neg w neg d w 0 d cl} def
/w4 {w 4 div} def
/w6 {w 6 div} def
/w8 {w 8 div} def
/m21 {mb f} def /m25 {mb s} def /m22 {mt f} def /m26{mt s} def
/m23 {mp x y w2 sub m w2 w d w neg 0 d cl f} def
/m27 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl s} def
/m28 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl s } def
/m29 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl fill gr} def
/m30 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl s gr} def
/m31 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m32 {mp x y w2 sub m w2 w d w neg 0 d cl s} def
/m33 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl f} def
/m34 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m35 {mp x y w2 add m w2 neg w2 neg d w2 w2 neg d w2 w2 d w2 neg w2 d x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m36 {mb x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m37 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl s} def
/m38 {mp x w4 sub y w2 add m w4 neg w4 neg d 0 w2 neg d w4 w4 neg d w2 0 d w4 w4 d 0 w2 d w4 neg w4 d w2 neg 0 d x y w2 sub m 0 w d x w2 sub y m w 0 d cl s} def
/m39 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl f} def
/m40 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl s} def
/m41 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl f} def
/m42 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl s} def
/m43 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl f} def
/m44 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl s} def
/m45 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl f} def
/m46 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl s} def
/m47 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d w4 neg w4 neg d  w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl f} def
/m48 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d  w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 neg w4 d w4 w4 d cl f} def
/m49 {mp x w2 sub w3 add y w2 sub w3 add m  0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d 0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m2 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m5 {mp x w2 sub y w2 sub m w w d x w2 sub y w2 add m w w neg d s} def
%%IncludeResource: ProcSet (FontSetInit)
%%IncludeResource: font Times-Roman
%%IncludeResource: font Times-Italic
%%IncludeResource: font Times-Bold
%%IncludeResource: font Times-BoldItalic
%%IncludeResource: font Helvetica
%%IncludeResource: font Helvetica-Oblique
%%IncludeResource: font Helvetica-Bold
%%IncludeResource: font Helvetica-BoldOblique
%%IncludeResource: font Courier
%%IncludeResource: font Courier-Oblique
%%IncludeResource: font Courier-Bold
%%IncludeResource: font Courier-BoldOblique
%%IncludeResource: font Symbol
%%IncludeResource: font ZapfDingbats
/reEncode {exch findfont dup length dict begin {1 index /FID eq  {pop pop} {def} ifelse } forall /Encoding exch def currentdict end dup /FontName get exch definefont pop } def [/Times-Bold /Times-Italic /Times-BoldItalic /Helvetica /Helvetica-Oblique
 /Helvetica-Bold /Helvetica-BoldOblique /Courier /Courier-Oblique /Courier-Bold /Courier-BoldOblique /Times-Roman /AvantGarde-Book /AvantGarde-BookOblique /AvantGarde-Demi /AvantGarde-DemiOblique /Bookman-Demi /Bookman-DemiItalic /Bookman-Light
 /Bookman-LightItalic /Helvetica-Narrow /Helvetica-Narrow-Bold /Helvetica-Narrow-BoldOblique /Helvetica-Narrow-Oblique /NewCenturySchlbk-Roman /NewCenturySchlbk-Bold /NewCenturySchlbk-BoldItalic /NewCenturySchlbk-Italic /Palatino-Bold
 /Palatino-BoldItalic /Palatino-Italic /Palatino-Roman ] {ISOLatin1Encoding reEncode } forall/Zone {/iy exch def /ix exch def  ix 1 sub  3144 mul  1 iy sub  2224 mul t} def
%%EndProlog
%%BeginSetup
%%EndSetup
newpath  gsave  90 r 0 -594 t  28 20 t .25 .25 scale  gsave 
%%Page: 1 1
 gsave  gsave 
 1 1 Zone
 gsave  0 0 t black[  ] 0 sd 3 lw 1 1 1 c 924 909 1995 9 bf black 1 1 1 c 693 727 2133 100 bf black 693 727 2133 100 bl 1 1 1 c 693 727 2133 100 bf black 693 727 2133 100 bl 2133 100 m 693 X s 2133 121 m -21 Y s 2151 110 m -10 Y s 2168 110 m -10 Y s
 2185 110 m -10 Y s 2203 110 m -10 Y s 2220 121 m -21 Y s 2237 110 m -10 Y s 2255 110 m -10 Y s 2272 110 m -10 Y s 2289 110 m -10 Y s 2307 121 m -21 Y s 2324 110 m -10 Y s 2341 110 m -10 Y s 2359 110 m -10 Y s 2376 110 m -10 Y s 2393 121 m -21 Y s
 2411 110 m -10 Y s 2428 110 m -10 Y s 2445 110 m -10 Y s 2462 110 m -10 Y s 2480 121 m -21 Y s 2497 110 m -10 Y s 2514 110 m -10 Y s 2532 110 m -10 Y s 2549 110 m -10 Y s 2566 121 m -21 Y s 2584 110 m -10 Y s 2601 110 m -10 Y s 2618 110 m -10 Y s
 2636 110 m -10 Y s 2653 121 m -21 Y s 2670 110 m -10 Y s 2688 110 m -10 Y s 2705 110 m -10 Y s 2722 110 m -10 Y s 2740 121 m -21 Y s 2757 110 m -10 Y s 2774 110 m -10 Y s 2792 110 m -10 Y s 2809 110 m -10 Y s 2826 121 m -21 Y s
 gsave  924 909 1995 9 C 2103.77 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.25) show NC gr 
 gsave  924 909 1995 9 C 2182.49 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.255) show NC gr 
 gsave  924 909 1995 9 C 2276.95 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.26) show NC gr 
 gsave  924 909 1995 9 C 2355.67 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.265) show NC gr 
 gsave  924 909 1995 9 C 2450.13 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.27) show NC gr 
 gsave  924 909 1995 9 C 2528.85 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.275) show NC gr 
 gsave  924 909 1995 9 C 2623.32 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.28) show NC gr 
 gsave  924 909 1995 9 C 2702.03 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.285) show NC gr 
 gsave  924 909 1995 9 C 2796.5 70.8472 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (5.29) show NC gr 
 gsave  924 909 1995 9 C 2753.2 41.3275 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (m_bc) show NC gr  2133 100 m 727 Y s 2156 100 m -23 X s 2144 127 m -11 X s 2144 154 m -11 X s 2144 181 m -11 X s 2144 209 m -11 X s 2156 236 m -23 X s 2144 263 m -11
 X s 2144 290 m -11 X s 2144 317 m -11 X s 2144 344 m -11 X s 2156 371 m -23 X s 2144 398 m -11 X s 2144 425 m -11 X s 2144 453 m -11 X s 2144 480 m -11 X s 2156 507 m -23 X s 2144 534 m -11 X s 2144 561 m -11 X s 2144 588 m -11 X s 2144 615 m -11 X
 s 2156 642 m -23 X s 2144 669 m -11 X s 2144 697 m -11 X s 2144 724 m -11 X s 2144 751 m -11 X s 2156 778 m -23 X s 2156 778 m -23 X s 2144 805 m -11 X s
 gsave  924 909 1995 9 C 2111.64 88.559 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (0) show NC gr 
 gsave  924 909 1995 9 C 2078.19 224.35 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (100) show NC gr 
 gsave  924 909 1995 9 C 2078.19 360.14 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (200) show NC gr 
 gsave  924 909 1995 9 C 2078.19 495.931 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (300) show NC gr 
 gsave  924 909 1995 9 C 2078.19 631.721 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (400) show NC gr 
 gsave  924 909 1995 9 C 2078.19 767.512 t 0 r /Helvetica findfont 29.5568 sf 0 0 m (500) show NC gr 
 gsave  924 909 1995 9 C 2064.41 588.426 t 90 r /Helvetica findfont 29.5568 sf 0 0 m (Events / \( 0.0004 \)) show NC gr  2137 101 m -4 X s 2133 100 m 5 Y s 2137 101 m 3 X s 2140 100 m 5 Y s 2137 101 m 4 Y s 2133 105 m 8 X s 2137 101 m -1 Y s 2133 100
 m 8 X s 2144 106 m -4 X s 2140 102 m 7 Y s 2144 106 m 3 X s 2147 102 m 7 Y s 2144 106 m 4 Y s 2140 110 m 8 X s 2144 106 m -3 Y s 2140 103 m 8 X s 2151 110 m -4 X s 2147 106 m 8 Y s 2151 110 m 3 X s 2154 106 m 8 Y s 2151 110 m 5 Y s 2147 115 m 8 X s
 2151 110 m -4 Y s 2147 106 m 8 X s 2158 101 m -4 X s 2154 100 m 5 Y s 2158 101 m 3 X s 2161 100 m 5 Y s 2158 101 m 4 Y s 2154 105 m 8 X s 2158 101 m -1 Y s 2154 100 m 8 X s 2165 108 m -4 X s 2161 104 m 8 Y s 2165 108 m 3 X s 2168 104 m 8 Y s 2165
 108 m 5 Y s 2161 113 m 8 X s 2165 108 m -3 Y s 2161 105 m 8 X s 2171 104 m -3 X s 2168 100 m 8 Y s 2171 104 m 4 X s 2175 100 m 8 Y s 2171 104 m 4 Y s 2168 108 m 7 X s 2171 104 m -2 Y s 2168 102 m 7 X s 2178 104 m -3 X s 2175 100 m 8 Y s 2178 104 m 4
 X s 2182 100 m 8 Y s 2178 104 m 4 Y s 2174 108 m 8 X s 2178 104 m -2 Y s 2174 102 m 8 X s 2185 104 m -3 X s 2182 100 m 8 Y s 2185 104 m 4 X s 2189 100 m 8 Y s 2185 104 m 4 Y s 2181 108 m 8 X s 2185 104 m -2 Y s 2181 102 m 8 X s 2192 106 m -3 X s
 2189 102 m 7 Y s 2192 106 m 4 X s 2196 102 m 7 Y s 2192 106 m 4 Y s 2188 110 m 8 X s 2192 106 m -3 Y s 2188 103 m 8 X s 2199 104 m -3 X s 2196 100 m 8 Y s 2199 104 m 4 X s 2203 100 m 8 Y s 2199 104 m 4 Y s 2195 108 m 8 X s 2199 104 m -2 Y s 2195 102
 m 8 X s 2206 101 m -3 X s 2203 100 m 5 Y s 2206 101 m 4 X s 2210 100 m 5 Y s 2206 101 m 4 Y s 2202 105 m 8 X s 2206 101 m -1 Y s 2202 100 m 8 X s 2213 104 m -3 X s 2210 100 m 8 Y s 2213 104 m 4 X s 2217 100 m 8 Y s 2213 104 m 4 Y s 2209 108 m 8 X s
 2213 104 m -2 Y s 2209 102 m 8 X s 2220 107 m -3 X s 2217 103 m 8 Y s 2220 107 m 3 X s 2223 103 m 8 Y s 2220 107 m 4 Y s 2216 111 m 8 X s 2220 107 m -3 Y s 2216 104 m 8 X s 2227 104 m -4 X s 2223 100 m 8 Y s 2227 104 m 3 X s 2230 100 m 8 Y s 2227
 104 m 4 Y s 2223 108 m 8 X s 2227 104 m -2 Y s 2223 102 m 8 X s 2234 103 m -4 X s 2230 100 m 7 Y s 2234 103 m 3 X s 2237 100 m 7 Y s 2234 103 m 3 Y s 2230 106 m 8 X s 2234 103 m -2 Y s 2230 101 m 8 X s 2241 106 m -4 X s 2237 102 m 7 Y s 2241 106 m 3
 X s 2244 102 m 7 Y s 2241 106 m 4 Y s 2237 110 m 8 X s 2241 106 m -3 Y s 2237 103 m 8 X s 2248 104 m -4 X s 2244 100 m 8 Y s 2248 104 m 3 X s 2251 100 m 8 Y s 2248 104 m 4 Y s 2244 108 m 8 X s 2248 104 m -2 Y s 2244 102 m 8 X s 2255 103 m -4 X s
 2251 100 m 7 Y s 2255 103 m 3 X s 2258 100 m 7 Y s 2255 103 m 3 Y s 2251 106 m 8 X s 2255 103 m -2 Y s 2251 101 m 8 X s 2262 103 m -4 X s 2258 100 m 7 Y s 2262 103 m 3 X s 2265 100 m 7 Y s 2262 103 m 3 Y s 2258 106 m 7 X s 2262 103 m -2 Y s 2258 101
 m 7 X s 2268 112 m -3 X s 2265 108 m 8 Y s 2268 112 m 4 X s 2272 108 m 8 Y s 2268 112 m 6 Y s 2265 118 m 7 X s 2268 112 m -4 Y s 2265 108 m 7 X s 2275 103 m -3 X s 2272 100 m 7 Y s 2275 103 m 4 X s 2279 100 m 7 Y s 2275 103 m 3 Y s 2271 106 m 8 X s
 2275 103 m -2 Y s 2271 101 m 8 X s 2282 106 m -3 X s 2279 102 m 7 Y s 2282 106 m 4 X s 2286 102 m 7 Y s 2282 106 m 4 Y s 2278 110 m 8 X s 2282 106 m -3 Y s 2278 103 m 8 X s 2289 108 m -3 X s 2286 104 m 8 Y s 2289 108 m 4 X s 2293 104 m 8 Y s 2289
 108 m 5 Y s 2285 113 m 8 X s 2289 108 m -3 Y s 2285 105 m 8 X s 2296 104 m -3 X s 2293 100 m 8 Y s 2296 104 m 4 X s 2300 100 m 8 Y s 2296 104 m 4 Y s 2292 108 m 8 X s 2296 104 m -2 Y s 2292 102 m 8 X s 2303 107 m -3 X s 2300 103 m 8 Y s 2303 107 m 4
 X s 2307 103 m 8 Y s 2303 107 m 4 Y s 2299 111 m 8 X s 2303 107 m -3 Y s 2299 104 m 8 X s 2310 101 m -3 X s 2307 100 m 5 Y s 2310 101 m 4 X s 2314 100 m 5 Y s 2310 101 m 4 Y s 2306 105 m 8 X s 2310 101 m -1 Y s 2306 100 m 8 X s 2317 103 m -3 X s
 2314 100 m 7 Y s 2317 103 m 3 X s 2320 100 m 7 Y s 2317 103 m 3 Y s 2313 106 m 8 X s 2317 103 m -2 Y s 2313 101 m 8 X s 2324 104 m -4 X s 2320 100 m 8 Y s 2324 104 m 3 X s 2327 100 m 8 Y s 2324 104 m 4 Y s 2320 108 m 8 X s 2324 104 m -2 Y s 2320 102
 m 8 X s 2331 104 m -4 X s 2327 100 m 8 Y s 2331 104 m 3 X s 2334 100 m 8 Y s 2331 104 m 4 Y s 2327 108 m 8 X s 2331 104 m -2 Y s 2327 102 m 8 X s 2338 100 m -4 X s 2334 100 m 4 Y s 2338 100 m 3 X s 2341 100 m 4 Y s 2338 100 m 2 Y s 2334 102 m 8 X s
 2345 104 m -4 X s 2341 100 m 8 Y s 2345 104 m 3 X s 2348 100 m 8 Y s 2345 104 m 4 Y s 2341 108 m 8 X s 2345 104 m -2 Y s 2341 102 m 8 X s 2352 104 m -4 X s 2348 100 m 8 Y s 2352 104 m 3 X s 2355 100 m 8 Y s 2352 104 m 4 Y s 2348 108 m 8 X s 2352 104
 m -2 Y s 2348 102 m 8 X s 2359 106 m -4 X s 2355 102 m 7 Y s 2359 106 m 3 X s 2362 102 m 7 Y s 2359 106 m 4 Y s 2355 110 m 7 X s 2359 106 m -3 Y s 2355 103 m 7 X s 2365 108 m -3 X s 2362 104 m 8 Y s 2365 108 m 4 X s 2369 104 m 8 Y s 2365 108 m 5 Y s
 2362 113 m 7 X s 2365 108 m -3 Y s 2362 105 m 7 X s 2372 101 m -3 X s 2369 100 m 5 Y s 2372 101 m 4 X s 2376 100 m 5 Y s 2372 101 m 4 Y s 2368 105 m 8 X s 2372 101 m -1 Y s 2368 100 m 8 X s 2379 103 m -3 X s 2376 100 m 7 Y s 2379 103 m 4 X s 2383
 100 m 7 Y s 2379 103 m 3 Y s 2375 106 m 8 X s 2379 103 m -2 Y s 2375 101 m 8 X s 2386 108 m -3 X s 2383 104 m 8 Y s 2386 108 m 4 X s 2390 104 m 8 Y s 2386 108 m 5 Y s 2382 113 m 8 X s 2386 108 m -3 Y s 2382 105 m 8 X s 2393 110 m -3 X s 2390 106 m 8
 Y s 2393 110 m 4 X s 2397 106 m 8 Y s 2393 110 m 5 Y s 2389 115 m 8 X s 2393 110 m -4 Y s 2389 106 m 8 X s 2400 103 m -3 X s 2397 100 m 7 Y s 2400 103 m 4 X s 2404 100 m 7 Y s 2400 103 m 3 Y s 2396 106 m 8 X s 2400 103 m -2 Y s 2396 101 m 8 X s 2407
 104 m -3 X s 2404 100 m 8 Y s 2407 104 m 4 X s 2411 100 m 8 Y s 2407 104 m 4 Y s 2403 108 m 8 X s 2407 104 m -2 Y s 2403 102 m 8 X s 2414 104 m -3 X s 2411 100 m 8 Y s 2414 104 m 3 X s 2417 100 m 8 Y s 2414 104 m 4 Y s 2410 108 m 8 X s 2414 104 m -2
 Y s 2410 102 m 8 X s 2421 104 m -4 X s 2417 100 m 8 Y s 2421 104 m 3 X s 2424 100 m 8 Y s 2421 104 m 4 Y s 2417 108 m 8 X s 2421 104 m -2 Y s 2417 102 m 8 X s 2428 107 m -4 X s 2424 103 m 8 Y s 2428 107 m 3 X s 2431 103 m 8 Y s 2428 107 m 4 Y s 2424
 111 m 8 X s 2428 107 m -3 Y s 2424 104 m 8 X s 2435 103 m -4 X s 2431 100 m 7 Y s 2435 103 m 3 X s 2438 100 m 7 Y s 2435 103 m 3 Y s 2431 106 m 8 X s 2435 103 m -2 Y s 2431 101 m 8 X s 2442 103 m -4 X s 2438 100 m 7 Y s 2442 103 m 3 X s 2445 100 m 7
 Y s 2442 103 m 3 Y s 2438 106 m 8 X s 2442 103 m -2 Y s 2438 101 m 8 X s 2449 106 m -4 X s 2445 102 m 7 Y s 2449 106 m 3 X s 2452 102 m 7 Y s 2449 106 m 4 Y s 2445 110 m 8 X s 2449 106 m -3 Y s 2445 103 m 8 X s 2456 101 m -4 X s 2452 100 m 5 Y s
 2456 101 m 3 X s 2459 100 m 5 Y s 2456 101 m 4 Y s 2452 105 m 7 X s 2456 101 m -1 Y s 2452 100 m 7 X s 2462 114 m -3 X s 2459 110 m 8 Y s 2462 114 m 4 X s 2466 110 m 8 Y s 2462 114 m 5 Y s 2459 119 m 7 X s 2462 114 m -5 Y s 2459 109 m 7 X s 2469 107
 m -3 X s 2466 103 m 8 Y s 2469 107 m 4 X s 2473 103 m 8 Y s 2469 107 m 4 Y s 2465 111 m 8 X s 2469 107 m -3 Y s 2465 104 m 8 X s 2476 106 m -3 X s 2473 102 m 7 Y s 2476 106 m 4 X s 2480 102 m 7 Y s 2476 106 m 4 Y s 2472 110 m 8 X s 2476 106 m -3 Y s
 2472 103 m 8 X s 2483 110 m -3 X s 2480 106 m 8 Y s 2483 110 m 4 X s 2487 106 m 8 Y s 2483 110 m 5 Y s 2479 115 m 8 X s 2483 110 m -4 Y s 2479 106 m 8 X s 2490 107 m -3 X s 2487 103 m 8 Y s 2490 107 m 4 X s 2494 103 m 8 Y s 2490 107 m 4 Y s 2486 111
 m 8 X s 2490 107 m -3 Y s 2486 104 m 8 X s 2497 100 m -3 X s 2494 100 m 4 Y s 2497 100 m 4 X s 2501 100 m 4 Y s 2497 100 m 2 Y s 2493 102 m 8 X s 2504 110 m -3 X s 2501 106 m 8 Y s 2504 110 m 3 X s 2507 106 m 8 Y s 2504 110 m 5 Y s 2500 115 m 8 X s
 2504 110 m -4 Y s 2500 106 m 8 X s 2511 104 m -4 X s 2507 100 m 8 Y s 2511 104 m 3 X s 2514 100 m 8 Y s 2511 104 m 4 Y s 2507 108 m 8 X s 2511 104 m -2 Y s 2507 102 m 8 X s 2518 106 m -4 X s 2514 102 m 7 Y s 2518 106 m 3 X s 2521 102 m 7 Y s 2518
 106 m 4 Y s 2514 110 m 8 X s 2518 106 m -3 Y s 2514 103 m 8 X s 2525 118 m -4 X s 2521 114 m 8 Y s 2525 118 m 3 X s 2528 114 m 8 Y s 2525 118 m 6 Y s 2521 124 m 8 X s 2525 118 m -5 Y s 2521 113 m 8 X s 2532 120 m -4 X s 2528 117 m 7 Y s 2532 120 m 3
 X s 2535 117 m 7 Y s 2532 120 m 7 Y s 2528 127 m 8 X s 2532 120 m -5 Y s 2528 115 m 8 X s 2539 130 m -4 X s 2535 126 m 8 Y s 2539 130 m 3 X s 2542 126 m 8 Y s 2539 130 m 8 Y s 2535 138 m 8 X s 2539 130 m -6 Y s 2535 124 m 8 X s 2546 130 m -4 X s
 2542 126 m 8 Y s 2546 130 m 3 X s 2549 126 m 8 Y s 2546 130 m 8 Y s 2542 138 m 8 X s 2546 130 m -6 Y s 2542 124 m 8 X s 2553 139 m -4 X s 2549 135 m 8 Y s 2553 139 m 3 X s 2556 135 m 8 Y s 2553 139 m 9 Y s 2549 148 m 7 X s 2553 139 m -7 Y s 2549 132
 m 7 X s 2559 157 m -3 X s 2556 153 m 8 Y s 2559 157 m 4 X s 2563 153 m 8 Y s 2559 157 m 10 Y s 2556 167 m 7 X s 2559 157 m -9 Y s 2556 148 m 7 X s 2566 177 m -3 X s 2563 173 m 8 Y s 2566 177 m 4 X s 2570 173 m 8 Y s 2566 177 m 12 Y s 2562 189 m 8 X
 s 2566 177 m -10 Y s 2562 167 m 8 X s 2573 194 m -3 X s 2570 190 m 8 Y s 2573 194 m 4 X s 2577 190 m 8 Y s 2573 194 m 12 Y s 2569 206 m 8 X s 2573 194 m -12 Y s 2569 182 m 8 X s 2580 230 m -3 X s 2577 226 m 8 Y s 2580 230 m 4 X s 2584 226 m 8 Y s
 2580 230 m 15 Y s 2576 245 m 8 X s 2580 230 m -13 Y s 2576 217 m 8 X s 2587 280 m -3 X s 2584 276 m 8 Y s 2587 280 m 4 X s 2591 276 m 8 Y s 2587 280 m 17 Y s 2583 297 m 8 X s 2587 280 m -15 Y s 2583 265 m 8 X s 2594 295 m -3 X s 2591 291 m 8 Y s
 2594 295 m 4 X s 2598 291 m 8 Y s 2594 295 m 17 Y s 2590 312 m 8 X s 2594 295 m -15 Y s 2590 280 m 8 X s 2601 355 m -3 X s 2598 351 m 8 Y s 2601 355 m 3 X s 2604 351 m 8 Y s 2601 355 m 19 Y s 2597 374 m 8 X s 2601 355 m -18 Y s 2597 337 m 8 X s 2608
 402 m -4 X s 2604 398 m 8 Y s 2608 402 m 3 X s 2611 398 m 8 Y s 2608 402 m 21 Y s 2604 423 m 8 X s 2608 402 m -19 Y s 2604 383 m 8 X s 2615 476 m -4 X s 2611 472 m 8 Y s 2615 476 m 3 X s 2618 472 m 8 Y s 2615 476 m 23 Y s 2611 499 m 8 X s 2615 476 m
 -22 Y s 2611 454 m 8 X s 2622 519 m -4 X s 2618 515 m 8 Y s 2622 519 m 3 X s 2625 515 m 8 Y s 2622 519 m 24 Y s 2618 543 m 8 X s 2622 519 m -23 Y s 2618 496 m 8 X s 2629 557 m -4 X s 2625 553 m 8 Y s 2629 557 m 3 X s 2632 553 m 8 Y s 2629 557 m 25 Y
 s 2625 582 m 8 X s 2629 557 m -24 Y s 2625 533 m 8 X s 2636 701 m -4 X s 2632 697 m 8 Y s 2636 701 m 3 X s 2639 697 m 8 Y s 2636 701 m 29 Y s 2632 730 m 8 X s 2636 701 m -28 Y s 2632 673 m 8 X s 2643 707 m -4 X s 2639 703 m 8 Y s 2643 707 m 3 X s
 2646 703 m 8 Y s 2643 707 m 30 Y s 2639 737 m 8 X s 2643 707 m -28 Y s 2639 679 m 8 X s 2650 707 m -4 X s 2646 703 m 8 Y s 2650 707 m 3 X s 2653 703 m 8 Y s 2650 707 m 30 Y s 2646 737 m 7 X s 2650 707 m -28 Y s 2646 679 m 7 X s 2656 729 m -3 X s
 2653 725 m 8 Y s 2656 729 m 4 X s 2660 725 m 8 Y s 2656 729 m 30 Y s 2653 759 m 7 X s 2656 729 m -28 Y s 2653 701 m 7 X s 2663 703 m -3 X s 2660 699 m 8 Y s 2663 703 m 4 X s 2667 699 m 8 Y s 2663 703 m 30 Y s 2659 733 m 8 X s 2663 703 m -28 Y s 2659
 675 m 8 X s 2670 762 m -3 X s 2667 758 m 8 Y s 2670 762 m 4 X s 2674 758 m 8 Y s 2670 762 m 30 Y s 2666 792 m 8 X s 2670 762 m -30 Y s 2666 732 m 8 X s 2677 710 m -3 X s 2674 706 m 8 Y s 2677 710 m 4 X s 2681 706 m 8 Y s 2677 710 m 30 Y s 2673 740 m
 8 X s 2677 710 m -28 Y s 2673 682 m 8 X s 2684 673 m -3 X s 2681 670 m 7 Y s 2684 673 m 4 X s 2688 670 m 7 Y s 2684 673 m 29 Y s 2680 702 m 8 X s 2684 673 m -27 Y s 2680 646 m 8 X s 2691 614 m -3 X s 2688 610 m 8 Y s 2691 614 m 4 X s 2695 610 m 8 Y
 s 2691 614 m 27 Y s 2687 641 m 8 X s 2691 614 m -26 Y s 2687 588 m 8 X s 2698 538 m -3 X s 2695 534 m 8 Y s 2698 538 m 3 X s 2701 534 m 8 Y s 2698 538 m 25 Y s 2694 563 m 8 X s 2698 538 m -24 Y s 2694 514 m 8 X s 2705 496 m -4 X s 2701 492 m 8 Y s
 2705 496 m 3 X s 2708 492 m 8 Y s 2705 496 m 24 Y s 2701 520 m 8 X s 2705 496 m -23 Y s 2701 473 m 8 X s 2712 453 m -4 X s 2708 449 m 7 Y s 2712 453 m 3 X s 2715 449 m 7 Y s 2712 453 m 22 Y s 2708 475 m 8 X s 2712 453 m -22 Y s 2708 431 m 8 X s 2719
 362 m -4 X s 2715 358 m 8 Y s 2719 362 m 3 X s 2722 358 m 8 Y s 2719 362 m 19 Y s 2715 381 m 8 X s 2719 362 m -18 Y s 2715 344 m 8 X s 2726 321 m -4 X s 2722 317 m 8 Y s 2726 321 m 3 X s 2729 317 m 8 Y s 2726 321 m 18 Y s 2722 339 m 8 X s 2726 321 m
 -17 Y s 2722 304 m 8 X s 2733 271 m -4 X s 2729 267 m 8 Y s 2733 271 m 3 X s 2736 267 m 8 Y s 2733 271 m 16 Y s 2729 287 m 8 X s 2733 271 m -15 Y s 2729 256 m 8 X s 2740 238 m -4 X s 2736 234 m 8 Y s 2740 238 m 3 X s 2743 234 m 8 Y s 2740 238 m 15 Y
 s 2736 253 m 8 X s 2740 238 m -13 Y s 2736 225 m 8 X s 2747 192 m -4 X s 2743 188 m 8 Y s 2747 192 m 3 X s 2750 188 m 8 Y s 2747 192 m 13 Y s 2743 205 m 7 X s 2747 192 m -11 Y s 2743 181 m 7 X s 2753 172 m -3 X s 2750 168 m 8 Y s 2753 172 m 4 X s
 2757 168 m 8 Y s 2753 172 m 11 Y s 2750 183 m 7 X s 2753 172 m -10 Y s 2750 162 m 7 X s 2760 154 m -3 X s 2757 150 m 8 Y s 2760 154 m 4 X s 2764 150 m 8 Y s 2760 154 m 10 Y s 2756 164 m 8 X s 2760 154 m -8 Y s 2756 146 m 8 X s 2767 143 m -3 X s 2764
 140 m 7 Y s 2767 143 m 4 X s 2771 140 m 7 Y s 2767 143 m 10 Y s 2763 153 m 8 X s 2767 143 m -7 Y s 2763 136 m 8 X s 2774 130 m -3 X s 2771 126 m 8 Y s 2774 130 m 4 X s 2778 126 m 8 Y s 2774 130 m 8 Y s 2770 138 m 8 X s 2774 130 m -6 Y s 2770 124 m 8
 X s 2781 116 m -3 X s 2778 112 m 8 Y s 2781 116 m 4 X s 2785 112 m 8 Y s 2781 116 m 7 Y s 2777 123 m 8 X s 2781 116 m -4 Y s 2777 112 m 8 X s 2788 114 m -3 X s 2785 110 m 8 Y s 2788 114 m 4 X s 2792 110 m 8 Y s 2788 114 m 5 Y s 2784 119 m 8 X s 2788
 114 m -5 Y s 2784 109 m 8 X s 2795 107 m -3 X s 2792 103 m 8 Y s 2795 107 m 3 X s 2798 103 m 8 Y s 2795 107 m 4 Y s 2791 111 m 8 X s 2795 107 m -3 Y s 2791 104 m 8 X s 2802 103 m -4 X s 2798 100 m 7 Y s 2802 103 m 3 X s 2805 100 m 7 Y s 2802 103 m 3
 Y s 2798 106 m 8 X s 2802 103 m -2 Y s 2798 101 m 8 X s 2809 106 m -4 X s 2805 102 m 7 Y s 2809 106 m 3 X s 2812 102 m 7 Y s 2809 106 m 4 Y s 2805 110 m 8 X s 2809 106 m -3 Y s 2805 103 m 8 X s 2816 106 m -4 X s 2812 102 m 7 Y s 2816 106 m 3 X s
 2819 102 m 7 Y s 2816 106 m 4 Y s 2812 110 m 8 X s 2816 106 m -3 Y s 2812 103 m 8 X s 2823 104 m -4 X s 2819 100 m 8 Y s 2823 104 m 3 X s 2826 100 m 8 Y s 2823 104 m 4 Y s 2819 108 m 7 X s 2823 104 m -2 Y s 2819 102 m 7 X s /w 16 def /w2 {w 2 div}
 def /w3 {w 3 div} def 2137 101 2144 106 2151 110 2158 101 2165 108 2171 104 2178 104 2185 104 2192 106 2199 104 2206 101 2213 104 2220 107 2227 104 2234 103 2241 106 2248 104 2255 103 2262 103 2268 112 2275 103 2282 106 2289 108 2296 104 2303 107
 2310 101 2317 103 2324 104 2331 104 2338 100 2345 104 2352 104 2359 106 2365 108 2372 101 2379 103 2386 108 2393 110 2400 103 2407 104 2414 104 2421 104 2428 107 2435 103 2442 103 2449 106 2456 101 2462 114 2469 107 2476 106 2483 110 2490 107 2497
 100 2504 110 2511 104 2518 106 2525 118 2532 120 2539 130 2546 130 2553 139 2559 157 2566 177 2573 194 2580 230 2587 280 2594 295 2601 355 2608 402 2615 476 2622 519 2629 557 2636 701 2643 707 2650 707 2656 729 2663 703 2670 762 2677 710 2684 673
 2691 614 2698 538 2705 496 2712 453 2719 362 2726 321 2733 271 2740 238 2747 192 2753 172 2760 154 2767 143 2774 130 2781 116 2788 114 2795 107 2802 103 2809 106 2816 106 2823 104 100 { m20} R 0 0 1 c 9 lw black 0 0 1 c 2133 105 m cl s 2133 105 m
 354 X 7 1 d 7 X 6 1 d 7 2 d 7 2 d 7 3 d 7 5 d 7 7 d 7 9 d 7 13 d 7 16 d 3 11 d 4 11 d 3 13 d 4 15 d 3 16 d 4 18 d 3 19 d 4 22 d 3 22 d 4 25 d 3 26 d 3 27 d 7 57 d 7 60 d 7 59 d 4 28 d 3 27 d 4 25 d 3 23 d 4 21 d 3 17 d 4 14 d 3 10 d 3 7 d 4 3 d 3 -1
 d 4 -5 d 3 -9 d 4 -13 d 3 -15 d 4 -19 d 3 -22 d 4 -25 d 3 -26 d 4 -28 d 6 -58 d 7 -60 d 7 -58 d 7 -55 d 4 -25 d 3 -23 d 4 -22 d 3 -21 d 4 -18 d 3 -17 d 4 -16 d 3 -14 d 3 -12 d 4 -11 d 7 -18 d 7 -13 d 7 -11 d 7 -7 d 7 -5 d 6 -4 d 7 -3 d 7 -2 d 7 -1 d
 7 -2 d s black 3 lw 2133 100 m 693 X s 2133 121 m -21 Y s 2151 110 m -10 Y s 2168 110 m -10 Y s 2185 110 m -10 Y s 2203 110 m -10 Y s 2220 121 m -21 Y s 2237 110 m -10 Y s 2255 110 m -10 Y s 2272 110 m -10 Y s 2289 110 m -10 Y s 2307 121 m -21 Y s
 2324 110 m -10 Y s 2341 110 m -10 Y s 2359 110 m -10 Y s 2376 110 m -10 Y s 2393 121 m -21 Y s 2411 110 m -10 Y s 2428 110 m -10 Y s 2445 110 m -10 Y s 2462 110 m -10 Y s 2480 121 m -21 Y s 2497 110 m -10 Y s 2514 110 m -10 Y s 2532 110 m -10 Y s
 2549 110 m -10 Y s 2566 121 m -21 Y s 2584 110 m -10 Y s 2601 110 m -10 Y s 2618 110 m -10 Y s 2636 110 m -10 Y s 2653 121 m -21 Y s 2670 110 m -10 Y s 2688 110 m -10 Y s 2705 110 m -10 Y s 2722 110 m -10 Y s 2740 121 m -21 Y s 2757 110 m -10 Y s
 2774 110 m -10 Y s 2792 110 m -10 Y s 2809 110 m -10 Y s 2826 121 m -21 Y s 2133 100 m 727 Y s 2156 100 m -23 X s 2144 127 m -11 X s 2144 154 m -11 X s 2144 181 m -11 X s 2144 209 m -11 X s 2156 236 m -23 X s 2144 263 m -11 X s 2144 290 m -11 X s
 2144 317 m -11 X s 2144 344 m -11 X s 2156 371 m -23 X s 2144 398 m -11 X s 2144 425 m -11 X s 2144 453 m -11 X s 2144 480 m -11 X s 2156 507 m -23 X s 2144 534 m -11 X s 2144 561 m -11 X s 2144 588 m -11 X s 2144 615 m -11 X s 2156 642 m -23 X s
 2144 669 m -11 X s 2144 697 m -11 X s 2144 724 m -11 X s 2144 751 m -11 X s 2156 778 m -23 X s 2156 778 m -23 X s 2144 805 m -11 X s 1 1 1 c black
 gsave  924 909 1995 9 C 2247.43 871.814 t 0 r /Helvetica findfont 45.3205 sf 0 0 m (Signal + Background) show NC gr  gr showpage
 gr 
%%Trailer
%%Pages:  1
 gr  gr  gr 
%%EOF
