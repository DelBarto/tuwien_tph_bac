\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {german}{}
\babel@toc {german}{}
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}About this Thesis}{}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Introduction}{1}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Cylindrical coordinate system}{1}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Spherical coordinate system}{1}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Hadron}{2}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Meson}{3}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}The Belle Experiment at KEK}{4}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}The KEKB accelerator}{4}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}The Belle detector}{8}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Tracking detectors}{9}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline SVD}{9}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline CDC}{10}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Performance}{10}{section*.13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Calorimetry and neutral particles}{11}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline ECL}{11}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline KLM}{12}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Particle identification}{12}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Hadron identification}{13}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Electron identification}{15}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Muon identification}{15}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Trigger and data acquisition}{16}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Trigger system}{16}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Data acquisition}{17}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}DST production}{18}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Data analysis}{19}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}$B$ meson reconstruction}{19}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Monte Carlo generated Data}{22}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Data Analysis}{23}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Counting MC events}{23}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Model fitting}{23}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Efficiency}{24}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Pull-Test}{28}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Test on real data}{29}{section.4.4}
