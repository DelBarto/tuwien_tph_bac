hadd -f "evt_charged_exp55_s0.root" "evt_charged_exp55_r0_s0.root" "evt_charged_exp55_r500_s0.root" "evt_charged_exp55_r1000_s0.root" "evt_charged_exp55_r1500_s0.root"
hadd -f "evt_charged_exp55_s1.root" "evt_charged_exp55_r0_s1.root" "evt_charged_exp55_r500_s1.root" "evt_charged_exp55_r1000_s1.root" "evt_charged_exp55_r1500_s1.root"
hadd -f "evt_charged_exp55_s2.root" "evt_charged_exp55_r0_s2.root" "evt_charged_exp55_r500_s2.root" "evt_charged_exp55_r1000_s2.root" "evt_charged_exp55_r1500_s2.root"

hadd -f "evt_mixed_exp55_s0.root" "evt_mixed_exp55_r0_s0.root" "evt_mixed_exp55_r500_s0.root" "evt_mixed_exp55_r1000_s0.root" "evt_mixed_exp55_r1500_s0.root"
hadd -f "evt_mixed_exp55_s1.root" "evt_mixed_exp55_r0_s1.root" "evt_mixed_exp55_r500_s1.root" "evt_mixed_exp55_r1000_s1.root" "evt_mixed_exp55_r1500_s1.root"
hadd -f "evt_mixed_exp55_s2.root" "evt_mixed_exp55_r0_s2.root" "evt_mixed_exp55_r500_s2.root" "evt_mixed_exp55_r1000_s2.root" "evt_mixed_exp55_r1500_s2.root"

hadd -f "on_res_exp55.root" "on_res_exp55_r0.root" "on_res_exp55_r500.root" "on_res_exp55_r1000.root" "on_res_exp55_r1500.root"

hadd -f "evt_exp55_s0.root" "evt_charged_exp55_s0.root" "evt_mixed_exp55_s0.root"
hadd -f "evt_exp55_s1.root" "evt_charged_exp55_s1.root" "evt_mixed_exp55_s1.root"
hadd -f "evt_exp55_s2.root" "evt_charged_exp55_s2.root" "evt_mixed_exp55_s2.root"

hadd -f "evt_exp55_s0_s1.root" "evt_exp55_s0.root" "evt_exp55_s1.root"
hadd -f "evt_exp55_s0_s2.root" "evt_exp55_s0.root" "evt_exp55_s2.root"
hadd -f "evt_exp55_s1_s2.root" "evt_exp55_s1.root" "evt_exp55_s2.root"

hadd -f "evt_exp55.root" "evt_exp55_s0s1.root" "evt_exp55_s2.root"
